import requests
import json
import os
import datetime
import dateutil.parser

EXCHANGE = 'bitmex'
SYMBOL = 'xbtusd'
FROM = '2017-12-21-00-00-00'
TO = '2017-12-23-23-59-59'

r = requests.get('https://dynamicdata.colewoodcapital.com/api/'
                 'ticks?exchange={0}&symbol={1}&'
                 'from={2}&to={3}'.format(EXCHANGE, SYMBOL, FROM, TO))

csv = ''
data_set = r.json()['data']
for tick in data_set:
    ts = dateutil.parser.parse(tick['tstamp'])
    csv += '{0},{1},{2}\n'.format(int(ts.timestamp()), tick['bid'], tick['ask'])

directory = 'datasets/{0}/{1}/csv/'.format(EXCHANGE, SYMBOL)
if not os.path.exists(directory):
    os.makedirs(directory)

filename = directory + '{0}_{1}.csv'.format(FROM, TO)
with open(filename, 'w') as f:
    f.write(csv)
