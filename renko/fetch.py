import requests
import json
import os
import argparse
from time import sleep
import datetime
import dateutil.parser

# https://dynamicdata.colewoodcapital.com/api/

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

HOURS_INCREMENT = 24

def strToTime(myString):
    return datetime.datetime.strptime(myString, '%Y-%m-%d-%H-%M-%S')

def timeToStr(myTime):
    return myTime.strftime('%Y-%m-%d-%H-%M-%S')

client = requests.session()
data = ''

parser = argparse.ArgumentParser(description='Download ticks from crypto tick data server')
parser.add_argument('host', type=str)
parser.add_argument('exchange')
parser.add_argument('symbol', help='case insensitive')
parser.add_argument('begin', metavar='begin time', help='query begin time. Format Y-m-d-H-M-S')
parser.add_argument('end', metavar='end time', nargs='?', help='query end time. Format Y-m-d-H-M-S')
parser.add_argument('--outDir', default=None)
parser.add_argument('--debug', default=False)
args = parser.parse_args()

DEBUG = args.debug
HOST = args.host.rstrip('/')
EXCHANGE = args.exchange.lower()
SYMBOL = args.symbol.lower()
OUTDIR = args.outDir.rstrip('/') + '/' if args.outDir else None
begin_time = strToTime(args.begin)
end_time = strToTime(args.end)  if args.end else dt.datetime.now()

s = begin_time
i = s
ATTEMPTS = 3

if OUTDIR:
    directory = os.path.join(SCRIPT_DIR, 'ticks', 'raw', EXCHANGE, SYMBOL, OUTDIR)
else:
    directory = os.path.join(SCRIPT_DIR, 'ticks', 'raw', EXCHANGE, SYMBOL)

if not os.path.exists(directory):
    os.makedirs(directory)

if DEBUG:
    print('begin collecting tick data ...')

while s < end_time:
    i += datetime.timedelta(hours=HOURS_INCREMENT)

    if i > end_time:
        i = end_time

    fname = '{0}_{1}.csv'.format(timeToStr(s), timeToStr(i))
    filename = directory + fname

    if os.path.isfile(filename):
        if DEBUG:
            print('file already exists: skipping: ', fname)
    else:
        url = '{0}/ticks?exchange={1}&symbol={2}&' \
                 'from={3}&to={4}'.format(HOST, EXCHANGE, SYMBOL, timeToStr(s), timeToStr(i))

        res = None
        for k in range(ATTEMPTS):
            res = client.get(url)
            if res.status_code == 200:
                break
            sleep(3)

        if res.status_code != 200:
            print('fetch tick request failed {0} times, with url={1}, status_code={2}, text={3}'.format(
                ATTEMPTS,
                url,
                res.status_code,
                res.text
            ))
            sys.exit(1)

        csv = ''
        data_set = res.json()['data']
        for tick in data_set:
            ts = dateutil.parser.parse(tick['tstamp'])
            csv += '{0},{1},{2}\n'.format(ts.strftime('%Y-%m-%d %H:%M:%S'), tick['bid'], tick['ask'])

        with open(filename, 'w') as f:
            f.write(csv)

        if DEBUG:
            print('finished writing ticks from %s to %s' % (timeToStr(s), timeToStr(i)))

    s = i

if DEBUG:
    print('success')
