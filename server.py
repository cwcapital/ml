import sys
import websocket
import threading
import queue
import traceback
import ssl
from time import sleep
import json
import decimal
import logging
import logging.handlers
import operator
import utils.config as Config
from trading.bitmex.interface.market_maker.auth.APIKeyAuth import generate_nonce, generate_signature
from future.utils import iteritems
from future.standard_library import hooks
with hooks():  # Python 2/3 compat
    from urllib.parse import urlparse, urlunparse
from copy import copy
from pprint import pprint
import signal
import argparse
import warnings
from utils.metadata import PROJECT_DIR
import hashlib
from collections import deque

from twisted.internet import task
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

# Connects to BitMEX websocket for streaming realtime data.
# The Marketmaker still interacts with this as if it were a REST Endpoint, but now it can get
# much more realtime data without heavily polling the API.
#
# The Websocket offers a bunch of data as raw properties right on the object.
# On connect, it synchronously asks for a push of all this data then returns.
# Right after, the MM can start using its data. It will be updated in realtime, so the MM can
# poll as often as it wants.

def write(logger, level):
    def callback(message):
        # Only log if there is a message (not just a new line)
        if message.rstrip() != "":
            logger.log(level, message.rstrip())
    return callback

# setting up root logger
LOG_FILENAME = "{}/bitmexserver.log".format(PROJECT_DIR)
logger = logging.getLogger('root')
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)-8s %(message)s')
handler.setFormatter(formatter)

level = logging.DEBUG
logger.addHandler(handler)
logger.setLevel(level)
logger.write = write(logger, level)
logger.flush = lambda : None

# Replace stdout with logging to file at INFO level
sys.stdout = logger
#
# # Replace stderr with logging to file at ERROR level
sys.stderr = logger

def on_close(ws):
    # self.logger.info('*********Websocket Closed')
    print('*********Websocket Closed')
    # print(dir(ws))
    ws.close()

def on_error(ws, error):
    # self.logger.error('*******we got an error here: ')
    # self.logger.error(error)
    print('*******we got an error here: ')
    print(error)
    # if not ws.exited:
    #     ws.error(error)

class MyWebSocketApp(websocket.WebSocketApp):
    def run_forever(self, *args, **kwargs):
        try:
            super().run_forever(*args, **kwargs)
        except Exception as e:
            self.bucket.put(sys.exc_info())
    def __init__(self, endpoint, **kwargs):
        self.bucket = queue.Queue()
        super().__init__(endpoint, **kwargs)

class BitMEXWebsocket():
    # Don't grow a table larger than this amount. Helps cap memory usage.
    MAX_TABLE_LEN = 200

    def __init__(self, endpoint, log_handler):
        self.logger = logging.getLogger('__websockets__')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(log_handler)

        self.orderbook_type = endpoint
        self.__reset()

    def __del__(self):
        self.exit()

    def connect(self, endpoint="", symbol="", shouldAuth=True):
        '''Connect to the websocket and initialize data stores.'''

        self.logger.info("Connecting to WebSocket.")
        self.symbol = symbol
        self.shouldAuth = shouldAuth

        # We can subscribe right in the connection querystring, so let's build that.
        # Subscribe to all pertinent endpoints
        subscriptions = [sub + ':' + symbol for sub in ["quote"]]
        subscriptions += [ self.orderbook_type ]

        # Get WS URL and connect.
        urlParts = list(urlparse(endpoint))
        urlParts[0] = urlParts[0].replace('http', 'ws')
        urlParts[2] = "/realtime?subscribe=" + ",".join(subscriptions)
        wsURL = urlunparse(urlParts)
        self.logger.info("Connecting to %s" % wsURL)
        self.__connect(wsURL)

        self.logger.info('Got all market data. Starting.')

    #
    # Data methods
    #
    def get_instrument(self, symbol):
        instruments = self.data['instrument']

        matchingInstruments = [i for i in instruments if i['symbol'] == symbol]

        if len(matchingInstruments) == 0:
            raise Exception("Unable to find instrument or index with symbol: " + symbol)
        instrument = matchingInstruments[0]
        # Turn the 'tickSize' into 'tickLog' for use in rounding
        instrument['tickLog'] = decimal.Decimal(str(instrument['tickSize'])).as_tuple().exponent * -1

        return instrument

    def get_ticker(self):
        '''Return a ticker object. Generated from instrument.'''

        instrument = self.get_instrument(self.symbol)

        # If this is an index, we have to get the data from the last trade.
        if instrument['symbol'][0] == '.':
            ticker = {}
            ticker['mid'] = ticker['buy'] = ticker['sell'] = ticker['last'] = instrument['markPrice']
        # Normal instrument
        else:
            bid = instrument['bidPrice'] or instrument['lastPrice']
            ask = instrument['askPrice'] or instrument['lastPrice']
            ticker = {
                "last": instrument['lastPrice'],
                "buy": bid,
                "sell": ask
            }

        # The instrument has a tickSize. Use it to round values.
        return {k: round(float(v or 0), instrument['tickLog']) for k, v in iteritems(ticker)}

    def funds(self):
        return self.data['margin'][0]

    def market_depth(self, symbol):
        raise NotImplementedError('orderBook is not subscribed; use askPrice and bidPrice on instrument')
        # return self.data['orderBook25'][0]


    def recent_trades(self):
        return self.data['trade']

    #
    # Lifecycle methods
    #
    def error(self, err):
        self._error = err
        self.logger.error(err)
        self.exit()

    def exit(self):
        def handler(signum, frame):
            raise Exception('5 second timeout exceeded! :(')

        self.exited = True

        signal.signal(signal.SIGALRM, handler)

        signal.alarm(5)

        try:
            self.ws.close()
        except Exception as e:
            print(e)
        finally:
            signal.alarm(0)

    #
    # Private methods
    #

    def __connect(self, wsURL):
        '''Connect to the websocket in a thread.'''

        self.logger.info("Starting thread")

        ssl_defaults = ssl.get_default_verify_paths()
        sslopt_ca_certs = {'ca_certs': ssl_defaults.cafile}
        self.ws = MyWebSocketApp(wsURL,
                                         on_message=self.__on_message,
                                         on_close=on_close,
                                         on_open=self.__on_open,
                                         on_error=on_error,
                                         header=self.__get_auth()
                                         )
        self.ws.keep_running = True

        self.wst = threading.Thread(target=lambda : self.ws.run_forever(sslopt=sslopt_ca_certs))
        self.wst.daemon = False
        self.wst.start()

        # Wait for connect before continuing
        conn_timeout = 5
        while (not self.ws.sock or not self.ws.sock.connected) and conn_timeout and not self._error:
            sleep(1)
            conn_timeout -= 1

        if not conn_timeout or self._error:
            self.logger.error("Couldn't connect to WS! Exiting.")
            if self._error:
                self.logger.error(self._error)
            self.exit()
            sys.exit(1)

    def __get_auth(self):
        '''Return auth headers. Will use API Keys if present in config.'''

        if self.shouldAuth is False:
            return []

        self.logger.info("Authenticating with API Key.")
        # To auth to the WS using an API key, we generate a signature of a nonce and
        # the WS API endpoint.
        nonce = generate_nonce()
        return [
            "api-nonce: " + str(nonce),
            "api-signature: " + generate_signature(config.API_SECRET, 'GET', '/realtime', nonce, ''),
            "api-key:" + config.API_KEY
        ]

    def __wait_for_symbol(self, symbol):
        '''On subscribe, this data will come down. Wait for it.'''
        while not {'instrument', 'quote'} <= set(self.data) or \
           len(self.data['instrument']) == 0 or len(self.data['quote']) == 0:
           sleep(5)

    def __send_command(self, command, args=[]):
        '''Send a raw command.'''
        self.ws.send(json.dumps({"op": command, "args": args}))

    def __on_message(self, ws, message):
        '''Handler for parsing WS messages.'''
        message = json.loads(message)
        table = message['table'] if 'table' in message else None
        action = message['action'] if 'action' in message else None
        try:
            if 'subscribe' in message:
                if message['success']:
                    self.logger.info("Subscribed to %s." % message['subscribe'])
                else:
                    self.error("Unable to subscribe to %s. Error: \"%s\" Please check and restart." %
                               (message['request']['args'][0], message['error']))
            elif 'status' in message:
                if message['status'] == 400:
                    self.error(message['error'])
                if message['status'] == 401:
                    self.error("API Key incorrect, please check and restart.")
            elif action:

                if table not in self.data:
                    self.data[table] = []

                if table not in self.keys:
                    self.keys[table] = []

                # There are four possible actions from the WS:
                # 'partial' - full table image
                # 'insert'  - new row
                # 'update'  - update row
                # 'delete'  - delete row
                if action == 'partial':
                    self.logger.debug("%s: partial" % table)
                    self.data[table] += message['data']
                    # Keys are communicated on partials to let you know how to uniquely identify
                    # an item. We use it for updates.
                    self.keys[table] = message['keys']
                elif action == 'insert':
                    self.logger.debug('%s: inserting %s' % (table, message['data']))
                    self.data[table] += message['data']

                    # Limit the max length of the table to avoid excessive memory usage.
                    # Don't trim orders because we'll lose valuable state if we do.
                    if table != 'order' and len(self.data[table]) > BitMEXWebsocket.MAX_TABLE_LEN:
                        self.data[table] = self.data[table][(BitMEXWebsocket.MAX_TABLE_LEN // 2):]

                elif action == 'update':
                    self.logger.debug('%s: updating %s' % (table, message['data']))
                    # Locate the item in the collection and update it.
                    for updateData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], updateData)
                        if not item:
                            continue  # No item found to update. Could happen before push

                        # Log executions
                        # if table == 'order':
                        #     is_canceled = 'ordStatus' in updateData and updateData['ordStatus'] == 'Canceled'
                        #     if 'cumQty' in updateData and not is_canceled:
                        #         contExecuted = updateData['cumQty'] - item['cumQty']
                        #         if contExecuted > 0:
                        #             instrument = self.get_instrument(item['symbol'])
                        #             self.logger.info("Execution: %s %d Contracts of %s at %.*f" %
                        #                      (item['side'], contExecuted, item['symbol'],
                        #                       instrument['tickLog'], item['price']))

                        # Update this item.
                        item.update(updateData)

                        # Remove canceled / filled orders
                        if table == 'order' and item['leavesQty'] <= 0:
                            self.data[table].remove(item)

                elif action == 'delete':
                    self.logger.debug('%s: deleting %s' % (table, message['data']))
                    # Locate the item in the collection and remove it.
                    for deleteData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], deleteData)
                        try:
                            self.data[table].remove(item)
                        except ValueError as e:
                            if e.args[0] == 'list.remove(x): x not in list':
                                continue
                            raise ValueError(e)

                else:
                    raise Exception("Unknown action: %s" % action)
        except:
            self.logger.error(traceback.format_exc())

    def __on_open(self, ws):
        self.logger.info("******Websocket Opened.")

    def __reset(self):
        self.data = {}
        self.keys = {}
        self.exited = False
        self._error = None

    def get_orderbook(self):
        data = self.data.get(self.orderbook_type)
        if not data:
            warnings.warn('unable to get orderbook: {} from data: {}'.format(
                self.orderbook_type, self.data
            ))
            return {'bids': [], 'asks': []}
        try:
            results_len = len(data)
        except KeyError:
            results_len = 0

        self.logger.debug('get {}. len={}'.format(
            data,
            results_len
        ))

        if results_len > 0:
            return max(data, key=operator.itemgetter('timestamp'))
        else:
            print('[ERROR] results_len = 0')
            print('data:', data)
            return {'bids': [], 'asks': []}

    def flush_data(self):
        temp = copy(self.data)
        orderbook = []

        for k,v in self.data.items():
            if k[:9].lower() == 'orderbook':
                orderbook.append(k)
            elif k == 'instrument':
                pass
            else:
                self.data[k] = []

        for o in orderbook:
            del temp[o]

        return temp


def findItemByKeys(keys, table, matchData):
    for item in table:
        matched = True
        for key in keys:
            if item[key] != matchData[key]:
                matched = False
        if matched:
            return item

class BitMEXProtocol(LineReceiver):
    def __init__(self, factory):
        self.factory = factory

    def sendData(self, message):
        self.sendLine(message)

    def lineReceived(self, payload):
        counter = self.factory.counter
        client_counter = json.loads(payload.decode('utf-8'))['counter']

        if client_counter < counter:
            print('replaying data from counter = {} -> {}'.format(
                client_counter, counter
            ))

            if self.factory.sentData.maxlen and counter >= self.factory.sentData.maxlen:
                diff = counter - client_counter
                start = self.factory.sentData.maxlen - diff
                end = self.factory.sentData.maxlen
            else:
                start = client_counter
                end = counter

            for i in range(start, end):
                payload = self.factory.sentData[i]
                self.sendData(payload)

    def connectionMade(self):
        print('Connection made.')
        self.factory.protocols.append(self)

    def connectionLost(self, reason):
        print('lost connection!!!!')
        print('reason: {}'.format(reason.value))
        self.factory.protocols.remove(self)

class BitMEXProtocolFactory(Factory):
    def __init__(self):
        self.protocols = []
        self.sentData = deque()
        self.counter = 0

    def broadcastBitMEXData(self, bitmexWS):
        try:
            data = { 'data' : bitmexWS.ws.flush_data() }
            data['orderbook'] = bitmexWS.ws.get_orderbook()
            data['ticker'] = bitmexWS.ws.get_ticker()
            data['counter'] = self.counter
        except Exception as e:
            print('caught exception (below). skip sending data')
            print(e)
            return

        payload = json.dumps(data).encode('utf-8')

        logger.debug('--------------')
        logger.debug('counter: {}'.format(self.counter))
        logger.debug('sending data: {}'.format(hashlib.sha256(payload).hexdigest()))

        # save data before sending to clients
        self.sentData.append(payload)

        for proto in self.protocols:
            proto.sendData(payload)

        self.counter += 1

    def buildProtocol(self, addr):
        print('building protocol ... ')
        proto = BitMEXProtocol(self)
        return proto

class Main(threading.Thread):
    def __init__(self, log_handler):
        super().__init__()
        self.stoprequest = threading.Event()

        symbol = 'XBTUSD'
        orderbook_type = 'orderBook10'
        endpoint = '{}:{},trade:{},instrument:{},execution,order,position'.format(
            orderbook_type, symbol, symbol, symbol
        )

        self.ws = BitMEXWebsocket(endpoint=endpoint, log_handler=log_handler)
        self.ws.connect(endpoint=config.BASE_URL, symbol=config.SYMBOL)
        self.ws.orderbook_type = orderbook_type
        self.ws.symbol = symbol
        self.thread_bucket = self.ws.ws.bucket

    def run(self):
        # As long as we weren't asked to stop, try to take new tasks from the
        # queue. The tasks are taken with a blocking 'get', so no CPU
        # cycles are wasted while waiting.
        # Also, 'get' is given a timeout, so stoprequest is always checked,
        # even if there's nothing in the queue.

        while not self.stoprequest.isSet():
            if not self.ws.ws.sock.connected:
                break

            try:
                exc = self.thread_bucket.get(block=False)
            except queue.Empty:
                pass
            else:
                exc_type, exc_obj, exc_trace = exc
                # deal with the exception
                ws.logger('{}, {}'.format(exc_type, exc_obj))
                traceback.print_tb(exc_trace, limit=1, file=sys.stdout)
                break

            sleep(1)

        print('terminating main thread ...')

def check_main_thread(main_thread):
    if not main_thread.isAlive():
        reactor.stop()

def cbLoopDone(result):
    """
    Called when loop was stopped with success.
    """
    print("Loop done.")
    reactor.stop()

def ebLoopFailed(failure):
    """
    Called when loop execution failed.
    """
    print(failure.getTraceback())
    reactor.stop()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='machine learning bitmex algo')
    parser.add_argument('--config', required=False, default=None)
    args = parser.parse_args()

    # setting up configuration
    DEFAULT_LOG_DIR = '{}/config.json'.format(PROJECT_DIR)
    if args.config:
        Config.set_path_to_config_file(args.config)
    else:
        logger.warn('config path not set. using default: {}'.format(DEFAULT_LOG_DIR))
        Config.set_path_to_config_file(DEFAULT_LOG_DIR)

    config = Config.load()

    print('starting main thread ...')

    main_thread = Main(log_handler=handler)
    main_thread.start()

    print('starting server ...')

    singleton_factory = BitMEXProtocolFactory()
    endpoint = TCP4ServerEndpoint(reactor, 8948)
    endpoint.listen(singleton_factory)

    print('server started.')

    loop1 = task.LoopingCall(singleton_factory.broadcastBitMEXData, main_thread)
    loopDeferred1 = loop1.start(5.0)
    loopDeferred1.addCallback(cbLoopDone)
    loopDeferred1.addErrback(ebLoopFailed)

    loop2 = task.LoopingCall(check_main_thread, main_thread)
    loopDeferred2 = loop2.start(5.0)
    loopDeferred2.addCallback(cbLoopDone)
    loopDeferred2.addErrback(ebLoopFailed)

    reactor.run()

    print('killing websockets ...')
    main_thread.ws.ws.keep_running = False

    print('killing main thread ...')
    main_thread.stoprequest.set()
