from .events import OrderAmendEvent

def callback_max_deviation_from_top_of_book(max_deviation,
                        distance_from_top_of_book_on_price_amend):
    def callback(order, *args, **kwargs):
        orderbook = kwargs['orderbook']
        if order.side == 'buy':
            top_of_book_bid = orderbook['bids'][0][0]
            if top_of_book_bid - order.price >= max_deviation:
                new_prc = top_of_book_bid - distance_from_top_of_book_on_price_amend
                OrderAmendEvent(order.clOrdID, new_prc)
        else:
            top_of_book_ask = orderbook['asks'][0][0]
            if order.price - top_of_book_ask >= max_deviation:
                new_prc = top_of_book_ask + distance_from_top_of_book_on_price_amend
                OrderAmendEvent(order.clOrdID, new_prc)

    return callback
