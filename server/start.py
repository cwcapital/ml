from flask import Flask, jsonify, request
import numpy as np
from renko import RenkoDNN, RenkoRNN
from pprint import pprint

app = Flask(__name__)

# xbtusd_renko_260_in_1_out_100_100_hidden_16bin = Renko1('260_in_1_out_100_100_hidden_16bin')
# xbtusd_renko_260_in_1_out_100_100_hidden_16bin_aws = Renko1('260_in_1_out_100_100_hidden_16bin_aws')
# testnoshuffle2 = Renko1('testnoshuffle2')
# testshuffle3 = Renko1('testshuffle3')
# stuckatval63 = Renko1('260_in_1_out_100_100_hidden_16_1_out_2_stuckatval63')
# stuckatval63_2 = Renko1('260_in_1_out_100_100_hidden_16_1_out_2_stuckatval63_2')
# aws_1_out_2 = Renko1('260_in_1_out_100_100_hidden_16_1_out_2_aws')
# testspecialloss_63 = Renko1('testspecialloss_63')
# testspecialloss_66 = Renko1('testspecialloss_66')
# eight_box = Renko1('8_box')
# eight_box_3 = Renko1('8_box_3')
# eight_box_4 = RenkoDNN('8_box_4', 260, [100, 100], 2)
# eight_box_5 = RenkoDNN('8_box_5', 260, [100, 100], 2)

# rnn_model = RenkoRNN('rnn_model', n_input=2, time_steps=50,
#         num_units=100, n_output_classes=2, forget_bias=1)

rnnsplit2 = RenkoRNN('rnn_model_split_2', n_input=2, time_steps=50,
        num_units=100, n_output_classes=2, forget_bias=1)

# rnn_model_aws = RenkoRNN('rnn_model_aws', n_input=2, time_steps=50,
#         num_units=100, n_output_classes=2, forget_bias=1)

rnn_model_large = RenkoRNN('rnn_model_large', n_input=2, time_steps=50,
        num_units=100, n_output_classes=2, forget_bias=1)

rnn_model_split = RenkoRNN('rnn_model_split', n_input=2, time_steps=50,
        num_units=100, n_output_classes=2, forget_bias=1)

@app.route('/ml/<algo_type>/<algo_name>', methods=['POST'])
def algo(algo_type, algo_name):
    algo = globals()[algo_name]
    algo_type = algo_type.lower()
    x = np.array(request.get_json())

    if algo_type == 'dnn':
        x_batch = x.reshape(1, x.shape[0])
    elif algo_type == 'rnn':
        x_batch = x.reshape((-1, 50, 2))
    else:
        x_batch = None


    response = {
        'result': 'success',
        'data': algo.run(x_batch).tolist()[-1]
    }

    return jsonify(response)
