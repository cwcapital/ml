from trading.bitmex.interface.run import BitMEXInterface
from trading.bitmex.interface.utils import PendingOrderTracker, InvalidDelete, DataError
from trading.bitmex.strategies import Strategy
from trading.bitmex import helper
from trading.utils import runtime
from trading.utils.events import ReturnEvent, CancelAllOrdersEvent, OrderPlaceEvent
from trading.utils.custom_events import CustomEvent
from trading.utils.managers import PendingOrderManager, RenkoBarManager
from trading.utils.callbacks import callback_max_deviation_from_top_of_book
from trading.tools import environment
import requests
import os
import datetime as dt
from pathlib import Path
import pytz
import sys
import statistics as stats
from pprint import pprint
from math import log
import pickle
import argparse
import math
import json
import logging
import pickle
import warnings
from time import sleep

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[1])
RENKO_DIR = os.path.join(PROJECT_DIR, 'renko')

logger = logging.getLogger('root')

# class ModelEvaluation:
#     def __init__(self, model_name):
#         self.name = model_name
#         self.predictions = []
#         self.outcomes = []
#         self.sample_set = []
#         self._impending_pred = None
#         self._impending_sample = None
#
#     def _sanity_check(self):
#         err = ''
#         if len(self.predictions) == 0:
#             err += 'cannot evaluate because there arent any predictions. '
#         if len(self.outcomes) == 0:
#             err += 'cannot evaluate because there arent any outcomes. '
#         if len(self.predictions) != len(self.outcomes):
#             err += '"# of outcomes({})" should equal one more than "# of predictions({})"'.format(
#                 len(self.predictions), len(self.outcomes)
#             )
#
#         if len(self.predictions) > 0:
#             pred_size = len(self.predictions[0])
#             outcome_size = 1
#             for i,j in zip(self.predictions, self.outcomes):
#                 if len(i) != pred_size:
#                     err += 'prediction: {} should have size({})'.format(i, pred_size)
#                 if type(j) != int:
#                     err += 'outcome: {} should be of type int'.format(j)
#
#         return err if err else None
#
#     def _win_loss_percentage(self, threshold):
#         winners = []
#         skipped = []
#         for pred, outcome in zip(self.predictions, self.outcomes):
#             largest_confidence = helper.argmax(pred)
#             largest_confidence_val = pred[largest_confidence]
#
#             if largest_confidence_val >= threshold:
#                 if largest_confidence == outcome:
#                     winners.append(1)
#                 else:
#                     winners.append(0)
#
#                 skipped.append(0)
#             else:
#                 winners.append(-1)
#                 skipped.append(1)
#
#
#
#         skipped_len = sum(skipped)
#         winners_sum = sum(1 if x == 1 else 0 for x in winners)
#         total = len(winners) - skipped_len
#
#         try:
#             win_percentage = round(winners_sum/total, 2)
#         except ZeroDivisionError:
#             win_percentage = 0.0
#
#         return {
#             'winners': winners_sum,
#             'losers': total - winners_sum,
#             'win_percentage': win_percentage,
#             'skipped': skipped_len,
#             'result': 'success'
#         }
#
#     def _loss_error(self, x, n_choices):
#         def h(a):
#             return -1 / math.log((1/float(a))*math.log(float(a)))
#
#         return n_choices * (math.e ** ((1 - abs(x-(1/float(n_choices)))) / -h(n_choices)))
#
#     def _win_loss_summary(self):
#         results = []
#         winner_results = []
#         loser_results = []
#         n_choices = len(self.predictions[0])
#
#         for n, info in enumerate(zip(self.predictions, self.outcomes)):
#             pred, outcome = info
#             largest_confidence = helper.argmax(pred)
#             if largest_confidence == outcome:
#                 choice = pred[largest_confidence]
#                 err = -log(choice)
#                 results.append((n, 'W', [round(p,2) for p in pred], outcome, round(err, 2)))
#                 winner_results.append(round(err, 2))
#             else:
#                 choice = pred[largest_confidence]
#                 err = self._loss_error(choice, n_choices)
#                 results.append((n, 'L', [round(p,2) for p in pred], outcome, round(err, 2)))
#                 loser_results.append(round(err, 2))
#
#         try:
#             mean = stats.mean((y for _,_,_,_,y in results))
#         except stats.StatisticsError:
#             mean = 0
#         try:
#             winner_mean = stats.mean(winner_results)
#         except stats.StatisticsError:
#             winner_mean = 0
#         try:
#             loser_mean = stats.mean(loser_results)
#         except stats.StatisticsError:
#             loser_mean = 0
#
#         try:
#             stdev = stats.stdev((y for _,_,_,_,y in results))
#         except stats.StatisticsError:
#             stdev = 0
#         try:
#             winner_stdev = stats.stdev(winner_results)
#         except stats.StatisticsError:
#             winner_stdev = 0
#         try:
#             loser_stdev = stats.stdev(loser_results)
#         except stats.StatisticsError:
#             loser_stdev = 0
#
#         return {
#             'sample_set': self.sample_set,
#             'scorecard': results,
#             'mean': {
#                 'overall': mean,
#                 'winner': winner_mean,
#                 'loser': loser_mean
#             },
#             'stdev': {
#                 'overall': stdev,
#                 'winner': winner_stdev,
#                 'loser': loser_stdev
#             }
#         }
#
#     def reset(self):
#         self.predictions = []
#         self.sample_set = []
#         self.outcomes = []
#         self.clear_state()
#
#     def clear_state(self):
#         self._impending_pred = None
#         self._impending_sample = None
#
#     def evaluate(self):
#         if self._impending_pred:
#             self.predictions.append(self._impending_pred)
#             if self._impending_sample:
#                 self.sample_set.append(self._impending_sample)
#
#             self.clear_state()
#         else:
#             err = 'no impending predictions to evaluate'
#
#         err = self._sanity_check()
#         if err:
#             result = {
#                 'error': err,
#                 'result': 'error',
#                 'model': self.name
#             }
#         else:
#             result = {
#                 'win_loss_50_threshold': self._win_loss_percentage(.5),
#                 'win_loss_70_threshold': self._win_loss_percentage(.7),
#                 'loss_summary': self._win_loss_summary(),
#                 'result': 'success',
#                 'model': self.name
#             }
#
#         return result
#
#     def record_prediction(self, prediction, sample_set=None):
#         self._impending_pred = prediction
#         if sample_set:
#             self._impending_sample = sample_set
#
#     def record_outcome(self, outcome):
#         if self._impending_pred:
#             self.outcomes.append(outcome)


class TradingInterface(BitMEXInterface):
    def __init__(self, websocket_subscriptions, args, ordermanager=None, initial_data=None, config=None):
        if initial_data:
            self.candles, self.tstamps = initial_data()
        else:
            self.candles, self.tstamps = ([], [])
        self.args = args
        self.DRY_RUN = args.DRY_RUN
        self.history = []
        self.trackers = []
        self.client_ids = []
        self.i = 0
        self.strategy = Strategy(volume=100,
                                 consecutive_bars_until_trend_mode=4,
                                 no_of_counter_bars_until_retrace_mode=2,
                                 max_first_run_exposure=1200)
        self.renko_bar_manager = RenkoBarManager(
            initial_price=None,
            box_size=args.box_size,
            initial_direction=None
        )

        self.stream = environment.DataStream(records=['orderbook', 'trades', 'ticker'],
                                             path_save='{}/stream'.format(PROJECT_DIR))

        self.soft_limit_handler = None

        try:
            self.id = self.tstamps[-1]['id']
        except IndexError:
            self.id = 1

        self.eval = {}
        for algo in self.args.algos:
            model_data_file = '.modelevaluation.{}.pkl'.format(algo)
            restore = False
            if os.path.exists(model_data_file):
                response = input('would your like to restore {}? (y/n): '
                               .format(algo).lower().strip(' '))
                if response in ['y', 'yes', 'n', 'no']:
                    restore = True if response in ['y', 'yes'] else False
                else:
                    sys.exit(1)

            if model_data_file and restore:
                with open(model_data_file, 'rb') as f:
                    self.eval[algo] = pickle.load(f)
                    self.eval[algo].clear_state()
            else:
                self.eval[algo] = ModelEvaluation(algo)

        super().__init__(websocket_subscriptions, ordermanager=ordermanager, config=config)

    def reset_ledger(self):
        self.history = []
        super().reset_ledger()

    def set_soft_limit_switch(self, cur_price, trigger_price):
        if cur_price == trigger_price:
            raise ValueError('current price({}) cannot equal trigger price({})'.format(
                cur_price, trigger_price
            ))

        def callback_soft_limit_triggered(**kwargs):
            print('these kwargs: ', kwargs)
            logger.info('soft limit triggered ...')

            open_exposure = kwargs['open_exposure']

            CancelAllOrdersEvent()

            PendingOrderManager.update_cancel_order_events()

            if not open_exposure == 0:
                OrderPlaceEvent(prc=kwargs['reverse_order_price'],
                                qty=-open_exposure,
                                orderbook=kwargs['orderbook'],
                                callback=[
                                  ('onUpdate', callback_max_deviation_from_top_of_book(max_deviation=3,
                                  distance_from_top_of_book_on_price_amend=1))
                                ],
                                DRY_RUN=self.config.DRY_RUN
                )

            ReturnEvent({
                'type': 'action',
                'message': 'soft_limit_triggered',
                'direction': kwargs['direction'],
                'triggered': True
            })

            PendingOrderManager.update_cancel_order_events()

        def check_soft_limit_triggered(direction, trigger_price):
            def callback_func(**kwargs):
                current_price = kwargs.get('current_price')
                open_exposure = kwargs.get('open_exposure')

                if current_price:
                    if direction == 'LONG':
                        top_of_book = kwargs['orderbook']['asks'][0][0]
                        return current_price <= trigger_price, { 'open_exposure': open_exposure,
                            'direction': 'LONG', 'reverse_order_price': top_of_book + 0.5 }

                    if direction == 'SHORT':
                        top_of_book = kwargs['orderbook']['bids'][0][0]
                        return current_price >= trigger_price, { 'open_exposure': open_exposure,
                            'direction': 'SHORT', 'reverse_order_price': top_of_book - 0.5 }

                return False, {}

            return callback_func

        if self.soft_limit_handler:
            self.soft_limit_handler.deregister_handler()

        if trigger_price:
            logger.info('setting soft limit switch to {}'.format(trigger_price))

            name = '{}_soft_limit'.format(trigger_price)
            direction = 'LONG' if trigger_price < cur_price else 'SHORT'

            self.soft_limit_handler = CustomEvent.register(name,
                event_func=check_soft_limit_triggered(direction, trigger_price),
                callbacks=[callback_soft_limit_triggered])

    def run_algo(self, direction_encode, ticker, orderbook, total_volume):
        rel_candles, rel_tstamps = helper.toRelativeCandles(self.candles, self.tstamps)
        input_data = []
        for pair in list(zip(rel_candles, rel_tstamps)):
            input_data.extend(pair)

        logger.debug('new tick: {}'.format(self.get_tick()))

        logger.info('history: {}'.format(self.history))

        open_qty, event = self.strategy.execute(direction_encode, ticker, open_exposure=total_volume)
        if open_qty != 0:
            event['type'] = 'order'
            open_prc = orderbook['bids'][0][0] if open_qty > 0 else orderbook['asks'][0][0]
            runtime.OrderPlaceEvent(qty=open_qty, prc=open_prc, orderbook=orderbook, callback=[
                ('onUpdate', callback_max_deviation_from_top_of_book(max_deviation=3,
                                                distance_from_top_of_book_on_price_amend=1))],
                tags=['cancel_on_new_pending_order_placed'],
                DRY_RUN=self.config.DRY_RUN
            )
            PendingOrderManager.update_new_order_events(interface=self)

        vwap, total_volume, closed_pl = self.get_vwap_and_total_volume_and_closed_pl()
        logger.info('total_volume, vwap: ({}, {})'.format(total_volume, vwap))
        floating_pl = self.floating_pl(vwap, total_volume)

        fees_paid = self.fees_paid()

        logger.info('floating pl: {}'.format(floating_pl))
        logger.info('closed pl: {}'.format(closed_pl))
        logger.info('fees earned: {}'.format(fees_paid))
        logger.info('total pl: {}'.format(floating_pl + closed_pl + fees_paid))
        logger.info('-----------------------------')
        self.history.append(direction_encode)

        ReturnEvent({
            'type': 'account_state',
            'vwap': vwap,
            'total_volume': total_volume,
            'closed_pl': closed_pl,
            'floating_pl': floating_pl,
            'fees_paid': fees_paid
        })

        # events = [kwargs] if kwargs else []

        # return_value = (state, event)

        for algo in self.args.algos:
            algo_type, algo_name = algo.split('::')
            res = requests.post('http://localhost:5000/ml/{}/{}'.format(algo_type, algo_name), json=input_data)

            self.eval[algo].record_outcome(direction_encode)
            eval_result = self.eval[algo].evaluate()

            res_data = res.json()
            if res_data['result'] == 'success':
                self.eval[algo].record_prediction(res_data['data'], sample_set=input_data)

            if eval_result['result'] == 'success':
                # with open('analysis_data_1.csv', 'a') as f:
                #     data = eval_result['loss_summary']['scorecard'][-1]
                #     _,_,pred,out,_ = data
                #     f.write(','.join([str(i) for i in pred]) + ',' + str(out) + '\n')
                # with open('analysis_data_2.csv', 'a') as f:
                #     sample_set, data = next(zip(
                #         eval_result['loss_summary']['sample_set'][-2:],
                #         eval_result['loss_summary']['scorecard'][-1:]
                #     ))
                #     f.write(','.join([str(i) for i in sample_set[::2]]) + '\n')
                #     f.write(','.join([str(i) for i in sample_set[1::2]]) + '\n')
                #     _,_,pred,out,_ = data
                #     f.write(','.join([str(i) for i in pred]) + ',' + str(out) + '\n')
                # with open('analysis_data_3.csv', 'a') as f:
                #     sample_set, data = next(zip(
                #         eval_result['loss_summary']['sample_set'][-2:],
                #         eval_result['loss_summary']['scorecard'][-1:]
                #     ))
                #     f.write(','.join([str(i) for i in sample_set]))
                #     _,_,pred,out,_ = data
                #     f.write(',{}\n'.format(out))

                logger.info('summary: ')
                pprint(eval_result['loss_summary']['scorecard'])
                logger.info('win_loss_50: ')
                pprint(eval_result['win_loss_50_threshold'])
                logger.info('win_loss_70: ')
                pprint(eval_result['win_loss_70_threshold'])
                with open('.modelevaluation.{}.pkl'.format(algo), 'wb') as f:
                    pickle.dump(self.eval[algo], f)

            logger.debug(res_data)
            logger.info('----------------------------------------------------------')

        # return return_value

    def run_init(self, **kwargs):
        '''
        todo: raise exeception if history price deviates too much from current market price
        and raise an exception if too much time passed since last restart
        [HACK] fixes DataError when database is out of sync. Historical query used
          for ml algo. Not for trading algo
        '''
        logger.debug('setting PendingOrderManager.DRY_RUN={}'.format(self.args.DRY_RUN))

        PendingOrderManager.DRY_RUN = self.args.DRY_RUN

        if not kwargs.get('ignore_initial_candle'):
            price = self.manager.get_ticker()['last']

            print('[DEBUG] initial candles:', self.candles)

            try:
                if abs(price - self.candles[-1]) > self.args.box_size:
                    # raise DataError('historical price({}) deviates too much than market price({})'.format(
                    #         self.candles[-1], price
                    # ))
                    print('historical price({}) deviates too much than market price({})'.format(
                            self.candles[-1], price
                    ))
                    print('[ERROR][HACK] setting the last candle to the market price')
                    self.candles.append(price)

            except IndexError:
                self.candles.append(price)

        # if self.args.restore:
        #     try:
        #         with open('ml_1_state.pkl', 'rb') as f:
        #             state = pickle.load(f)
        #             self.strategy = state['strategy']
        #             self.history = state['history']
        #             self.ledger = state['ledger']
        #     except (FileNotFoundError, EOFError):
        #         pass


    def main(self, **kwargs):
        orderbook = kwargs['orderbook']
        trades = kwargs['trades']
        ticker = kwargs['ticker']
        execution = kwargs.get('execution')
        order = kwargs.get('order')
        position = kwargs.get('position')

        current_tick = ticker['last']
        vwap, open_exposure, closed_pl = self.get_vwap_and_total_volume_and_closed_pl()
        events = []
        return_value = {
            'events': [],
            'state': {}
        }

        # run all update handlers
        if self.soft_limit_handler:
            self.soft_limit_handler.set_params(
                current_price=current_tick,
                open_exposure=open_exposure,
                orderbook=orderbook
            )

        if self.DRY_RUN:
            pass
        else:
            results = self.manager.exchange.bitmex.http_orders(self.client_ids)
            order_manager_results = []
            remove = []
            for res in results:
                if res['ordStatus'] == 'Filled':
                    print('Order Filled: {}'.format(res))
                    order_manager_results.append((
                        'filled',
                        res['price'],
                        res['orderQty'],
                        {}
                    ))
                    self.ledger.extend([(res['orderQty'], res['price'], 'pending')])
                    remove.append(res)
                else:
                    print('Order {} Status: {}'.format(res['clOrdID'], res['ordStatus']))

            for i in remove:
                client_id = i['clOrdID']
                self.client_ids.remove(client_id)

        # for result in order_manager_results:
        #     state, price, qty, kwargs = result
        #
        #     events.append({
        #         'type': 'order',
        #         'order_state': state,
        #         'order_price': price,
        #         'order_quantity': qty
        #     })

        # --- check if hard close limit is hit
        # b_isTriggerd, hard_close_limit_price = self.check_hard_close_limit()
        #
        # if b_isTriggerd:
        #     ReturnEvent({
        #         'type': 'safety',
        #         'message': 'hard_close_limit_triggered',
        #         'price': hard_close_limit_price
        #     })
        #     print('Hard close limit triggered at {} !!!'.format(hard_close_limit_price))

        # --- update strategy
        result = self.renko_bar_manager.update(new_price=current_tick)
        if result:
            direction_encode = self.renko_bar_manager.direction_encode()

            self.run_algo(direction_encode, ticker, orderbook, open_exposure)

            ReturnEvent({
                'type': 'new_bar',
                'direction': self.renko_bar_manager.direction,
                'current_bar_price': self.renko_bar_manager.price,
                'next_up_bar_price': self.renko_bar_manager.next_up_price,
                'next_down_bar_price': self.renko_bar_manager.next_down_price
            })

            with open('ml_1_state.pkl', 'wb') as f:
                pickle.dump({'strategy': self.strategy,
                             'history': self.history,
                             'ledger': self.ledger }, f)
        else:
            if self.i % 12 == 0:
                vwap, total_volume, closed_pl = self.get_vwap_and_total_volume_and_closed_pl()
                logger.info('total_volume, vwap: ({}, {})'.format(total_volume, vwap))
                floating_pl = self.floating_pl(vwap, total_volume)

                fees_paid = self.fees_paid()

                logger.info('floating pl: {}'.format(floating_pl))
                logger.info('closed pl: {}'.format(closed_pl))
                logger.info('fees earned: {}'.format(fees_paid))
                logger.info('total pl: {}'.format(floating_pl + closed_pl + fees_paid))
                logger.info('-----------------------------')
                logger.info('total cancelled: {}'.format(len(PendingOrderManager.fetch_cancelled_orders())))
                logger.info('total filled: {}'.format(len(PendingOrderManager.fetch_filled_orders())))
                logger.info('total: {}'.format(len(PendingOrderManager.fetch_all_orders())))
                logger.info('last 20 orders:')
                if len(PendingOrderManager.pending) > 20:
                    pprint(PendingOrderManager.pending[len(PendingOrderManager.pending)-20:])
                else:
                    pprint(PendingOrderManager.pending)

                logger.info('-----------------------------')
                pprint(ticker)
                logger.info('-----------------------------')

        self.i += 1
        return ReturnEvent.flush()

    def run_main_loop(self, **kwargs):
        def format_trades(trades):
            ret_data = []
            for t in trades:
                if t['side'] == 'Buy':
                    mult = 1
                elif t['side'] == 'Sell':
                    mult = -1
                else:
                    raise Exception('unexpected side, t[\'side\'] = {0}'.format(t['side']))
                ret_data.append([t['timestamp'], t['price'], t['size'] * mult])

            return ret_data

        super().run_main_loop(**kwargs)

        try:
            data = kwargs['data']
        except KeyError:
            data = self.manager.exchange.flush_data()

        try:
            orderbook = kwargs['orderbook']
        except KeyError:
            orderbook = self.manager.exchange.get_orderbook()

        trades = format_trades(data['trade'])

        try:
            ticker = kwargs['ticker']
        except KeyError:
            ticker =  self.manager.get_ticker()


        execution = data.get('execution')
        order = data.get('order')
        position = data.get('position')

        events = self.engine(orderbook=orderbook, trades=trades, ticker=ticker,
            execution=execution, order=order, position=position,
            data = data, interface=self,
            DRY_RUN=self.config.DRY_RUN)

        if len(events) > 0:
            print('[INFO] new events:')
            print(events)

        # with open('mainstate.pkl', 'wb') as f:
        #     pickle.dump(self, f)

        # raise Exception('stop here ml_1')

    def run_deinit(self):
        pass
