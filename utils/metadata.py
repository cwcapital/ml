import os
import pathlib

__SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = pathlib.Path(__SCRIPT_DIR).parents[0]
