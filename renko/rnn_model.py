import tensorflow as tf
import numpy as np
# from tensorflow.contrib.rnn import MultiRNNCell
from tensorflow.contrib.layers import flatten, fully_connected, dropout, batch_norm
import os
from utils import DataLoader
import datetime as dt

def construct_graph(graph, args):
    with graph.as_default():
        X = tf.placeholder(tf.float32, shape=[None, args.time_steps, args.n_input], name='X')
        y = tf.placeholder(tf.int32, shape=[None], name='y')
        is_training = tf.placeholder(tf.bool, name='is_training')


        with tf.name_scope('rnn'):
            stacked_rnn = []
            for i in range(5):
                stacked_rnn.append(tf.nn.rnn_cell.GRUCell(args.num_units))

            cell = tf.nn.rnn_cell.MultiRNNCell(stacked_rnn)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=0.5)
            outputs, states = tf.nn.dynamic_rnn(cell, X, dtype="float32")

            final_state = states[-1]

            logits = fully_connected(final_state, args.n_output_classes, activation_fn=None)

        with tf.name_scope('loss'):
            xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=y, logits=logits
            )
            loss = tf.reduce_mean(xentropy)
            tf.summary.scalar('loss', loss)

        with tf.name_scope('analysis'):
            correct = tf.nn.in_top_k(logits, y, 1)
            accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name='accuracy')
            choices = tf.nn.softmax(logits)
            tf.summary.histogram('choices', choices)

        optimizer = tf.train.AdamOptimizer(learning_rate=args.learning_rate)
        training_op = optimizer.minimize(loss)
        merged = tf.summary.merge_all()

        tensors = {
            'training_op': training_op,
            'X': X,
            'y': y,
            'is_training': is_training,
            'choices': choices,
            'accuracy': accuracy,
            'loss': loss,
            'logits': logits
        }

        return tensors
