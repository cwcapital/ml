import unittest
import inspect

from trading.utils.managers import Manager, RenkoBarManager, PendingOrderManager
from trading.utils.events import RenkoBarEvent, OrderPlaceEvent, \
    CancelOrderEvent, OrderFilledEvent, OrderAmendEvent, Event, CancelAllOrdersEvent
from trading.utils.custom_events import CustomEvent
from trading.utils.entities import Order
from trading.utils import runtime
from trading.utils.callbacks import callback_max_deviation_from_top_of_book
from trading.utils.tests.helper.tags import display_test_name
from trading.utils.exceptions import InvalidOrderState

generic_orderbook = {
        'symbol': 'XBTUSD',
        'bids': [[8000, 13552],
            [7999, 525]],
        'asks': [[8001, 34714],
            [8002, 1725]],
        'timestamp': '2018-06-08T17:28:29.274Z'
}

class config:
    BASE_URL = "https://testnet.bitmex.com/api/v1/"
    API_KEY = "gCGylClnf_KlgByPdof2kCw-"
    API_SECRET = "eB_FKSayeiphYH-cO9vOmtY7H41hDwInm85I1j56vjqBDQ87"
    SYMBOL = "XBTUSD"
    DIGITS = 1
    DRY_RUN = True
    LOG_LEVEL = "INFO"
    MAX_POSITION = 1.5
    LOOP_INTERVAL = 0.5

class DummyManager(Manager):
    def update(self):
        pass

class TestPendingOrderTracker(unittest.TestCase):
    def setUp(self):
        PendingOrderManager.pending = []
        PendingOrderManager.DRY_RUN = True
        CustomEvent._queue = {}

    @display_test_name
    def test_buy_order_is_cancelled_after_second_order_is_opened(self):
        # Place a pending order
        order1 = runtime.OrderPlaceEvent(prc=8000, qty=100, orderbook=generic_orderbook,
                    tags=['cancel_on_new_pending_order_placed'])

        runtime.Engine.update(orderbook=generic_orderbook, trades=[])

        pending_orders = PendingOrderManager.fetch_non_filled_orders()
        self.assertEqual(len(pending_orders), 1)
        self.assertEqual(pending_orders[0].clOrdID, order1.clOrdID)

        # Place a second pending order
        order2 = OrderPlaceEvent(prc=8001, qty=-200, orderbook=generic_orderbook)

        # First order should canceled by CustomEvent.engine() <- called in runtime.Engine.update()
        runtime.Engine.update(orderbook=generic_orderbook, trades=[])

        pending_orders = PendingOrderManager.fetch_non_filled_orders()
        self.assertEqual(len(pending_orders), 1)
        self.assertEqual(pending_orders[0].clOrdID, order2.clOrdID)

        # Check that first order is actually cancelled
        cancelled_orders = PendingOrderManager.fetch_cancelled_orders()
        self.assertEqual(len(cancelled_orders), 1)
        self.assertEqual(cancelled_orders[0].clOrdID, order1.clOrdID)


    @display_test_name
    def test_buy_order_is_cancelled_after_threshold_is_crossed(self):
        def check_buy_limit_threshold_crossed(open_prc, threshold, clOrdID):
            def callback_func(**kwargs):
                trigger_prc = kwargs['trigger_prc']
                if trigger_prc >= open_prc + threshold:
                    return True, {
                        'clOrdID': clOrdID
                    }
                else:
                    return False, None

            return callback_func

        def callback_cancel_order(**kwargs):
            clOrdID = kwargs['clOrdID']
            CancelOrderEvent(clOrdID)
            PendingOrderManager.update_cancel_order_events()

        # Place a pending order
        order1 = OrderPlaceEvent(prc=8000, qty=100, orderbook=generic_orderbook)
        custom = CustomEvent.register(name='cancel_order_{}_event'.format(order1.clOrdID),
                                      event_func=check_buy_limit_threshold_crossed(
                                                        open_prc=8000,
                                                        threshold=5,
                                                        clOrdID=order1.clOrdID),
                                      callbacks=[callback_cancel_order])

        custom.set_params(trigger_prc=8004)

        runtime.Engine.update()

        pending_orders = PendingOrderManager.fetch_non_filled_orders()

        order1 = PendingOrderManager.get(clOrdID=order1.clOrdID)

        self.assertEqual(len(pending_orders), 1)
        self.assertEqual(pending_orders[0].clOrdID, order1.clOrdID)
        self.assertEqual(order1.ordStatus, 'New')

        # Trigger threshold cross
        custom.set_params(trigger_prc=8005)

        runtime.Engine.update()

        order1 = PendingOrderManager.get(clOrdID=order1.clOrdID)
        self.assertEqual(order1.ordStatus, 'Canceled')


    @display_test_name
    def test_sell_order_price_is_amended_when_deviates_ticker(self):
        orderbook_shift_down_by_ten = {
            'symbol': generic_orderbook['symbol'],
            'bids': [
                [generic_orderbook['bids'][0][0]-10, generic_orderbook['bids'][0][1]],
                [generic_orderbook['bids'][1][0]-10, generic_orderbook['bids'][1][1]]
            ],
            'asks': [
                [generic_orderbook['asks'][0][0]-10, generic_orderbook['bids'][0][1]],
                [generic_orderbook['asks'][1][0]-10, generic_orderbook['bids'][1][1]]
            ],
            'timestamp': generic_orderbook['timestamp']
        }

        order = OrderPlaceEvent(prc=8001, qty=-100, orderbook=generic_orderbook,
                        callback=[('onUpdate', callback_max_deviation_from_top_of_book(10, 3))])

        # update to assign new order to PendingOrderManager
        runtime.Engine.update(orderbook=generic_orderbook, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)

        self.assertEqual(order.price, 8001)

        runtime.Engine.update(orderbook=orderbook_shift_down_by_ten, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)
        self.assertEqual(order.price, 8001-10+3)

        orderbook_shift_down_by_twenty_two = {
            'symbol': generic_orderbook['symbol'],
            'bids': [
                [generic_orderbook['bids'][0][0]-22, generic_orderbook['bids'][0][1]],
                [generic_orderbook['bids'][1][0]-22, generic_orderbook['bids'][1][1]]
            ],
            'asks': [
                [generic_orderbook['asks'][0][0]-22, generic_orderbook['bids'][0][1]],
                [generic_orderbook['asks'][1][0]-22, generic_orderbook['bids'][1][1]]
            ],
            'timestamp': generic_orderbook['timestamp']
        }

        runtime.Engine.update(orderbook=orderbook_shift_down_by_twenty_two, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)
        self.assertEqual(order.price, 8001-22+3)

    @display_test_name
    def test_buy_order_price_is_amended_when_deviates_ticker(self):
        orderbook_shift_up_by_ten = {
            'symbol': generic_orderbook['symbol'],
            'bids': [
                [generic_orderbook['bids'][0][0]+10, generic_orderbook['bids'][0][1]],
                [generic_orderbook['bids'][1][0]+10, generic_orderbook['bids'][1][1]]
            ],
            'asks': [
                [generic_orderbook['asks'][0][0]+10, generic_orderbook['bids'][0][1]],
                [generic_orderbook['asks'][1][0]+10, generic_orderbook['bids'][1][1]]
            ],
            'timestamp': generic_orderbook['timestamp']
        }

        order = OrderPlaceEvent(prc=8000, qty=100, orderbook=generic_orderbook,
                        callback=[('onUpdate', callback_max_deviation_from_top_of_book(10, 3))])

        # update to assign new order to PendingOrderManager
        runtime.Engine.update(orderbook=generic_orderbook, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)

        self.assertEqual(order.price, 8000)

        runtime.Engine.update(orderbook=orderbook_shift_up_by_ten, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)
        self.assertEqual(order.price, 8000+10-3)

        orderbook_shift_up_by_twenty_two = {
            'symbol': generic_orderbook['symbol'],
            'bids': [
                [generic_orderbook['bids'][0][0]+22, generic_orderbook['bids'][0][1]],
                [generic_orderbook['bids'][1][0]+22, generic_orderbook['bids'][1][1]]
            ],
            'asks': [
                [generic_orderbook['asks'][0][0]+22, generic_orderbook['bids'][0][1]],
                [generic_orderbook['asks'][1][0]+22, generic_orderbook['bids'][1][1]]
            ],
            'timestamp': generic_orderbook['timestamp']
        }

        runtime.Engine.update(orderbook=orderbook_shift_up_by_twenty_two, trades=[])

        order = PendingOrderManager.get(clOrdID=order.clOrdID)
        self.assertEqual(order.price, 8000+22-3)

    @display_test_name
    def test_query_non_filled_orders(self):
        PendingOrderManager.add_pending(
          clOrdID='a',
          prc=1000,
          qty=10,
          ordType='pending',
          ordStatus='New',
          preceding_quantity=500,
          callback=None
        )
        PendingOrderManager.add_pending(
          clOrdID='b',
          prc=1000,
          qty=10,
          ordType='pending',
          ordStatus='Cancelled',
          preceding_quantity=500,
          callback=None
        )
        PendingOrderManager.add_pending(
          clOrdID='c',
          prc=1000,
          qty=10,
          ordType='pending',
          ordStatus='PartiallyFilled',
          preceding_quantity=500,
          callback=None
        )
        PendingOrderManager.add_pending(
          clOrdID='d',
          prc=1000,
          qty=10,
          ordType='pending',
          ordStatus='New',
          preceding_quantity=500,
          callback=None
        )
        PendingOrderManager.add_pending(
          clOrdID='e',
          prc=1000,
          qty=10,
          ordType='pending',
          ordStatus='Filled',
          preceding_quantity=500,
          callback=None
        )

        queried_orders = PendingOrderManager.fetch_non_filled_orders()

        self.assertEqual(len(queried_orders), 3)
        self.assertEqual(queried_orders[0].clOrdID, 'a')
        self.assertEqual(queried_orders[1].clOrdID, 'c')
        self.assertEqual(queried_orders[2].clOrdID, 'd')


class TestCheckForFilledOrders(unittest.TestCase):
    def setUp(self):
        Event.instances = {}
        PendingOrderManager.pending = []
        PendingOrderManager.DRY_RUN = True

    @display_test_name
    def test_buy_preceding_quantity(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7672.5, 1125],
                    [7672, 1025]],
                'asks': [[7675, 2525],
                    [7675.5, 895]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=888, prc=7672.5, orderbook=orderbook)
        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.preceding_quantity, 1125)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7672.5, 50],
            ['2018-06-08T17:30:23.571Z', 7672.5, -1000]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.preceding_quantity, 125)

    @display_test_name
    def test_sell_preceding_quantity(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7672.5, 1125],
                    [7672, 1025]],
                'asks': [[7675, 2525],
                    [7675.5, 895]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=-888, prc=7675, orderbook=orderbook)
        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.preceding_quantity, -2525)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7675, -400],
            ['2018-06-08T17:30:23.571Z', 7675, 1525]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.preceding_quantity, -1000)

    @display_test_name
    def test_buy_order_is_filled_when_top_of_book_bid_shifts_below_open_price(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7673.5, 13552],
                    [7673, 525],
                    [7672.5, 25],
                    [7672, 1025],
                    [7671.5, 25],
                    [7671, 1361],
                    [7670.5, 3025],
                    [7670, 3552],
                    [7669.5, 4525],
                    [7669, 4725]],
                'asks': [[7674, 34714],
                    [7674.5, 1725],
                    [7675, 2525],
                    [7675.5, 895],
                    [7676, 525],
                    [7676.5, 624],
                    [7677, 3722],
                    [7677.5, 375],
                    [7678, 25],
                    [7678.5, 25]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=888, prc=7673, orderbook=orderbook)

        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'New')

        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7672.5, 25],
                    [7672, 1025],
                    [7671.5, 25],
                    [7671, 1361],
                    [7670.5, 3025],
                    [7670, 3552],
                    [7669.5, 4525],
                    [7669, 4725],
                    [7668.5, 383],
                    [7668, 8890]],
                'asks': [[7674, 34714],
                    [7674.5, 1725],
                    [7675, 2525],
                    [7675.5, 895],
                    [7676, 525],
                    [7676.5, 624],
                    [7677, 3722],
                    [7677.5, 375],
                    [7678, 25],
                    [7678.5, 25]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'Filled')

    @display_test_name
    def test_sell_order_is_filled_when_top_of_book_ask_shifts_above_open_price(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7673.5, 13552],
                    [7673, 525],
                    [7672.5, 25],
                    [7672, 1025],
                    [7671.5, 25],
                    [7671, 1361],
                    [7670.5, 3025],
                    [7670, 3552],
                    [7669.5, 4525],
                    [7669, 4725]],
                'asks': [[7674, 34714],
                    [7674.5, 1725],
                    [7675, 2525],
                    [7675.5, 895],
                    [7676, 525],
                    [7676.5, 624],
                    [7677, 3722],
                    [7677.5, 375],
                    [7678, 25],
                    [7678.5, 25]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=-888, prc=7674.5, orderbook=orderbook)

        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'New')

        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7672.5, 25],
                    [7672, 1025],
                    [7671.5, 25],
                    [7671, 1361],
                    [7670.5, 3025],
                    [7670, 3552],
                    [7669.5, 4525],
                    [7669, 4725],
                    [7668.5, 383],
                    [7668, 8890]],
                'asks': [[7675, 2525],
                    [7675.5, 895],
                    [7676, 525],
                    [7676.5, 624],
                    [7677, 3722],
                    [7677.5, 375],
                    [7678, 25],
                    [7678.5, 25],
                    [7679, 123],
                    [7679.5, 889]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'Filled')

    @display_test_name
    def test_buy_order_is_filled_when_enough_trades_hit_the_orderbook(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7673.5, 13552],
                    [7673, 525]],
                'asks': [[7674, 34714],
                    [7674.5, 1725]],
                'timestamp': '2018-06-08T17:28:29.274Z'
        }

        order = OrderPlaceEvent(qty=888, prc=7673.5, orderbook=orderbook)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7673.5, -10000],
            ['2018-06-08T17:30:23.571Z', 7673.5, -3500]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.filled_quantity, 0)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7673.5, -50],
            ['2018-06-08T17:30:23.571Z', 7673.5, -50]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.filled_quantity, 48)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7673.5, -1000]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.filled_quantity, 888)

    @display_test_name
    def test_sell_order_is_filled_when_enough_trades_hit_the_orderbook(self):
        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[7673.5, 13552],
                    [7673, 525]],
                'asks': [[7674, 34714],
                    [7674.5, 1725]],
                'timestamp': '2018-06-08T17:28:29.274Z'
        }

        order = OrderPlaceEvent(qty=-888, prc=7674, orderbook=orderbook)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7674, 30000],
            ['2018-06-08T17:30:23.571Z', 7674, 3714]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.filled_quantity, 0)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7674, 1000],
            ['2018-06-08T17:30:23.571Z', 7674, 100]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.filled_quantity, -100)

        trades = [
            ['2018-06-08T17:30:23.571Z', 7674, 900]
        ]

        runtime.Engine.update(orderbook=orderbook, trades=trades)
        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.filled_quantity, -888)

    @display_test_name
    def test_buy_order_preceding_quantity_is_reduced_if_orderbook_volume_drops(self):
        orderbook = {
                    'symbol': 'XBTUSD',
                    'bids': [[7672.5, 1125],
                        [7672, 1025]],
                    'asks': [[7675, 2525],
                        [7675.5, 895]],
                    'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=888, prc=7672.5, orderbook=orderbook)
        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.preceding_quantity, 1125)

        orderbook = {
                    'symbol': 'XBTUSD',
                    'bids': [[7672.5, 925],
                        [7672, 1025]],
                    'asks': [[7675, 2525],
                        [7675.5, 895]],
                    'timestamp': '2018-06-08T17:31:29.274Z'
        }

        with self.assertLogs('root') as cm:
            runtime.Engine.update(orderbook=orderbook, trades=[])

        self.assertEqual(cm.output[0], 'INFO:root:orderbook drop: reducing preceding quantity from {} -> {}'.format(
            1125, 925
        ))

        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.preceding_quantity, 925)

    @display_test_name
    def test_sell_order_preceding_quantity_is_reduced_if_orderbook_volume_drops(self):
        orderbook = {
                    'symbol': 'XBTUSD',
                    'bids': [[7672.5, 1125],
                        [7672, 1025]],
                    'asks': [[7675, 2525],
                        [7675.5, 895]],
                    'timestamp': '2018-06-08T17:31:29.274Z'
        }

        order = OrderPlaceEvent(qty=-888, prc=7675, orderbook=orderbook)
        runtime.Engine.update(orderbook=orderbook, trades=[])
        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(order.preceding_quantity, -2525)

        orderbook = {
                    'symbol': 'XBTUSD',
                    'bids': [[7672.5, 925],
                        [7672, 1025]],
                    'asks': [[7675, 1500],
                        [7675.5, 895]],
                    'timestamp': '2018-06-08T17:31:29.274Z'
        }

        with self.assertLogs('root') as cm:
            runtime.Engine.update(orderbook=orderbook, trades=[])

        self.assertEqual(cm.output[0], 'INFO:root:orderbook drop: reducing preceding quantity from {} -> {}'.format(
            -2525, -1500
        ))

        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.preceding_quantity, -1500)

class TestCallbacks(unittest.TestCase):
    def setUp(self):
        PendingOrderManager.pending = []

    def test_only_cancelled_order_callback_triggers_on_cancelled_order(self):
        pass

    def test_only_filled_order_callback_triggers_on_cancelled_order(self):
        pass

    def test_only_amend_order_callback_triggers_on_cancelled_order(self):
        pass

class TestOrderManager(unittest.TestCase):
    def setUp(self):
        Event.reset()
        PendingOrderManager.reset()
        PendingOrderManager.DRY_RUN = True

    @display_test_name
    def test_get_exposure(self):
        self.assertEqual(PendingOrderManager.get_exposure(), 0)

        OrderPlaceEvent(qty=100, prc=None, orderbook=generic_orderbook)

        PendingOrderManager.update()

        self.assertEqual(PendingOrderManager.get_exposure(), 100)

        OrderPlaceEvent(qty=-200, prc=None, orderbook=generic_orderbook)

        PendingOrderManager.update()

        self.assertEqual(PendingOrderManager.get_exposure(), -100)

        # test pending buy order
        tob = generic_orderbook['bids'][0][0]
        pending_order = OrderPlaceEvent(qty=100, prc=tob, orderbook=generic_orderbook)

        PendingOrderManager.update()

        self.assertEqual(PendingOrderManager.get_exposure(), -100)

        OrderFilledEvent(pending_order.clOrdID, qty=100)

        PendingOrderManager.update()

        self.assertEqual(PendingOrderManager.get_exposure(), 0)

        # test pending sell order
        tob = generic_orderbook['asks'][0][0]
        pending_order = OrderPlaceEvent(qty=-100, prc=tob, orderbook=generic_orderbook)

        OrderFilledEvent(pending_order.clOrdID, qty=-100)

        PendingOrderManager.update()

        self.assertEqual(PendingOrderManager.get_exposure(), -100)

    @display_test_name
    def test_cancel_all_orders_event_successfully_cancels_all_non_filled_orders(self):
        order1 = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)
        order2 = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)
        order3 = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)

        runtime.Engine.update()

        self.assertEqual(order1.ordStatus, 'New')
        self.assertEqual(order2.ordStatus, 'New')
        self.assertEqual(order3.ordStatus, 'New')

        CancelAllOrdersEvent()

        runtime.Engine.update()

        order1 = PendingOrderManager.get(clOrdID=order1.clOrdID)
        order2 = PendingOrderManager.get(clOrdID=order2.clOrdID)
        order3 = PendingOrderManager.get(clOrdID=order3.clOrdID)

        self.assertEqual(order1.ordStatus, 'Canceled')
        self.assertEqual(order2.ordStatus, 'Canceled')
        self.assertEqual(order3.ordStatus, 'Canceled')

    @display_test_name
    def test_filled_orders_that_are_canceled_raises_an_exception(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)
        runtime.Engine.update()

        query = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(query.ordStatus, 'New')

        OrderFilledEvent(order.clOrdID, qty=100)
        runtime.Engine.update()

        query = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(query.ordStatus, 'Filled')

        with self.assertWarns(UserWarning) as cm:
            CancelOrderEvent(query.clOrdID)
            runtime.Engine.update()

        warning_msg = str(cm.warnings[0].message)
        self.assertEqual(warning_msg, '{} status cannot be changed from Filled -> Canceled'.format(
            query
        ))

    @display_test_name
    def test_amend_price_of_order(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)
        runtime.Engine.update()

        query = PendingOrderManager.get(order.clOrdID)
        self.assertIsNotNone(query)
        self.assertEqual(query.price, 8000)

        OrderAmendEvent(clOrdID=order.clOrdID, price=7999)
        with self.assertLogs('root') as cm:
            runtime.Engine.update()

        self.assertEqual(cm.output[0], 'INFO:root:Order {} price amended from {} -> {}'.format(
            order.clOrdID,
            8000,
            7999
        ))

        query = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(query.price, 7999)


    @display_test_name
    def test_pending_order_state_is_set_filled_when_order_filled_event_is_fired(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)
        OrderFilledEvent(clOrdID=order.clOrdID, qty=50)

        with self.assertLogs('root') as cm:
            runtime.Engine.update()

        self.assertEqual(cm.output[1], 'INFO:root:Order {} filled {}, filled/total={}/{}'.format(
            order.clOrdID,
            50,
            50,
            100
        ))

        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'PartiallyFilled')

        with self.assertLogs('root') as cm:
            OrderFilledEvent(clOrdID=order.clOrdID, qty=50)
            runtime.Engine.update()

        self.assertEqual(cm.output[0], 'INFO:root:Order {} filled {}, filled/total={}/{}'.format(
            order.clOrdID,
            50,
            100,
            100
        ))

        order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(order.ordStatus, 'Filled')


    @display_test_name
    def test_canceled_pending_order_state_is_set_canceled(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)

        with self.assertLogs('root') as cm:
            CancelOrderEvent(clOrdID=order.clOrdID)
            runtime.Engine.update()

        order = PendingOrderManager.get(order.clOrdID)

        self.assertEqual(cm.output[1], 'INFO:root:Order (\'{}\', {}, {}/{}, \'Canceled\') was successfully canceled'.format(
            order.clOrdID, order.price, order.filled_quantity, order.quantity))

        self.assertEqual(order.ordStatus, 'Canceled')


    @display_test_name
    def test_pending_order_state_is_set_to_new_when_order_is_placed(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)

        self.assertEqual(order.ordStatus, 'New')


    @display_test_name
    def test_pending_order_events_return_a_queryable_unique_id(self):
        order = OrderPlaceEvent(qty=100, prc=8000, orderbook=generic_orderbook)

        with self.assertLogs('root') as cm:
            runtime.Engine.update()

        self.assertEqual(cm.output[0], 'INFO:root:Placed pending order: (\'{}\', {}, 0/{}, \'{}\')'.format(
            order.clOrdID, order.price, order.quantity, order.ordStatus)
        )

        queried_order = PendingOrderManager.get(order.clOrdID)
        self.assertEqual(queried_order.quantity, 100)
        self.assertEqual(queried_order.price, 8000)
        self.assertEqual(queried_order.ordStatus, 'New')

    @display_test_name
    def test_place_order_event_places_order_in_order_queue(self):
        OrderPlaceEvent(qty=1000, prc=7999, orderbook=generic_orderbook)
        orders = OrderPlaceEvent.flush()
        self.assertEqual(len(orders), 1)
        order = orders[0]
        self.assertEqual(order.quantity, 1000)
        self.assertEqual(order.price, 7999)


class TestManagerClass(unittest.TestCase):
    def setUp(self):
        pass

    @display_test_name
    def test_has_an_update_method(self):
        manager = DummyManager()
        self.assertTrue(hasattr(manager, 'update'))
        self.assertTrue(inspect.ismethod(manager.update))

    @display_test_name
    def test_initial_state_is_active(self):
        manager = DummyManager()
        self.assertEqual(manager.state, 'active')

class TestRenkoBarMangaer(unittest.TestCase):
    def setUp(self):
        pass

    @display_test_name
    def test_initialization_without_initial_price(self):
        manager = RenkoBarManager(
            initial_price=None, box_size=5, initial_direction=None
        )

        update = manager.update(new_price=1007)

        self.assertEqual(manager.next_up_price, 1012)
        self.assertEqual(manager.next_down_price, 1002)


    @display_test_name
    def test_new_renko_down_bar_fires_a_corresponding_price_event(self):
        initial_prc = 1000
        box_size = 5
        prev_direction = 'UP'
        manager = RenkoBarManager(initial_prc, box_size, prev_direction)

        update = manager.update(new_price=1002)
        self.assertIsNone(update)

        update = manager.update(new_price=996)
        self.assertIsNone(update)

        update = manager.update(new_price=991)
        self.assertIsNone(update)

        update = manager.update(new_price=989)
        self.assertIsNotNone(update)

        e = RenkoBarEvent.flush()[0]
        self.assertEqual(update.description, e.description)
        self.assertEqual(e.prev_price, 1000)
        self.assertEqual(e.price, 990)

        self.assertEqual(manager.price, 990)
        self.assertEqual(manager.next_up_price, 1000)
        self.assertEqual(manager.next_down_price, 985)
        self.assertEqual(manager.direction, 'DOWN')


    @display_test_name
    def test_next_down_price_and_next_up_price_centers_around_new_price_gap(self):
        '''
        tests that if the new price gaps, next_up_price and next_down_price will be
        centered around the new price on the next renko bar
        '''
        initial_prc = 1000
        box_size = 5
        prev_direction = 'UP'
        manager = RenkoBarManager(initial_prc, box_size, prev_direction)

        self.assertEqual(manager.next_up_price, 1005)
        self.assertEqual(manager.next_down_price, 990)

        update = manager.update(new_price=975)
        self.assertIsNotNone(update)

        events = RenkoBarEvent.flush()
        self.assertEqual(len(events), 1)
        e = events[0]

        self.assertEqual(e.prev_price, 1000)
        self.assertEqual(e.price, 975)

        self.assertEqual(manager.price, 975)
        self.assertEqual(manager.direction, 'DOWN')
        self.assertEqual(manager.next_up_price, 985)
        self.assertEqual(manager.next_down_price, 970)

    @display_test_name
    def test_new_renko_up_bar_fires_a_corresponding_price_event(self):
        initial_prc = 1000
        box_size = 5
        prev_direction = 'UP'
        manager = RenkoBarManager(initial_prc, box_size, prev_direction)

        update = manager.update(new_price=1004)
        self.assertIsNone(update)

        update = manager.update(new_price=1006)
        self.assertIsNotNone(update)

        e = RenkoBarEvent.flush()[0]
        self.assertEqual(update.description, e.description)
        self.assertEqual(e.prev_price, 1000)
        self.assertEqual(e.price, 1005)

        self.assertEqual(manager.price, 1005)
        self.assertEqual(manager.next_up_price, 1010)
        self.assertEqual(manager.next_down_price, 995)
        self.assertEqual(manager.direction, 'UP')
