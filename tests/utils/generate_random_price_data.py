import datetime as dt
import random
from copy import deepcopy

class Timestamp:
    def __init__(self, initialTimestamp=None):
        if initialTimestamp is None:
            self.time = dt.datetime.now()
        elif type(initialTimestamp) == dt.datetime:
            self.time = initialTimestamp
        else:
            self.time = dt.datetime.strptime(initialTimestamp, '%Y-%m-%dT%H:%M:%S.%fZ')

    def get(self):
        return self.time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    def step_forward(self, seconds=1):
        self.time += dt.timedelta(seconds=seconds)

def generateTicker(orderbook, trades, prevTicker, **kwargs):
    def lastBuy():
        prev = None
        for _,p,v in trades:
            if v < 0:
                break
            else:
                prev = p
        return prev if prev else prevTicker['buy']

    def lastSell():
        if len(trades) > 0:
            price = trades[len(trades)-1][1]
            vol = trades[len(trades)-1][2]

            return price if vol < 0 else prevTicker['sell']

        return prevTicker['sell']

    orderbook = orderbook.copy()

    if kwargs.get('last_ticker_price'):
        last = kwargs['last_ticker_price']
    else:
        last = lastBuy() if random.randrange(2) == 0 else lastSell()

    return {
        'buy': orderbook['asks'][0][0],
        'sell': orderbook['bids'][0][0],
        'last': last
    }


def generateTrades(orderbook, timestamp):
    ts = timestamp.get()
    orderbook = orderbook.get()
    new_buy_vol = max(int(random.normalvariate(150000, 80000)), 0)
    new_sell_vol = max(int(random.normalvariate(150000, 80000)), 0)
    trades = []

    for i in range(len(orderbook['asks'])):
        if new_buy_vol <= 0:
            break
        p,v = orderbook['asks'][i]
        trade_vol = min(new_buy_vol, v)
        if trade_vol > 0:
            trades.append([ts, p, trade_vol])
            orderbook['asks'][i][1] -= trade_vol
            new_buy_vol -= trade_vol

    for i in range(len(orderbook['bids'])):
        if new_sell_vol <= 0:
            break
        p,v = orderbook['bids'][i]
        trade_vol = min(new_sell_vol, v)
        if trade_vol > 0:
            trades.append([ts, p, -trade_vol])
            orderbook['bids'][i][1] -= trade_vol
            new_sell_vol -= trade_vol

    return trades

class OrderBook:
    def newVolume(self):
        return random.randint(20000, 150000)

    def deltaVolume(self):
        return random.randint(10000, 80000)

    def __init__(self, initialBid, timestamp):
        self.create_orderbook(initialBid, timestamp)

    def create_orderbook(self, initialBid, timestamp):
        self.bids = []
        self.asks = []
        self.SPREAD = 2.0
        bidPrice = initialBid
        askPrice = initialBid + self.SPREAD

        for _ in range(10):
            self.bids.append([bidPrice, self.newVolume()])
            self.asks.append([askPrice, self.newVolume()])
            bidPrice -= 0.5
            askPrice += 0.5

        self.orderbook = {
            'timestamp': timestamp.get(),
            'bids': self.bids,
            'asks': self.asks
        }

    def update(self, initialBid, timestamp, reset_sensitivity, create_new_orderbook=False):
        if random.randrange(reset_sensitivity) == 0 or create_new_orderbook:
            return self.create_orderbook(initialBid, timestamp)

        bids = []
        for p,v in self.bids:
            if v <= 0:
                v = self.newVolume()
            elif random.randrange(3) == 0:
                v += self.deltaVolume()
                if v <= 0:
                    v = self.newVolume()
            else:
                pass

            bids.append([p,v])

        self.orderbook['bids'] = bids

        asks = []
        for p,v in self.asks:
            if v <= 0:
                v = self.newVolume()
            elif random.randrange(3) == 0:
                v += self.deltaVolume()
                if v <= 0:
                    v = self.newVolume()
            else:
                pass

            asks.append([p,v])

        self.orderbook['asks'] = asks

        self.orderbook['timestamp'] = timestamp.get()

    def get(self):
        return self.orderbook

    def copy(self):
        return deepcopy(self.orderbook)

class RandomPriceGenerator:
    def __init__(self, pivot_prices, n_random_fluctuations, variance,
    max_deviation=None, rounding=2, distribution='normalvariate'):
        self.pivot_prices = pivot_prices
        self.n_random_fluctuations = n_random_fluctuations
        self.random_func = getattr(random, distribution)
        self.index = 1
        self.counter = 1
        self.variance = variance
        self.max_deviation = max_deviation
        self.rounding = rounding
        self.b_first_iteration = True
        self.b_terminated = False

    def __next__(self):
        current_value = self.pivot_prices[self.index-1]

        if self.b_terminated:
            raise StopIteration
        try:
            next_value = self.pivot_prices[self.index]
        except IndexError:
            self.b_terminated = True
            return self.pivot_prices[self.index-1]

        if self.n_random_fluctuations > 0:
            ticker = (self.counter-1) % self.n_random_fluctuations
            if ticker == 0:
                # print('ticker == 0')
                return_value = self.pivot_prices[self.index]
                self.index += 1
            else:
                weight = ticker / self.n_random_fluctuations

                mean_value = (current_value * (1-weight)) + (next_value * weight)
                standard_dev = 0.1
                args = [mean_value, self.variance]

                return_value = self.random_func(*args)
                # print('we: ', weight, 'me: ', mean_value, 're: ', return_value)
            if self.max_deviation is not None:
                if return_value < current_value:
                    max_return_value = current_value - self.max_deviation
                    return_value = max(return_value, max_return_value)
                else:
                    max_return_value = current_value + self.max_deviation
                    return_value = min(return_value, max_return_value)
        else:
            self.index += 1
            return_value = current_value

        return_value = round(return_value, self.rounding) if self.rounding is not None else return_value

        self.counter += 1
        # print('current: ', current_value, ' ... ', ' return: ', return_value)
        return return_value

    def __iter__(self):
        return self

class RandomizedEnvironmentGenerator:
    def __init__(self, pivot_price_list, initial_timestamp,
             n_random_fluctuations, variance, max_deviation,
             rounding, randomize_ticker_last_price=True,
             orderbook_reset_sensitivity=12):
        '''
        n_random_fluctuations := number of random fluctuations in between each
            two pivot prices
        orderbook_reset_sensitivity := determines the probability that the orderbook
            is reset to match new random prices. orderbook_reset_sensitivity=1 will
            reset on each iteration
        max_deviation := maximum deviation a randomly generated price can
            deviate from a pivot price
        '''
        assert len(pivot_price_list) > 1, 'cannot generate from singleton list'

        self.orderbook_list = []
        self.trades_list = []
        self.ticker_list = []
        self.timestamp = Timestamp(initial_timestamp)
        self.randomize_ticker_last_price = randomize_ticker_last_price
        self.counter = 0
        self.ticker = {
            'buy': pivot_price_list[0],
            'sell': pivot_price_list[0] + 0.5,
            'last': pivot_price_list[0]
        }
        self.orderbook = OrderBook(pivot_price_list[0], self.timestamp)

        self.pivot_price_list = pivot_price_list
        self.n_random_fluctuations = n_random_fluctuations
        self.orderbook_reset_sensitivity = orderbook_reset_sensitivity
        self.variance = variance
        self.max_deviation = max_deviation
        self.rounding = rounding

        self.prices = RandomPriceGenerator(pivot_price_list,
                    n_random_fluctuations,
                    variance,
                    max_deviation,
                    rounding)

        self.pivot_index = 1

    def __next__(self):
        price = next(self.prices)

        next_pivot = self.pivot_price_list[self.pivot_index]

        if price == next_pivot:
            if self.pivot_index < len(self.pivot_price_list) - 1:
                self.pivot_index += 1
            b_create_new_orderbook = True
        else:
            b_create_new_orderbook = False

        self.orderbook.update(price, self.timestamp, self.orderbook_reset_sensitivity, create_new_orderbook=b_create_new_orderbook)

        orderbook = self.orderbook.copy()

        trades = generateTrades(self.orderbook, self.timestamp)

        if self.randomize_ticker_last_price:
            self.ticker = generateTicker(self.orderbook, trades, self.ticker)
        else:
            self.ticker = generateTicker(self.orderbook, trades, self.ticker,
                        last_ticker_price=price)

        self.timestamp.step_forward()

        return orderbook, trades, self.ticker

    def __iter__(self):
        return self
