import warnings
from copy import copy

class CustomEvent:
    _queue = {}

    def __init__(self, name, event_func, callback_list):
        self.name = name
        self.event_func = event_func
        self.event_func_params = {}
        self.callback_list = callback_list

    def set_params(self, **kwargs):
        self.event_func_params = copy(kwargs)

    @classmethod
    def find(cls, name):
        try:
            return cls._queue[name]
        except KeyError:
            warnings.warn('Unable to find custom_event {}, reason=could not find name'.format(name))
            return None

    @classmethod
    def register(cls, name, event_func, callbacks):
        event = cls(name, event_func, callbacks)
        cls._queue[name] = event
        return event

    def deregister_handler(self):
        del self.__class__._queue[self.name]

    @classmethod
    def deregister(cls, name):
        try:
            del cls._queue[name]
        except KeyError:
            warnings.warn('Unable to deregister custom_event {}, reason=could not find name'.format(name))

    @classmethod
    def engine(cls, **upstream):
        triggered = []
        for _,e in cls._queue.items():
            result, kwargs = e.event_func(**e.event_func_params)
            if result:
                for callback in e.callback_list:
                    callback(**kwargs, **upstream)
                triggered.append(e.name)

        for event_name in triggered:
            cls.deregister(event_name)

        return triggered
