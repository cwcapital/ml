python candles.py ticks/processed/bitmex/xbtusd/ --box_size=12 --n_inputs=20
--n_outputs=1 --verbose=false --output_file_size=20 --validation_split=0.1
--include-dates=true

... was able to achieve validation accuracy 80%. big change was changing
the learning rate down to 0.0001 from 0.01

including time elapsed between renko bars also seemed to have a positive effect
