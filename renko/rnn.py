import tensorflow as tf
from tensorflow.contrib.layers import fully_connected, dropout
from datetime import datetime
from utils import DataLoader, AccuracyMeasurer, str2bool, save_to_csv, flatten
import numpy
import argparse
import sys
from pprint import pprint
from rnn_model import construct_graph

numpy.set_printoptions(threshold=numpy.nan)

parser = argparse.ArgumentParser(description='Renko candle recurrent neural network')
parser.add_argument('--n_input', type=int, required=True,  help='number of input candles')
parser.add_argument('--time_steps', type=int, required=True,  help='number of time steps to unroll the rnn')
parser.add_argument('--n_output_classes', type=int, required=True, help='number of output classes(possibilites)')
parser.add_argument('--num_units', type=int, required=True,  help='number of neuron cell units')
parser.add_argument('--forget_bias', type=float, required=True, help='determines rate network forgets old information during learning')
parser.add_argument('--keep_prob', type=float, required=True, help='determines network dropout rate during learning')
parser.add_argument('--batch_size', type=int, required=True, help='number of instances feed to network')
parser.add_argument('--learning_rate', type=float, required=True, help='neural network learning rate')
parser.add_argument('--n_epochs', type=int, required=True, help='number of training cycles')
parser.add_argument('--n_epochs_save', type=int, required=True, help='number of epochs before evaluating and saving model')
# parser.add_argument('--accuracy_threshold', type=float, required=True, help='threshold that a prediction must be above to be considered correct, when measuring model accuracy')
parser.add_argument('--restore', type=str2bool, required=True, help='specifies to initialize a new model or restore from checkpoint')
parser.add_argument('--restoreDir')
parser.add_argument('--dataInputDir')
parser.add_argument('--save_validation_to_csv', type=str2bool, required=True, help='saves validation results to csv file located in analysis/')
parser.add_argument('--with_x_batch', type=str2bool, required=False, default=False)
parser.add_argument('--shuffle_data', type=str2bool, required=False, default=False)
args = parser.parse_args()

if not args.restore:
    res = input('WARNING: initializing a new model will overwrite any previously saved checkout. Continue (y/n)? ')
    if res.lower().strip(' ') not in ['y', 'yes']:
        sys.exit(1)

graph = tf.Graph()
tensors = construct_graph(graph, args)
training_op = tensors['training_op']
X = tensors['X']
y = tensors['y']
is_training = tensors['is_training']
choices = tensors['choices']
accuracy = tensors['accuracy']
loss = tensors['loss']
logits = tensors['logits']

now = datetime.utcnow().strftime('%Y%m%d%H%M%S')
root_logdir = 'tf_logs'
logdir = "{}/run-{}".format(root_logdir, now)

max_reuse_count = args.n_epochs_save if args.shuffle_data else None
dataloader = DataLoader(args.dataInputDir, max_files_opened=None,
   max_reuse_count_before_data_reload=max_reuse_count,
   batch_size=args.batch_size, shuffle_data=True,
   random_batch_selection=False, unstack=2,
   time_steps=args.time_steps, DEBUG=False)

with graph.as_default():
    merged = tf.summary.merge_all()
    file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())
    saver = tf.train.Saver()

    try:
        with tf.Session() as sess:
            # if not os.path.exists(args.restoreDir):
                # response = input('Restore previous model (y/n)')
                # if response.lower().strip(' ') not in ['y', 'yes']:

            if args.restore:
                print('restoring model from {}'.format(args.restoreDir))
                saver.restore(sess, args.restoreDir)
            else:
                tf.global_variables_initializer().run()

            for epoch in range(args.n_epochs):
                X_batch, y_batch = dataloader.next_batch()
                sess.run(training_op, feed_dict={X: X_batch, y: y_batch, is_training: True})

                if epoch % args.n_epochs_save == 0:
                    acc_train = accuracy.eval(feed_dict={X: X_batch,  y: y_batch, is_training: False})
                    choice_dist = choices.eval(feed_dict={X: X_batch, is_training: False})

                    print('test:')
                    print('----------')
                    print(choice_dist[-10:])
                    print('----------')
                    print(y_batch[-10:])

                    print(epoch, 'Test accuracy:', acc_train)


                if epoch % args.n_epochs_save == 0:
                    # val_data, val_label = dataloader.validation_data
                    val_data, val_label = dataloader.next_validation_batch()
                    acc_train = accuracy.eval(feed_dict={X: val_data, y: val_label, is_training: False})
                    choice_dist = choices.eval(feed_dict={X: val_data, is_training: False})

                    # print('validation:')
                    # print('----------')
                    # print(choice_dist[-10:])
                    # print('----------')
                    # print(val_label[-10:])

                    print(epoch, 'Validation accuracy:', acc_train)


                if epoch % args.n_epochs_save == 0:
                    summary, _ = sess.run([merged, loss], feed_dict={X: val_data, y: val_label, is_training: False})
                    file_writer.add_summary(summary, epoch)

                    if args.save_validation_to_csv:
                        kwargs = {}
                        if args.with_x_batch:
                            kwargs = {'X_batch': X_batch}
                        kwargs['model'] = True
                        kwargs['excel'] = True
                        kwargs['model_type'] = 'rnn'
                        save_to_csv(choice_dist, val_data, epoch, **kwargs)

                    if args.restoreDir:
                        saver.save(sess, args.restoreDir)

            if args.restoreDir:
                saver.save(sess, args.restoreDir)

    finally:
        file_writer.close()
