import os
from utils import ticksToRenko, Tick, identity_category, toRelativeCandles, str2bool
import datetime as dt
import argparse
from candles import createTestSet, save_to_file

parser = argparse.ArgumentParser(description='parses ticks to renko candlesticks')
parser.add_argument('path', help='path to directory of processed ticks')
parser.add_argument('--n_input', required=True, type=int)
parser.add_argument('--n_candles_out', required=True, type=int)
parser.add_argument('--box_size', required=True, type=int)
parser.add_argument('--outDir', required=False)
parser.add_argument('--data_validation_split', required=True, type=float)
parser.add_argument('--verbose', type=str2bool, required=False, default=False)
args = parser.parse_args()

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PATH = os.path.join(SCRIPT_DIR, args.path.rstrip('/'))

def fileStartDate(name):
    start_date, _ = name.split('_')
    return dt.datetime.strptime(start_date, '%Y-%m-%d-%H-%M-%S')

def fileEndDate(name):
    _, end_date = name.split('.')[0].split('_')
    return dt.datetime.strptime(end_date, '%Y-%m-%d-%H-%M-%S')

if args.outDir:
    PROCESSED_PATH = os.path.join(SCRIPT_DIR, '/'.join(args.path.split('/')), args.outDir)
else:
    PROCESSED_PATH = os.path.join(SCRIPT_DIR, '/'.join(args.path.split('/')))

if not os.path.exists(PROCESSED_PATH):
    os.makedirs(PROCESSED_PATH)

test_destination_path = os.path.join(SCRIPT_DIR, 'ticks', 'candles', args.outDir, 'test')
if not os.path.exists(test_destination_path):
    os.makedirs(test_destination_path)

validation_destination_path = os.path.join(SCRIPT_DIR, 'ticks', 'candles', args.outDir, 'validation')
if not os.path.exists(validation_destination_path):
    os.makedirs(validation_destination_path)

map_of_files = {}
for (dirpath, dirnames, filenames) in os.walk(PATH):
    for filename in filenames:
        if filename.endswith('.csv'):
            map_of_files[filename] = os.sep.join([dirpath, filename])

keys_sorted = sorted(map_of_files.keys(), key=fileStartDate)

prev_date = None
data = ''

f_start_date = fileStartDate(keys_sorted[0])

candle_list = []
tstamp_list = []

for key in keys_sorted:
    prev_candle = None
    max_val = 0

    fname = map_of_files[key]
    with open(fname) as f:
        candles, timestamps = ticksToRenko(f, args.box_size, Tick)

    if args.verbose:
        print('process file: ', fname)

    rel_candles, rel_tstamps = toRelativeCandles(candles, timestamps)
    candle_list.extend(rel_candles)
    tstamp_list.extend(rel_tstamps)


test_set, validation_set =  createTestSet(candle_list, args.n_input, args.n_candles_out, data_validation_split=args.data_validation_split,
    rel_tstamps=tstamp_list, categoryGroup='identity_category')

save_to_file(test_set, validation_set, args.outDir,
                    output_file_size=1000,
                    SCRIPT_DIR=SCRIPT_DIR,
                    META={
                        'n_input_nodes': args.n_input,
                        'n_candles_out': args.n_candles_out,
                        'box_size': args.box_size,
                        'type': 'rnn_sequence'
                    },
                    VERBOSE=False)
