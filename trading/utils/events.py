from copy import copy
import uuid
from .custom_events import CustomEvent
from .exceptions import InvalidLimit, InvalidBuyLimit, InvalidSellLimit
import warnings
import inspect

class Event:
    instances = {}

    @classmethod
    def get_all_instances(cls):
        return cls.instances

    @classmethod
    def get_instances(cls, type):
        try:
            return cls.instances[type]
        except KeyError:
            return []

    @classmethod
    def reset(cls):
        cls.instances = {}

    def __init__(self, type):
        self.type = type
        self._visable = True
        try:
            type_list = self.__class__.instances[type]
        except KeyError:
            self.__class__.instances[type] = []
            type_list = self.__class__.instances[type]

        type_list.append(self)

    def __repr__(self):
        return self.type

class OrderAmendEvent(Event):
    def __init__(self, clOrdID, price):
        self.clOrdID = clOrdID
        self.price = price
        super().__init__('order_amend')

    def __repr__(self):
        return 'order_amend {}, price={}'.format(
            self.clOrdID,
            self.price
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('order_amend', []))
        cls.instances['order_amend'] = []
        return temp

class ReturnEvent(Event):
    def __init__(self, eventDict):
        self.eventDict = eventDict
        super().__init__('return_events')

    def __repr__(self):
        return 'return event: {}'.format(
            self.eventDict
        )

    def __getitem__(self, x):
        return self.eventDict[x]

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('return_events', []))
        cls.instances['return_events'] = []
        return temp

class OrderFilledEvent(Event):
    def __init__(self, clOrdID, qty, **kwargs):
        self.clOrdID = clOrdID
        self.qty = qty
        self.kwargs = kwargs
        super().__init__('order_filled')

    def __repr__(self):
        return 'order_filled {}, amt={}'.format(
            self.clOrdID,
            self.qty
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('order_filled', []))
        cls.instances['order_filled'] = []
        return temp

class CancelOrderEvent(Event):
    def __init__(self, clOrdID):
        self.clOrdID = clOrdID
        super().__init__('cancel_order')

    def __repr__(self):
        return 'cancel_order {}'.format(
            self.clOrdID
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('cancel_order', []))
        cls.instances['cancel_order'] = []
        return temp

class CancelAllOrdersEvent(Event):
    def __init__(self):
        super().__init__('cancel_all_orders')

    def __repr__(self):
        return 'cancel_all_orders'

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('cancel_all_orders', []))
        cls.instances['cancel_all_orders'] = []
        return temp

class OrderPlaceEvent(Event):
    '''
    set prc = 0 to place a market order
    '''
    def _preceding_quantity(self, orderbook, side):
        if side == 'bids':
            for p,v in orderbook['bids']:
                if p == self.price:
                    return v
            orderbook_snapshot = 'orderbook[bids] = {}'.format(orderbook['bids'])

        if side == 'asks':
            for p,v in orderbook['asks']:
                if p == self.price:
                    return -v
            orderbook_snapshot = 'orderbook[asks] = {}'.format(orderbook['asks'])

        warnings.warn('could not find preceding quantity for prc {}\n{}'.format(
            self.price, orderbook_snapshot))

        return 0

    def __init__(self, qty, prc, orderbook=None, callback=None, tags=None, DRY_RUN=True, **kwargs):
        '''
        tags [list]: adds additional features to pending orders, fulfilled by a CustomEvent instance
            cancel_on_new_pending_order_placed:
                cancels the pending order if an additional pending order is placed
        '''
        self.quantity = qty
        self.price = prc
        self.clOrdID = str(uuid.uuid4())
        self.callback = callback
        self.preceding_quantity = None

        if DRY_RUN:
            if self.price:
                if not orderbook:
                    raise InvalidLimit('orderbook must be passed if price is not None')
                self.ordType = 'pending'
                self.ordStatus = 'New'
            else:
                self.ordType = 'market'
                self.ordStatus = 'WaitingToFillMarket'

            if orderbook:
                top_of_book_bid, _ = orderbook['bids'][0]
                top_of_book_ask, _ = orderbook['asks'][0]

                if self.quantity > 0:
                    if prc:
                        if prc > top_of_book_bid:
                            raise InvalidBuyLimit('prc {} must be <= top_of_book_bid({})'.format(
                                prc, top_of_book_bid
                            ))
                        self.preceding_quantity = self._preceding_quantity(orderbook, 'bids')
                    else:
                        prc = top_of_book_ask

                if self.quantity < 0:
                    if prc:
                        if prc < top_of_book_ask:
                            raise InvalidSellLimit('prc {} must be >= top_of_book_ask({})'.format(
                                prc, top_of_book_ask
                            ))
                        self.preceding_quantity = self._preceding_quantity(orderbook, 'asks')
                    else:
                        prc = top_of_book_bid
        else:
            self.price = prc
            self.ordType = 'pending' if prc else 'market'
            self.ordStatus = 'New' if self.ordType == 'pending' else 'WaitingToFillMarket'
        if tags:
            for tag in tags:
                if tag == 'cancel_on_new_pending_order_placed':
                    try:
                        CustomEvent.register(name='cancel_{}_on_new_pending_order_placed'.format(self.clOrdID),
                                     event_func=kwargs['check_new_order_opened'](self.clOrdID),
                                     callbacks=[kwargs['callback_cancel_order']])
                    except KeyError as e:
                        if e.args[0] == "check_new_order_opened":
                            err_msg = 'can only be used in trading.utils.runtime.OrderPlaceEvent'
                        raise KeyError('{} tag {}'.format(e.args[0], err_msg))
                else:
                    raise InvalidLimit('unrecognized tag: {}'.format(tag))

        super().__init__('place_order')

    def __repr__(self):
        return 'order @ ({}, {}, {}, {})'.format(
            self.clOrdID, self.price, self.quantity, self.ordStatus
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('place_order', []))
        cls.instances['place_order'] = []
        return temp

class PriceEvent(Event):
    def __init__(self, description, price):
        self.description = description
        self.price = price
        super().__init__('price')

    def __repr__(self):
        return '{} @ {}'.format(
            self.description, self.price
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('price', []))
        cls.instances['price'] = []
        return temp

class TradeEvent(Event):
    def __init__(self, uuid, description):
        self.uuid = uuid
        self.description = description
        super().__init__('trade')

    def __repr__(self):
        return '#{}: {}'.format(
            self.uuid,
            self.description
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('trade', []))
        cls.instances['trade'] = []
        return temp

class RenkoBarEvent(Event):
    def __init__(self, desc, price, prev_price, direction):
        self.description = desc
        self.price = price
        self.prev_price = prev_price
        self.direction = direction

        super().__init__('renko_bar')

    def __repr__(self):
        return '{} ({}): {} -> {}'.format(
            self.description,
            self.direction,
            self.prev_price,
            self.price
        )

    @classmethod
    def flush(cls):
        temp = copy(cls.instances.get('renko_bar', []))
        cls.instances['renko_bar'] = []
        return temp
