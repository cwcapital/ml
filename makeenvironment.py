import datetime as dt
import random
import json
from utils.generate_random_price_data import RandomPriceGenerator, Timestamp, OrderBook

'''
Simulates a series of orderbooks, trades, and tickers, given a list of prices that the market must cross
'''


filename = 'make_overall_data.json'

random.seed(99)

timestamp = Timestamp('2018-04-20T04:18:15.076Z')
orderbook = OrderBook(8275, timestamp)
ticker = {
    'buy': 8275,
    'sell': 8275.5,
    'last': 8275
}

price_list = RandomPriceGenerator([8276, 8285, 8260, 8295, 8290, 8305],
                n_random_fluctuations=20,
                variance=3,
                max_deviation=None,
                rounding=2)

orderbook_list = []
trades_list = []
ticker_list = []

counter = 0
for p in price_list:
    orderbook.update(p, timestamp, create_new_orderbook=True)
    orderbook_list.append(orderbook.get())

    trades = generateTrades(orderbook, timestamp)
    trades_list.append(trades)

    ticker = generateTicker(orderbook, trades, ticker)
    ticker_list.append(ticker)

    timestamp.step_forward()
    counter += 1

with open(filename, 'w') as f:
    data = {
        'trades': trades_list,
        'orderbook': orderbook_list,
        'ticker': ticker_list
    }
    f.write(json.dumps(data, indent=4, sort_keys=True))

print('saved file to: {}'.format(filename))
print('finished: created {} orderbook, trade and ticker objects'.format(counter))
