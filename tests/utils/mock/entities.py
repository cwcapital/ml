import datetime as dt
from dateutil.tz import tzutc

class OrderManager:
    def __init__(self, run_init, run_main_loop, run_deinit, orderbook_type, **kwargs):
        self.exchange = None
        self.run_init = run_init
        self.run_main_loop = run_main_loop
        self.run_deinit = run_deinit

    def init(self):
        self.start_time = dt.datetime.now()
        self.starting_qty = 0
        self.running_qty = 0
        self.ticker = None

    def set_ticker(self, ticker):
        self.ticker = ticker

    def get_ticker(self):
        return self.ticker

class MockManager:
    def __init__(self):
        pass

def fetch_initial_data(candle_list):
    def _func():
        candles = candle_list
        tstamps = []
        prev_tstamp = dt.datetime(2018, 1, 1, tzinfo=tzutc())
        for _ in candles:
            tstamps.append({
                'id': 1, 'tstamp': prev_tstamp + dt.timedelta(hours=1)
            })

        return candles, tstamps

    return _func
