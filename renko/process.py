import argparse
import datetime as dt
import os
import sys
from utils import str2bool

def fileStartDate(name):
    start_date, _ = name.split('_')
    return dt.datetime.strptime(start_date, '%Y-%m-%d-%H-%M-%S')

def fileEndDate(name):
    _, end_date = name.split('.')[0].split('_')
    return dt.datetime.strptime(end_date, '%Y-%m-%d-%H-%M-%S')

parser = argparse.ArgumentParser(description='Download trades from report server')
parser.add_argument('path', help='path to directory of raw ticks')
parser.add_argument('gap', help='defines a gap between ticks in seconds')
parser.add_argument('--verbose', type=str2bool, required=False)
parser.add_argument('--dry-run', type=str2bool, required=False)
args = parser.parse_args()

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
MAX_GAP_IN_SECS = int(args.gap)
PATH = os.path.join(SCRIPT_DIR, 'ticks', 'raw', args.path.rstrip('/'))
PROCESSED_PATH = os.path.join(SCRIPT_DIR, 'ticks', 'processed', '/'.join(args.path.split('/')))

if not os.path.exists(PROCESSED_PATH):
    os.makedirs(PROCESSED_PATH)

map_of_files = {}
for (dirpath, dirnames, filenames) in os.walk(PATH):
    for filename in filenames:
        if filename.endswith('.csv'):
            map_of_files[filename] = os.sep.join([dirpath, filename])

keys_sorted = sorted(map_of_files.keys(), key=fileStartDate)

prev_date = None
data = ''

f_start_date = fileStartDate(keys_sorted[0])
for key in keys_sorted:
    fname = map_of_files[key]
    with open(fname) as f:
        for line in f.readlines():
            date,bid,ask = line.split(',')
            d = dt.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')

            if prev_date and (d - prev_date).seconds >= MAX_GAP_IN_SECS:

                if args.dry_run:
                    print('split file between {0} and {1}'.format(
                        prev_date.strftime('%Y-%m-%d %H:%M:%S'),
                        d.strftime('%Y-%m-%d %H:%M:%S')
                    ))
                else:
                    if args.verbose:
                        print('splitting file between {0} and {1}'.format(
                            prev_date.strftime('%Y-%m-%d %H:%M:%S'),
                            d.strftime('%Y-%m-%d %H:%M:%S')
                        ))
                    rf = '{0}_{1}.csv'.format(
                        f_start_date.strftime('%Y-%m-%d-%H-%M-%S'),
                        prev_date.strftime('%Y-%m-%d-%H-%M-%S'))
                    with open(os.path.join(PROCESSED_PATH, rf), 'w') as f:
                        f.write(data)
                    f_start_date = d
                    data = ''

            data += line
            prev_date = d

# Write remaining data to file
if data and not args.dry_run:
    rf = '{0}_{1}.csv'.format(
        f_start_date.strftime('%Y-%m-%d-%H-%M-%S'),
        fileEndDate(keys_sorted[-1]).strftime('%Y-%m-%d-%H-%M-%S'))
    with open(os.path.join(PROCESSED_PATH, rf), 'w') as f:
        f.write(data)
    data = ''
