
Edit config: config.json
LOG_LEVEL. Available levels: logging.(DEBUG|INFO|WARN|ERROR)

$ sudo apt-get install -y virtualenv

$ virtualenv -p python3 .env

If you see the following error:
locale.Error: unsupported locale setting, please run the following 3 commands:

$ export LC_ALL="en_US.UTF-8"

$ export LC_CTYPE="en_US.UTF-8"

$ sudo dpkg-reconfigure locales

Install python pip requirements:

$ .env/bin/pip install -r requirements.txt

Bot execution strategy lives in main.py

Run program:

$ .env/bin/python run.py
