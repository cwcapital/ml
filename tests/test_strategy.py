import unittest
import logging
import sys
from trading.bitmex.strategies import Strategy, RenkoBox, SubBox, PriceBelowRenkoFloor, PriceAboveRenkoCeiling
from trading.utils.tests.helper.tags import display_test_name

mock_tick = {
    'buy': 1000,
    'sell': 1001,
    'last': 1000
}

logging.basicConfig(stream=sys.stdout, format="%(asctime)s:%(levelname)s   %(message)s")
logging.getLogger('strategy').setLevel(logging.DEBUG)

MAX_EXPOSURE = 800

class TestSubBoxDownStrategy(unittest.TestCase):
    def setUp(self):
        '''
        test uses a renko box composed of three sub boxes, all with
        length of 8 and delta of 2
        '''
        self.strategy_boxsize_fifteen_down = RenkoBox([
            SubBox(9000, 8985, 1000, -3000, delta=3),
            SubBox(8985, 8970, 2000, -2000, delta=3),
            SubBox(8970, 8955, 3000, -1000, delta=3)
        ])

    @display_test_name
    def test_current_index(self):
        self.assertEqual(self.strategy_boxsize_fifteen_down.curIndex(9000), 0)
        self.assertEqual(self.strategy_boxsize_fifteen_down.curIndex(8990), 0)
        self.assertEqual(self.strategy_boxsize_fifteen_down.curIndex(8985), 1)
        self.assertEqual(self.strategy_boxsize_fifteen_down.curIndex(8970), 2)
        self.assertEqual(self.strategy_boxsize_fifteen_down.curIndex(8955), 2)

        with self.assertRaises(PriceBelowRenkoFloor):
            self.strategy_boxsize_fifteen_down.curIndex(8954.9)

        with self.assertRaises(PriceAboveRenkoCeiling):
            self.strategy_boxsize_fifteen_down.curIndex(9001)

    @display_test_name
    def test_no_buy_orders_open_if_price_at_sub_box_floor(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8954,
            top_of_book_asks=8956, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[0][0], 8965.5)
        self.assertEqual(orders[1][0], 8968.5)

    @display_test_name
    def test_no_orders_open_if_renko_mode_not_normal(self):
        # Execute with normal mode
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8954,
            top_of_book_asks=8956, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 2)

        # Execute with trend mode
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8954,
            top_of_book_asks=8956, state={'orders': {}, 'renko_mode': 'trend_up'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 0)

    @display_test_name
    def test_no_sell_orders_open_if_price_at_sub_box_ceil(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8984,
            top_of_book_asks=8986, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 2)
        self.assertEqual(sell_order_count, 0)
        self.assertEqual(orders[0][0], 8971.5)
        self.assertEqual(orders[0][1], 1000)
        self.assertEqual(orders[1][0], 8974.5)
        self.assertEqual(orders[1][1], 1000)

    @display_test_name
    def test_buy_and_sell_orders_target_volume(self):
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {}, 'renko_mode': 'normal'})

        self.assertEqual(orders[0][0], 8986.5)
        self.assertEqual(orders[0][1], 500)

        self.assertEqual(orders[1][0], 8989.5)
        self.assertEqual(orders[1][1], 500)

        self.assertEqual(orders[2][0], 8995.5)
        self.assertEqual(orders[2][1], -1500)

        self.assertEqual(orders[3][0], 8998.5)
        self.assertEqual(orders[3][1], -1500)

    @display_test_name
    def test_two_buy_orders_and_two_sell_orders_open_if_price_at_sub_box_midpoint(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 2)
        self.assertEqual(orders[0][0], 8986.5)
        self.assertEqual(orders[1][0], 8989.5)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[2][0], 8995.5)
        self.assertEqual(orders[3][0], 8998.5)

    @display_test_name
    def test_order_in_state_memory_not_opened_when_price_at_midpoint(self):
        buy_order_count = 0
        sell_order_count = 0
        target_buy_vol = self.strategy_boxsize_fifteen_down.targetVolume(side='bids', price=8992)
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {
                8989.5: target_buy_vol
            }, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 1)
        self.assertEqual(orders[0][0], 8986.5)
        self.assertEqual(orders[0][1], 500)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[1][0], 8995.5)
        self.assertEqual(orders[1][1], -1500)
        self.assertEqual(orders[2][0], 8998.5)
        self.assertEqual(orders[2][1], -1500)

    @display_test_name
    def test_reads_state_of_buy_exposure_and_adjusts_exposure_to_target(self):
        cur_vol = 900
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {
                8989.5: cur_vol
            }, 'renko_mode': 'normal'})
        target_buy_vol = self.strategy_boxsize_fifteen_down.targetVolume(side='bids', price=8989.5)
        delta = target_buy_vol - cur_vol

        self.assertEqual(orders[1][0], 8989.5)
        self.assertEqual(orders[1][1], delta)

    @display_test_name
    def test_reads_state_of_sell_exposure_and_adjusts_exposure_to_target(self):
        cur_vol = -2000
        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {
                8995.5: cur_vol
            }, 'renko_mode': 'normal'})
        target_buy_vol = self.strategy_boxsize_fifteen_down.targetVolume(side='asks', price=8995.5)
        delta = target_buy_vol - cur_vol

        self.assertEqual(orders[2][0], 8995.5)
        self.assertEqual(orders[2][1], delta)

    @display_test_name
    def test_no_orders_open_at_midpoint_if_orders_already_opened(self):
        buy_order_count = 0
        sell_order_count = 0

        target_buy_vol = self.strategy_boxsize_fifteen_down.targetVolume(side='bids', price=8990)
        self.assertEqual(target_buy_vol, 500)
        target_sell_vol = self.strategy_boxsize_fifteen_down.targetVolume(side='asks', price=8993)
        self.assertEqual(target_sell_vol, -1500)

        orders = self.strategy_boxsize_fifteen_down.execute(top_of_book_bids=8992,
            top_of_book_asks=8993, state={'orders': {
                8986.5: target_buy_vol,
                8989.5: target_buy_vol,
                8995.5: target_sell_vol,
                8998.5: target_sell_vol
            }, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 0)

class TestSubBoxUpStrategy(unittest.TestCase):
    def setUp(self):
        '''
        test uses a renko box composed of three sub boxes, all with
        length of 8 and delta of 2
        '''
        self.strategy_boxsize_eight_up = RenkoBox([
            SubBox(7984, 7992, 8000, -1000, delta=2),
            SubBox(7992, 8000, 5000, -200, delta=2),
            SubBox(8000, 8008, 2000, -200, delta=2)
        ])

    @display_test_name
    def test_current_index(self):
        self.assertEqual(self.strategy_boxsize_eight_up.curIndex(7999), 1)
        self.assertEqual(self.strategy_boxsize_eight_up.curIndex(7984), 0)
        self.assertEqual(self.strategy_boxsize_eight_up.curIndex(7992), 1)
        self.assertEqual(self.strategy_boxsize_eight_up.curIndex(8006), 2)

        with self.assertRaises(PriceBelowRenkoFloor):
            self.strategy_boxsize_eight_up.curIndex(7983.7)

        with self.assertRaises(PriceAboveRenkoCeiling):
            self.strategy_boxsize_eight_up.curIndex(8009)

    @display_test_name
    def test_no_buy_orders_open_if_price_at_sub_box_floor(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=7999,
            top_of_book_asks=8001, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[0][0], 8006)
        self.assertEqual(orders[1][0], 8008)

    @display_test_name
    def test_no_sell_orders_open_if_price_at_sub_box_ceil(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8007,
            top_of_book_asks=8009, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 2)
        self.assertEqual(sell_order_count, 0)
        self.assertEqual(orders[0][0], 8000)
        self.assertEqual(orders[1][0], 8002)

    @display_test_name
    def test_buy_and_sell_orders_target_volume(self):
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=7987,
            top_of_book_asks=7989, state={'orders': {}, 'renko_mode': 'normal'})

        self.assertEqual(orders[0][0], 7984)
        self.assertEqual(orders[0][1], 4000)

        self.assertEqual(orders[1][0], 7986)
        self.assertEqual(orders[1][1], 4000)

        self.assertEqual(orders[2][0], 7990)
        self.assertEqual(orders[2][1], -500)

        self.assertEqual(orders[3][0], 7992)
        self.assertEqual(orders[3][1], -500)

    @display_test_name
    def test_two_buy_orders_and_two_sell_orders_open_if_price_at_sub_box_midpoint(self):
        buy_order_count = 0
        sell_order_count = 0
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8003,
            top_of_book_asks=8005, state={'orders': {}, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 2)
        self.assertEqual(orders[0][0], 8000)
        self.assertEqual(orders[1][0], 8002)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[2][0], 8006)
        self.assertEqual(orders[3][0], 8008)

    @display_test_name
    def test_order_in_state_memory_not_opened_when_price_at_midpoint(self):
        buy_order_count = 0
        sell_order_count = 0
        target_buy_vol = self.strategy_boxsize_eight_up.targetVolume(side='bids', price=8003)
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8003,
            top_of_book_asks=8005, state={'orders': {
                8002: target_buy_vol
            }, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 1)
        self.assertEqual(orders[0][0], 8000)
        self.assertEqual(sell_order_count, 2)
        self.assertEqual(orders[1][0], 8006)
        self.assertEqual(orders[2][0], 8008)

    @display_test_name
    def test_reads_state_of_buy_exposure_and_adjusts_exposure_to_target(self):
        cur_vol = -300
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8003,
            top_of_book_asks=8005, state={'orders': {
                8002: cur_vol
            }, 'renko_mode': 'normal'})
        target_buy_vol = self.strategy_boxsize_eight_up.targetVolume(side='bids', price=8003)
        delta = target_buy_vol - cur_vol

        self.assertEqual(orders[1][0], 8002)
        self.assertEqual(orders[1][1], delta)

    @display_test_name
    def test_reads_state_of_sell_exposure_and_adjusts_exposure_to_target(self):
        cur_vol = -600
        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8003,
            top_of_book_asks=8005, state={'orders': {
                8006: cur_vol
            }, 'renko_mode': 'normal'})
        target_buy_vol = self.strategy_boxsize_eight_up.targetVolume(side='asks', price=8005)
        delta = target_buy_vol - cur_vol

        self.assertEqual(orders[2][0], 8006)
        self.assertEqual(orders[2][1], delta)

    @display_test_name
    def test_no_orders_open_at_midpoint_if_orders_already_opened(self):
        buy_order_count = 0
        sell_order_count = 0

        target_buy_vol = self.strategy_boxsize_eight_up.targetVolume(side='bids', price=8003)
        self.assertEqual(target_buy_vol, 1000)
        target_sell_vol = self.strategy_boxsize_eight_up.targetVolume(side='asks', price=8005)
        self.assertEqual(target_sell_vol, -100)

        orders = self.strategy_boxsize_eight_up.execute(top_of_book_bids=8003,
            top_of_book_asks=8005, state={'orders': {
                8000: target_buy_vol,
                8002: target_buy_vol,
                8006: target_sell_vol,
                8008: target_sell_vol
            }, 'renko_mode': 'normal'})
        for p,v in orders:
            if v > 0:
                buy_order_count += 1
            if v < 0:
                sell_order_count += 1

        self.assertEqual(buy_order_count, 0)
        self.assertEqual(sell_order_count, 0)

class TestStrategy(unittest.TestCase):
    def setUp(self):
        self.strategy = Strategy(volume=100,
                                 consecutive_bars_until_trend_mode=5,
                                 no_of_counter_bars_until_retrace_mode=3,
                                 max_first_run_exposure=MAX_EXPOSURE)

    @display_test_name
    def test_initial_to_normal_mode(self):
        self.assertEqual(self.strategy.mode, 'normal')

    @display_test_name
    def test_buys_on_first_up_bar(self):
        open_exposure = 0
        result, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        self.assertEqual(result, 100)

    @display_test_name
    def test_sells_on_first_down_bar(self):
        open_exposure = 0
        result, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        self.assertEqual(result, -100)

    @display_test_name
    def test_exposure_and_execute_clear_exposure(self):
        open_exposure = 0

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        self.assertEqual(open_exposure, 300, msg='check that \
open exposure is calculated correctly')

        self.assertEqual(self.strategy.execute_clear_exposure(open_exposure=open_exposure), -300)

    @display_test_name
    def test_reverse_exposure_on_counter_bar_in_normal_mode(self):
        open_exposure = 0
        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        self.assertEqual(self.strategy.mode, 'normal')
        self.assertEqual(open_exposure, 300)

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        self.assertEqual(open_exposure, -100)

    @display_test_name
    def test_max_first_run_exposure(self):
        open_exposure = 0
        for i in range(10):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

        self.assertEqual(open_exposure, MAX_EXPOSURE)

        reverse_qty = self.strategy.execute_clear_exposure(open_exposure=open_exposure)
        self.assertEqual(reverse_qty, -MAX_EXPOSURE)

        open_exposure = 0
        for i in range(10):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

        self.assertEqual(open_exposure, -MAX_EXPOSURE)

    @display_test_name
    def test_enters_trend_up_mode(self):
        open_exposure = 0
        for i in range(4):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

        self.assertNotRegex(self.strategy.mode, 'trend')

        self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        self.assertEqual(self.strategy.mode, 'trend_up')

    @display_test_name
    def test_exposure_does_not_reverse_on_counter_bar_in_trend_up_mode(self):
        open_exposure = 0
        for _ in range(5):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        self.assertEqual(open_exposure, 500)

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        self.assertEqual(open_qty, 0)

    @display_test_name
    def test_exposure_does_not_reverse_on_counter_bar_in_trend_down_mode(self):
        open_exposure = 0
        for _ in range(5):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        self.assertEqual(open_exposure, -500)

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty

        self.assertEqual(open_exposure, -500)

    @display_test_name
    def test_enters_trend_down_mode(self):
        open_exposure = 0
        for _ in range(4):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

        self.assertNotRegex(self.strategy.mode, 'trend')

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        self.assertEqual(self.strategy.mode, 'trend_down')

    @display_test_name
    def enter_retrace_up_mode(self, open_exposure):
        for _ in range(6):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        for _ in range(3):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

    @display_test_name
    def test_enters_retrace_up_mode(self):
        self.assertIsNone(self.strategy.sub_mode)
        self.enter_retrace_up_mode(open_exposure=0)
        self.assertEqual(self.strategy.sub_mode, 'retrace_up')

    @display_test_name
    def test_exits_retrace_up_mode_on_lower_low(self):
        open_exposure = 0
        self.enter_retrace_up_mode(open_exposure=open_exposure)

        for _ in range(3):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        self.assertIsNotNone(self.strategy.sub_mode)

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        self.assertIsNone(self.strategy.sub_mode)

        self.assertEqual(self.strategy.mode, 'trend_down')

    @display_test_name
    def test_exits_retrace_up_mode_on_double_retrace(self):
        open_exposure = 0
        self.enter_retrace_up_mode(open_exposure=0)

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_up')

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_up')

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_up')

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.mode, 'normal')

        self.assertIsNone(self.strategy.sub_mode)

    @display_test_name
    def enter_retrace_down_mode(self, open_exposure):
        for _ in range(6):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        for _ in range(3):
            open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty

    @display_test_name
    def test_enters_retrace_down_mode(self):
        self.assertIsNone(self.strategy.sub_mode)
        self.enter_retrace_down_mode(open_exposure=0)
        self.assertEqual(self.strategy.sub_mode, 'retrace_down')

    @display_test_name
    def test_exits_retrace_down_mode_on_higher_high(self):
        open_exposure = 0
        self.enter_retrace_down_mode(open_exposure=open_exposure)

        for _ in range(3):
            open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
            open_exposure += open_qty
        self.assertIsNotNone(self.strategy.sub_mode)

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        self.assertIsNone(self.strategy.sub_mode)

        self.assertEqual(self.strategy.mode, 'trend_up')

    @display_test_name
    def test_exits_retrace_down_mode_on_double_retrace(self):
        open_exposure = 0
        self.enter_retrace_down_mode(open_exposure=open_exposure)

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_down')

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_down')

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.sub_mode, 'retrace_down')

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(self.strategy.mode, 'normal')

        self.assertIsNone(self.strategy.sub_mode)

    @display_test_name
    def test_does_not_place_any_orders_in_sleep_mode(self):
        open_exposure = 0
        self.strategy.sleep()

        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(open_exposure, 0)

        open_qty, _ = self.strategy.execute(bar=1, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(open_exposure, 0)

        self.strategy.wake_up()
        open_qty, _ = self.strategy.execute(bar=0, ticker=mock_tick, open_exposure=open_exposure)
        open_exposure += open_qty
        self.assertEqual(open_exposure, 100)
