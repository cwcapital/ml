from twisted.internet import reactor
from twisted.internet.protocol import Protocol, ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver

import os
import sys
from pathlib import Path
import argparse
import logging
import logging.handlers
import time  # this is only being used as part of the example
import json
import utils.config as Config
from trading.utils.managers import PendingOrderManager

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = SCRIPT_DIR
sys.path.append(os.path.join(PROJECT_DIR, 'trading', 'bitmex'))

from ml_1 import TradingInterface
import helper
import hashlib
from time import sleep

# Deafults
LOG_FILENAME = "{}/bitmextrading.log".format(PROJECT_DIR)
LOG_LEVEL = logging.DEBUG  # Could be e.g. "DEBUG" or "WARNING"

parser = argparse.ArgumentParser(description='machine learning bitmex algo')
parser.add_argument('--box_size', type=float, required=True)
parser.add_argument('--n_input', required=True, type=int, help='number of renko bars program will see')
parser.add_argument('--restore', required=False, default=False, type=helper.str2bool, help='if true, restores strategy state from last run')
parser.add_argument('--algos', required=False, default=[], nargs='*', help='ml algos')
parser.add_argument('--config', required=False, default=None)
parser.add_argument('--DRY_RUN', default=True, type=helper.str2bool)
parser.add_argument("-l", "--log", help="file to write log to (default '" + LOG_FILENAME + "')")
args = parser.parse_args()

# If the log file is specified on the command line then override the default
if args.log:
    LOG_FILENAME = args.log

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger('root')

# Set the log level to LOG_LEVEL
logger.setLevel(LOG_LEVEL)
# Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
# Format each log message like this
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)-8s %(message)s')
# Attach the formatter to the handler
handler.setFormatter(formatter)
# Attach the handler to the logger
logger.addHandler(handler)

strategy_logger = logging.getLogger('__strategy__')
strategy_logger.addHandler(handler)

websockets_logger = logging.getLogger('__websockets__')
websockets_logger.addHandler(handler)

marketmaker_logger = logging.getLogger('__marketmaker__')
marketmaker_logger.addHandler(handler)

marketmaker_logger = logging.getLogger('__ordermanager__')
marketmaker_logger.addHandler(handler)

# Set up configuration
if args.config:
    log_dir = args.config
else:
    DEFAULT_LOG_DIR = '{}/config.json'.format(PROJECT_DIR)
    logger.warn('config path not set. using default: {}'.format(DEFAULT_LOG_DIR))
    log_dir = DEFAULT_LOG_DIR

Config.set_path_to_config_file(log_dir)
config = Config.load()

if args.DRY_RUN != config.DRY_RUN:
    logger.info('ignoring DRY_RUN={} in config file in favor of command line argument, DRY_RUN={}'.format(
        config.DRY_RUN, args.DRY_RUN
    ))
    config = config._replace(DRY_RUN=args.DRY_RUN)

logger.info('config: {}'.format(config))

# Make a class we can use to capture stdout and sterr in the log
class MyLogger(object):
        def __init__(self, logger, level):
                """Needs a logger and a logger level."""
                self.logger = logger
                self.level = level

        def flush(self):
            pass

        def write(self, message):
                # Only log if there is a message (not just a new line)
                if message.rstrip() != "":
                        self.logger.log(self.level, message.rstrip())

# # Replace stdout with logging to file at INFO level
sys.stdout = MyLogger(logger, logging.INFO)
# # Replace stderr with logging to file at ERROR level
sys.stderr = MyLogger(logger, logging.ERROR)

counter = 0

class BitMEXClientProtocol(LineReceiver):
    MAX_LENGTH = 999999999  # connection drops of payload is greater than this value

    def __init__(self, interface):
        self.interface = interface
        self.isRequstingReplay = False

    def connectionMade(self):
        print('new connection made!')
        self.sendLine(json.dumps({'counter': counter}).encode('utf-8'))

    def lineReceived(self, payload):
        global counter

        data = json.loads(payload.decode('utf-8'))
        if counter < data['counter'] and not self.isRequstingReplay:
            print('requesting server replay from {} -> {}'.format(
                counter, data['counter']
            ))
            self.sendLine(json.dumps({'counter': counter}).encode('utf-8'))
            self.isRequstingReplay = True
            return
        else:
            self.isRequstingReplay = False

        if counter > data['counter']:
            print('ignoring data with counter({}) < internal counter({})'.format(
                counter, data['counter']
            ))
            return

        logger.debug('------------------')
        logger.debug('counter: {}'.format(counter))
        logger.debug('received data: {}'.format(hashlib.sha256(payload).hexdigest()))

        counter += 1
        self.interface.run_main_loop(
            data = data['data'],
            orderbook = data['orderbook'],
            ticker = data['ticker']
        )


class BitMEXClientFactory(ReconnectingClientFactory):
    def __init__(self):
        websocket_subscriptions = 'orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD'
        if not args.DRY_RUN:
            websocket_subscriptions += ',execution,order,position'

        self.interface = TradingInterface(
                        websocket_subscriptions=websocket_subscriptions,
                        args=args, ordermanager=None, config=config)

        PendingOrderManager.interface = self.interface

    def startedConnecting(self, connector):
        print('Starting to connect ...')

    def buildProtocol(self, addr):
        print('Connected.')

        self.resetDelay()

        return BitMEXClientProtocol(self.interface)

    def clientConnectionLost(self, connector, reason):
        print('Lost connection.  Reason:', reason)
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        print('Connection failed. Reason:', reason)
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)

# setup and run main event loop
host = '127.0.0.1'
port = 8948
reactor.connectTCP(host, port, BitMEXClientFactory())
reactor.run()
