class InvalidOrder(Exception):
    pass

class InvalidOrderState(InvalidOrder):
    pass

class InvalidDelete(InvalidOrder):
    pass

class InvalidLimit(InvalidOrder):
    pass

class InvalidBuyLimit(InvalidLimit):
    pass

class InvalidSellLimit(InvalidLimit):
    pass

class DataIntegrityError(Exception):
    pass
