from pathlib import Path
from abc import ABCMeta, abstractmethod
import tensorflow as tf
import numpy as np
import os
import sys

BASE_DIR = str(Path(os.path.realpath(__file__)).parents[1])
ML_DIR = os.path.join(BASE_DIR, 'renko')

sys.path.append(ML_DIR)

class RenkoBase(metaclass=ABCMeta):
    @abstractmethod
    def _construct(self, graph):
        pass

    def run(self, x_batch):
        X = self.X
        is_training = self.is_training

        with self.graph.as_default():
            choice_dist = self.choices.eval(
              feed_dict={X: x_batch , is_training: False},
              session=self.sess)

        return choice_dist

    def __init__(self, checkpointRestoreDir):
        self.graph = tf.Graph()
        self.sess = None

        self.model_path = ML_DIR
        self._construct(self.graph)

        RESTORE_DIR = os.path.join(BASE_DIR, 'renko', 'checkpoints', checkpointRestoreDir, 'algo.ckpt')

        with self.graph.as_default():
            self.sess = tf.Session()
            saver = tf.train.Saver()
            saver.restore(self.sess, RESTORE_DIR)

    def __del__(self):
        if self.sess:
            self.sess.close()

class RenkoRNN(RenkoBase):
    def _construct(self, graph):
        from rnn_model import construct_graph

        class args:
            n_input = self.n_input
            time_steps = self.time_steps
            num_units = self.num_units
            n_output_classes = self.n_output_classes
            learning_rate = 0
            forget_bias = self.forget_bias
            keep_prob = 1.0

        tensors = construct_graph(graph, args)
        self.X = tensors['X']
        self.is_training = tensors['is_training']
        self.choices = tensors['choices']


    def __init__(self, checkpointRestoreDir,
                    n_input,
                    time_steps,
                    num_units,
                    n_output_classes,
                    forget_bias):

        self.n_input = n_input
        self.time_steps = time_steps
        self.num_units = num_units
        self.n_output_classes = n_output_classes
        self.forget_bias = forget_bias

        super().__init__(checkpointRestoreDir)


class RenkoDNN(RenkoBase):
    def _construct(self, graph):
        from dnn_model import construct_graph

        class args:
            keep_prob = 0.5
            learning_rate = 0
            n_input_nodes = self.n_input_nodes
            hidden = self.hidden
            n_output_nodes = self.n_output_nodes
            with_dates = True

        tensors = construct_graph(graph, args)
        self.X = tensors['X']
        self.is_training = tensors['is_training']
        self.choices = tensors['choices']


    def __init__(self, checkpointRestoreDir, n_input_nodes, hidden, n_output_nodes):
        self.n_input_nodes = n_input_nodes
        self.hidden = hidden
        self.n_output_nodes = n_output_nodes

        super().__init__(checkpointRestoreDir)
