import json
import os
from pathlib import Path
from collections import namedtuple

__PATH_TO_CONFIG_FILE = None

def set_path_to_config_file(path):
    global __PATH_TO_CONFIG_FILE
    __PATH_TO_CONFIG_FILE = path

def get_path_to_config_file():
    return __PATH_TO_CONFIG_FILE

def load():
    with open(__PATH_TO_CONFIG_FILE) as f:
        _data = json.loads(f.read())

    del _data['__comment']

    Config = namedtuple('Config', _data)

    config = Config(**_data)

    return config
