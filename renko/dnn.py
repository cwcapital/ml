import tensorflow as tf
from tensorflow.contrib.layers import fully_connected, dropout
from datetime import datetime
from utils import DataLoader, AccuracyMeasurer, str2bool, save_to_csv
import numpy
import argparse
import sys
from pprint import pprint
from dnn_model import construct_graph

parser = argparse.ArgumentParser(description='Renko candle deep neural network')
parser.add_argument('--n_input_nodes', type=int, required=True,  help='number of input candles')
parser.add_argument('--with_dates', type=str2bool, required=True, help='doubles n_input_nodes if each input includes a date')
parser.add_argument('--n_output_nodes', type=int, required=True, help='number of output candles')
parser.add_argument('--n_hidden', type=int, nargs='*', help='defines hidden layers')
parser.add_argument('--keep_prob', type=float, required=True, help='determines network dropout rate during learning')
parser.add_argument('--batch_size', type=int, required=True, help='number of instances feed to network')
parser.add_argument('--learning_rate', type=float, required=True, help='neural network learning rate')
parser.add_argument('--n_epochs', type=int, required=True, help='number of training cycles')
parser.add_argument('--n_epochs_save', type=int, required=True, help='number of epochs before evaluating and saving model')
parser.add_argument('--accuracy_threshold', type=float, required=True, help='threshold that a prediction must be above to be considered correct, when measuring model accuracy')
parser.add_argument('--restore', type=str2bool, required=True, help='specifies to initialize a new model or restore from checkpoint')
parser.add_argument('--restoreDir')
parser.add_argument('--dataInputDir')
parser.add_argument('--save_validation_to_csv', type=str2bool, required=True, help='saves validation results to csv file located in analysis/')
parser.add_argument('--with_x_batch', type=str2bool, required=False, default=False)
parser.add_argument('--shuffle_data', type=str2bool, required=False, default=False)
args = parser.parse_args()

if not args.restore:
    res = input('WARNING: initializing a new model will overwrite any previously saved checkout. Continue (y/n)? ')
    if res.lower().strip(' ') not in ['y', 'yes']:
        sys.exit(1)

graph = tf.Graph()
tensors = construct_graph(graph, args)
training_op = tensors['training_op']
X = tensors['X']
y = tensors['y']
is_training = tensors['is_training']
choices = tensors['choices']
accuracy = tensors['accuracy']
loss = tensors['loss']
logits = tensors['logits']

now = datetime.utcnow().strftime('%Y%m%d%H%M%S')
root_logdir = 'tf_logs'
logdir = "{}/run-{}".format(root_logdir, now)

max_reuse_count = args.n_epochs_save if args.shuffle_data else None
dataloader = DataLoader(args.dataInputDir, max_files_opened=None,
   max_reuse_count_before_data_reload=max_reuse_count,
   batch_size=args.batch_size, shuffle_data=args.shuffle_data,
   random_batch_selection=False, DEBUG=True)

with graph.as_default():
    merged = tf.summary.merge_all()
    file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())
    saver = tf.train.Saver()

    try:
        with tf.Session() as sess:
            if args.restore:
                print('restoring model from {}'.format(args.restoreDir))
                saver.restore(sess, args.restoreDir)
            else:
                tf.global_variables_initializer().run()

            for epoch in range(args.n_epochs):
                X_batch, y_batch = dataloader.next_batch()
                sess.run(training_op, feed_dict={X: X_batch, y: y_batch, is_training: True})

                if epoch % args.n_epochs_save == 0:
                    acc_train = accuracy.eval(feed_dict={X: X_batch, y: y_batch, is_training: False})

                    # _loss = loss.eval(feed_dict={X: X_batch, y: y_batch, is_training: False})
                    # _logits = logits.eval(feed_dict={X: X_batch, y: y_batch, is_training: False})
                    choice_dist = choices.eval(feed_dict={X: X_batch, y: y_batch, is_training: False})

                    print(epoch, 'Test accuracy:', acc_train)


                if epoch % args.n_epochs_save == 0:
                    val_data, val_label = dataloader.validation_data
                    acc_train = accuracy.eval(feed_dict={X: val_data, y: val_label, is_training: False})

                    print(epoch, 'Validation accuracy:', acc_train)


                if epoch % args.n_epochs_save == 0:
                    summary, _ = sess.run([merged, loss], feed_dict={X: X_batch, y: y_batch, is_training: False})
                    file_writer.add_summary(summary, epoch)

                    if args.save_validation_to_csv:
                        kwargs = {}
                        if args.with_x_batch:
                            kwargs = {'X_batch': X_batch}
                        kwargs['model'] = False
                        kwargs['excel'] = True
                        save_to_csv(choice_dist, y_batch, epoch, **kwargs)

                    if args.restoreDir:
                        saver.save(sess, args.restoreDir)

            if args.restoreDir:
                saver.save(sess, args.restoreDir)

    finally:
        file_writer.close()
