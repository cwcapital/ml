import sys
import os
from pathlib import Path
import argparse
import json

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[1])

sys.path.append(os.path.join(PROJECT_DIR, 'trading', 'bitmex'))

from interface.run import BitMEXInterface
from interface.market_maker.market_maker import OrderManager

parser = argparse.ArgumentParser(description='script for generating test data')
parser.add_argument('filename', type=str, help='output filename.json')
args = parser.parse_args()

class SimpleInterface(BitMEXInterface):
    def __init__(self, orderbook_type):
        self._orderbook = []
        self._trades = []
        self._ticker = []
        self.i = 0

        super().__init__(orderbook_type)

    def run_init(self, manager):
        pass

    def run_deinit(self, manager):
        data = {
            'orderbook': self._orderbook,
            'trades': self._trades,
            'ticker': self._ticker
        }

        with open('{}.json'.format(args.filename), 'w') as f:
            json.dump(data, f, indent=4)

    def run_main_loop(self, manager):
        orderbook = manager.exchange.get_orderbook()
        trades = manager.exchange.flush_trades()
        ticker =  manager.get_ticker()

        self._orderbook.append(orderbook)
        self._trades.append(trades)
        self._ticker.append(ticker)

        self.i += 1

        print('data count: {}, ticker {}'.format(self.i, ticker))

interface = SimpleInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD')
interface.run_forever()
