from trading.tools.environment import DataStream
from trading.utils.tests.helper.tags import display_test_name
import json
import hashlib
import unittest
from time import sleep
import pickle

from trading.utils.exceptions import DataIntegrityError

data = {
    "orderbook": [
        {
            "asks": [
                [8278.0,126073],[8278.5,108381],[8279.0,52731],[8279.55, 8846],
                [8280.0,118501],[8280.5,30502],[8281.0,77385],
                [8281.5,146328],[8282.0,53582],[8282.5,85948]
            ],
            "bids": [
                [8276,28213],[8275.5,89862],[8275.0,103035],
                [8274.5,96407],[8274.0,74255],[8273.5,41755],[8273.0,97089],
                [8272.5,26224],[8272.0,24737],[8271.5,64877]
            ],
            "timestamp": "2018-04-20T04:18:15.076000Z"
        },
        {
            "asks": [
                [8278.54,4689],[8279.04,24666],
                [8279.54,71881],[8280.04,149989],[8280.54,78490],[8281.04,41710],
                [8281.54,50581],[8282.04,76972],[8282.54,82674],[8283.04,100245]
            ],
            "bids": [
                [8276.54,0],[8276.04,0],
                [8275.54,36913],[8275.04,86801],[8274.54,99294],[8274.04,90069],
                [8273.54,134232],[8273.04,33872],[8272.54,57556],[8272.04,121599]
            ],
            "timestamp": "2018-04-20T04:18:16.076000Z"
        },
        {
            "asks": [
                [8281.33,0],
                [8281.83,0],[8282.33,0],[8282.83,0],[8283.33,89743],[8283.83,58390],
                [8284.33,67569],[8284.83,92557],[8285.33,110837],[8285.83,121331]
            ],
            "bids": [
                [8279.33,0],[8278.83,0],[8278.33,0],[8277.83,85716],
                [8277.33,90290],[8276.83,46331],[8276.33,74382],
                [8275.83,108690],[8275.33,43272],[8274.83,53300]
            ],
            "timestamp": "2018-04-20T04:18:17.076000Z"
        }
    ],
    "ticker": [
        { "buy": 8278.0, "last": 8276, "sell": 8276 } ,
        { "buy": 8278.54, "last": 8278.54, "sell": 8276.54 },
        { "buy": 8281.33, "last": 8283.33, "sell": 8279.33 }
    ],
    "trades": [
        [
         ["2018-04-20T04:18:15.076000Z", 8276, -66447 ]
        ],
        [
         ["2018-04-20T04:18:16.076000Z", 8278.54, 137557 ],
         ["2018-04-20T04:18:16.076000Z", 8276.54, -77876 ],
         ["2018-04-20T04:18:16.076000Z", 8276.04, -124073 ],
         ["2018-04-20T04:18:16.076000Z", 8275.54, -81993 ]
        ],
        [
         ["2018-04-20T04:18:17.076000Z", 8281.33, 40030],
         ["2018-04-20T04:18:17.076000Z", 8281.83, 77162],
         ["2018-04-20T04:18:17.076000Z", 8282.33, 28952]
        ]
    ]
}

data_hash = hashlib.sha256(json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()

class DataStreamTest(unittest.TestCase):
    @display_test_name
    def test_hashes_between_data_and_the_record_created_from_data_match(self):
        d = DataStream(records=['orderbook', 'ticker', 'trades'], path_save='/tmp')
        for ob, ti, tr in zip(data['orderbook'], data['ticker'], data['trades']):
            d.record(orderbook=ob, ticker=ti, trades=tr)

        self.assertEqual(d.hash(), data_hash)

    @display_test_name
    def test_loaded_record_hash_matches_the_original_data_hash(self):
        d1 = DataStream(records=['orderbook', 'ticker', 'trades'], path_save='/tmp')
        for ob, ti, tr in zip(data['orderbook'], data['ticker'], data['trades']):
            d1.record(orderbook=ob, ticker=ti, trades=tr)

        sleep(0.2)  # allow daemon to save pickle file

        d2 = DataStream(path_load=d1.filename)
        loaded_data = d2.load()

        self.assertEqual(
            hashlib.sha256(json.dumps(loaded_data['data'], sort_keys=True).encode('utf-8')).hexdigest(),
            data_hash
        )

        self.assertEqual(loaded_data['hash'], data_hash)

    @display_test_name
    def test_corrupted_data_raises_exception(self):
        d = DataStream(records=['orderbook', 'ticker', 'trades'], path_save='/tmp')
        for ob, ti, tr in zip(data['orderbook'], data['ticker'], data['trades']):
            d.record(orderbook=ob, ticker=ti, trades=tr)

        sleep(0.2)  # allow daemon to save pickle file

        original_filename = d.filename
        original_hash = d.hash()

        d = DataStream(path_load=original_filename)
        corrupted_data = d.load()
        corrupted_data['data']['trades'].append(
            ["2018-04-20T04:18:16.076000Z", 8276.54, -77876 ]
        )
        with open(original_filename, 'wb') as f:
            pickle.dump(corrupted_data, f)

        with self.assertRaises(DataIntegrityError) as cm:
            DataStream(path_load=original_filename).load()

        self.assertEqual(cm.exception.args[0], '{} current data does not match original hash value {}'.format(
            original_filename, original_hash
        ))
