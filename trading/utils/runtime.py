from trading.utils.events import OrderPlaceEvent, CancelOrderEvent, ReturnEvent
from trading.utils.custom_events import CustomEvent
from trading.utils.managers import PendingOrderManager

def check_new_order_opened(clOrdID):
    initial_order_count = len(PendingOrderManager.fetch_all_orders()) + 1
    def callback_func(**kwargs):
        return (
            len(PendingOrderManager.fetch_all_orders()) > initial_order_count,
            {'clOrdID': clOrdID}
        )

    return callback_func

def callback_cancel_order(**kwargs):
    clOrdID = kwargs['clOrdID']
    CancelOrderEvent(clOrdID)
    PendingOrderManager.update_cancel_order_events(**kwargs)

class OrderPlaceEvent(OrderPlaceEvent):
    def __init__(self, qty, prc, orderbook=None, callback=None, tags=None, DRY_RUN=True, **kwargs):
        super().__init__(qty, prc,
                         orderbook=orderbook,
                         callback=callback,
                         tags=tags,
                         DRY_RUN=DRY_RUN,
                         check_new_order_opened=check_new_order_opened,
                         callback_cancel_order=callback_cancel_order,
                         **kwargs)

class Engine:
    @staticmethod
    def update(**kwargs):
        interface = kwargs.pop('interface', None)
        PendingOrderManager.update(interface=interface, **kwargs)
        CustomEvent.engine(interface=interface, **kwargs)
