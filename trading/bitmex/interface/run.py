from abc import ABCMeta, abstractmethod
from trading.utils.events import ReturnEvent
from trading.utils import runtime

# Try/except just keeps ctrl-c from printing an ugly stacktrace
class BitMEXInterface(metaclass=ABCMeta):
    @abstractmethod
    def run_init(self):
        pass

    @abstractmethod
    def run_main_loop(self):
        pass

    @abstractmethod
    def run_deinit(self):
        pass

    def get_tick(self):
        try:
            return self.last_tick['buy'], self.last_tick['sell'], self.last_tick['last']
        except TypeError:
            return None

    def check_hard_close_limit(self):
        # p = self.hard_close_limit_price
        # if p:
        #     _ , _ , ticker_price = self.get_tick()
        #     _ , exposure, _ = self.get_vwap_and_total_volume_and_closed_pl()
        #
        #     if exposure > 0:
        #         if ticker_price <= p:
        #             self.place_order(-exposure)
        #             return True, p
        #
        #     if exposure < 0:
        #         if ticker_price >= p:
        #             self.place_order(-exposure)
        #             return True, p
        #
        # return False, None
        raise Exception('deprecated. not compatible with Live mode. Use soft limit instead')

    def set_hard_close_limit(self, price):
        self.hard_close_limit_price = price

    def place_order(self, volume, **kwargs):
        order_price = kwargs.get('order_price')
        order_type = 'pending' if kwargs.get('order_type') == 'pending' else 'market'
        clOrdID = kwargs.get('clOrdID')
        order = []

        if order_price:
             order.append((volume, order_price, order_type, clOrdID))
        else:
            orderQty = kwargs.get('orderQty')  # DRY_RUN=False. info is passed downstream from server
            if orderQty:
                order.append((orderQty, kwargs['price'], order_type, clOrdID))
            else:
                bid, ask, _ = self.get_tick()

                if volume > 0:
                    order.append((volume, ask, order_type, clOrdID))
                if volume < 0:
                    order.append((volume, bid, order_type, clOrdID))

        if volume > 0:
            order_state = '{}_buy_filled'.format(order_type)
        else:
            order_state = '{}_sell_filled'.format(order_type)

        if order:
            self.ledger.extend(order)
            print('Order filled {}'.format(order))
            ReturnEvent({
                'type': 'order',
                'clOrdID': clOrdID,
                'order_state': order_state
            })

    def fees_paid(self):
        fees = 0
        for v,p,typ,_ in self.ledger:
            btc_value = (1/p) * v
            if typ == 'pending':
                fees -= abs(btc_value) * 0.00025
            else:
                fees += abs(btc_value) * 0.00075

        return -fees

    def get_vwap_and_total_volume_and_closed_pl(self):
        profit = 0
        vwap = 0
        total_volume = 0

        for v,p,_,_ in self.ledger:
            if total_volume > 0:
                if v > 0:
                    new_total_volume = total_volume + v
                    vwap = vwap * (total_volume / new_total_volume) + p * (v / new_total_volume)
                else:
                    vol_offset = min(abs(v), total_volume)
                    profit += (1/vwap - 1/p) * vol_offset
                    new_total_volume = total_volume + v

                total_volume = new_total_volume
                if total_volume > 0:
                    pass # vwap stays the same
                elif total_volume < 0:
                    vwap = p
                else:
                    vwap = 0

            elif total_volume < 0:
                if v < 0:
                    new_total_volume = total_volume + v
                    vwap = vwap * (total_volume / new_total_volume) + p * (v / new_total_volume)
                else:
                    vol_offset = min(v, abs(total_volume))
                    profit += (1/vwap - 1/p) * -vol_offset
                    new_total_volume = total_volume + v

                total_volume = new_total_volume
                if total_volume < 0:
                    pass # vwap stays the same
                elif total_volume > 0:
                    vwap = p
                else:
                    vwap = 0

            else:
                vwap = p
                total_volume = v

        return vwap, total_volume, profit

    def reset_ledger(self):
        self.ledger = []

    def floating_pl(self, vwap, total_volume):
        try:
            bid, ask, _ = self.get_tick()
            profit = 0

            if vwap > 0:
                if total_volume > 0:
                    profit = (1/vwap - 1/bid) * total_volume
                if total_volume < 0:
                    profit = (1/vwap - 1/ask) * total_volume
        except TypeError:
            profit = 0

        return profit

    @abstractmethod
    def main(self, **kwargs):
        pass

    # def engine(self, **kwargs):
    #     pass

    def simulation_engine(self, **kwargs):
        self.manager.set_ticker(kwargs['ticker'])

        runtime.Engine.update(interface=self, **kwargs)

        return self.main(**kwargs)

    def bitmex_engine(self, **kwargs):
        runtime.Engine.update(**kwargs)

        return self.main(**kwargs)

    def __init__(self, orderbook_type, ordermanager=None, config=None):
        if ordermanager:
            OrderManager = ordermanager
            self.engine = self.simulation_engine
        else:
            from .market_maker.market_maker import OrderManager
            self.engine = self.bitmex_engine
        self.config = config
        self.manager = OrderManager(self.run_init,
                                    self.run_main_loop,
                                    self.run_deinit,
                                    orderbook_type,
                                    config=config)
        self.manager.init()
        self.ledger = []
        self.hard_close_limit_price = None
        self.last_tick = None

    def run_main_loop(self, **kwargs):
        self.last_tick = kwargs.get('ticker')

    def run_forever(self):
        self.manager.run_forever()
