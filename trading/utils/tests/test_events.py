import unittest

from trading.utils.events import Event, PriceEvent, TradeEvent, RenkoBarEvent, \
                OrderFilledEvent, OrderPlaceEvent, CancelOrderEvent, OrderAmendEvent, \
                ReturnEvent, CancelAllOrdersEvent
from trading.utils.exceptions import InvalidLimit, InvalidBuyLimit, InvalidSellLimit

orderbook = {
        'symbol': 'XBTUSD',
        'bids': [[8000, 13552],
            [7999, 525]],
        'asks': [[8001, 34714],
            [8002, 1725]],
        'timestamp': '2018-06-08T17:28:29.274Z'
}

class TestEvent(unittest.TestCase):
    def setUp(self):
        Event.instances = {}

    def flush_event_clears_instances(self, EventObj, event_type, events):
        prc = 0
        e0 = events['e0']
        e1 = events['e1']

        self.assertEqual(len(Event.get_instances(event_type)), 2)
        events = EventObj.flush()
        self.assertEqual(e0.description, events[0].description)  # all events should have an event id
        self.assertEqual(e1.description, events[1].description)
        self.assertEqual(len(Event.get_instances(event_type)), 0)

    def event_shows_in_event_instances(self, event_type, events):
        e = events['e']
        self.assertEqual(len(Event.get_instances(event_type)), 1)
        self.assertEqual(Event.get_instances(event_type)[0], e)

class TestOrderAmendEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        clOrdID_1 = 'afj32kn23f9a'
        clOrdID_2 = 'ak2j3k2ljmfs'
        e0 = OrderAmendEvent(clOrdID_1, price=50)
        e1 = OrderAmendEvent(clOrdID_2, price=75)

        self.assertEqual(len(OrderAmendEvent.get_instances('order_amend')), 2)

        events = OrderAmendEvent.flush()

        self.assertEqual(e0.clOrdID, events[0].clOrdID)
        self.assertEqual(e0.price, events[0].price)
        self.assertEqual(e1.clOrdID, events[1].clOrdID)
        self.assertEqual(e1.price, events[1].price)
        self.assertEqual(len(OrderAmendEvent.get_instances('order_amend')), 0)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': OrderAmendEvent('dsafj23io2390', price=100)
        }
        self.event_shows_in_event_instances('order_amend', events)

class TestReturnEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        e0 = ReturnEvent({'type': 'a', 'msg': 'hi'})
        e1 = ReturnEvent({'type': 'b', 'msg': 'bye'})

        self.assertEqual(len(ReturnEvent.get_instances('return_events')), 2)

        events = ReturnEvent.flush()

        self.assertEqual(e0.eventDict, events[0].eventDict)
        self.assertEqual(e1.eventDict, events[1].eventDict)
        self.assertEqual(len(ReturnEvent.get_instances('return_events')), 0)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': ReturnEvent({'type': 'a', 'msg': 'hi'})
        }
        self.event_shows_in_event_instances('return_events', events)

class TestRenkoBarEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        events = {
            'e0': RenkoBarEvent(desc='testa', price=10, prev_price=0, direction='UP'),
            'e1': RenkoBarEvent(desc='testa', price=0, prev_price=10, direction='DOWN')
        }
        self.flush_event_clears_instances(RenkoBarEvent, 'renko_bar', events)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': RenkoBarEvent(desc='testa', price=10, prev_price=0, direction='UP')
        }
        self.event_shows_in_event_instances('renko_bar', events)

class TestOrderFilledEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        clOrdID_1 = 'afj32kn23f9a'
        clOrdID_2 = 'ak2j3k2ljmfs'
        e0 = OrderFilledEvent(clOrdID_1, qty=50)
        e1 = OrderFilledEvent(clOrdID_2, qty=75)

        self.assertEqual(len(OrderFilledEvent.get_instances('order_filled')), 2)

        events = OrderFilledEvent.flush()

        self.assertEqual(e0.clOrdID, events[0].clOrdID)
        self.assertEqual(e0.qty, events[0].qty)
        self.assertEqual(e1.clOrdID, events[1].clOrdID)
        self.assertEqual(e1.qty, events[1].qty)
        self.assertEqual(len(OrderFilledEvent.get_instances('order_filled')), 0)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': OrderFilledEvent('dsafj23io2390', qty=100)
        }
        self.event_shows_in_event_instances('order_filled', events)

class TestCancelAllOrdersEvent(TestEvent):
    def test_event_shows_in_event_instances(self):
        events = {
            'e': CancelAllOrdersEvent()
        }
        self.event_shows_in_event_instances('cancel_all_orders', events)

class TestCancelOrderEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        clOrdID_1 = 'afj32kn23f9a'
        clOrdID_2 = 'ak2j3k2ljmfs'
        e0 = CancelOrderEvent(clOrdID_1)
        e1 = CancelOrderEvent(clOrdID_2)

        self.assertEqual(len(CancelOrderEvent.get_instances('cancel_order')), 2)

        events = CancelOrderEvent.flush()

        self.assertEqual(e0.clOrdID, events[0].clOrdID)
        self.assertEqual(e1.clOrdID, events[1].clOrdID)
        self.assertEqual(len(CancelOrderEvent.get_instances('cancel_order')), 0)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': CancelOrderEvent('dsafj23io2390')
        }
        self.event_shows_in_event_instances('cancel_order', events)

class TestOrderPlaceEvent(TestEvent):
    def test_placing_a_pending_order_without_orderbook_raises_an_exception(self):
        with self.assertRaises(InvalidLimit):
            OrderPlaceEvent(qty=1000, prc=6500, orderbook=None)

    def test_placing_a_buy_above_top_of_book_bid_raises_an_exception(self):
        with self.assertRaises(InvalidBuyLimit):
            OrderPlaceEvent(qty=1000, prc=8200, orderbook=orderbook)

    def test_placing_a_sell_below_the_top_of_book_ask_raises_an_exception(self):
        with self.assertRaises(InvalidSellLimit):
            OrderPlaceEvent(qty=-1000, prc=7998, orderbook=orderbook)

    def test_flush_event_clears_instances(self):
        e0 = OrderPlaceEvent(1201, 7999, orderbook)
        e1 = OrderPlaceEvent(-300, 8001, orderbook)

        self.assertEqual(len(OrderPlaceEvent.get_instances('place_order')), 2)

        events = OrderPlaceEvent.flush()

        self.assertEqual(e0.quantity, events[0].quantity)
        self.assertEqual(e0.price, events[0].price)
        self.assertEqual(e1.quantity, events[1].quantity)
        self.assertEqual(e1.price, events[1].price)
        self.assertEqual(len(OrderPlaceEvent.get_instances('place_order')), 0)

    def test_event_shows_in_event_instances(self):
        events = {
            'e': OrderPlaceEvent(500, 7999, orderbook)
        }
        self.event_shows_in_event_instances('place_order', events)

class TestPriceEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        prc = 0
        events = {
            'e0': PriceEvent('testa', prc),
            'e1': PriceEvent('testb', prc)
        }
        self.flush_event_clears_instances(PriceEvent, 'price', events)

    def test_event_shows_in_event_instances(self):
        desc = 'test'
        prc = 0
        events = {
            'e': PriceEvent(desc, prc)
        }
        self.event_shows_in_event_instances('price', events)

class TestTradeEvent(TestEvent):
    def test_flush_event_clears_instances(self):
        events = {
            'e0': TradeEvent('test_uuid_a', 'my_desc'),
            'e1': TradeEvent('test_uuid_b', 'my_desc')
        }
        self.flush_event_clears_instances(TradeEvent, 'trade', events)

    def test_event_shows_in_event_instances(self):
        uuid = 'test'
        desc = 'test event'
        events = {
            'e': TradeEvent(uuid, desc)
        }
        self.event_shows_in_event_instances('trade', events)
