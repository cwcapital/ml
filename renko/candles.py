import argparse
import datetime as dt
import os
import sys
import json
import numpy as np
from pprint import pprint
from utils import ticksToRenko, toRelativeCandles, Tick, flatten, str2bool
import random

random.seed(4446321)

def renkoCategory(values, categoryGroup=None):
    if categoryGroup:
        module = __import__('utils')
        func = getattr(module, categoryGroup)

        if categoryGroup == 'identity_category':
            return func(values)

        if categoryGroup == 'directional_category':
            return func(values)

    result = []
    total = len(values)-1

    for i, k in enumerate(reversed(range(total))):
        first_val = values[k]
        second_val = values[k+1]

        assert(first_val != second_val)

        if first_val < second_val:
            state = 'up'
        else:
            state = 'down'

        result.append((2**i) * 1 if state=='down' else 0)

    category = func(sum(result), total)

    return category

def createTestSet(data, n_inputs, n_candles_out, data_validation_split, **kwargs):
    training_data = []
    validation_data = []

    rel_tstamps = kwargs.get('rel_tstamps')
    cat_group = kwargs.get('categoryGroup')
    val_split_perc = int(data_validation_split * 100)

    for i in reversed(range(len(data)-n_inputs-n_candles_out+1)):
        y = [data[k] for k in range(i+n_inputs-1, i+n_inputs+n_candles_out)]
        y_category = renkoCategory(y, categoryGroup=cat_group)

        if rel_tstamps:
            X = flatten([(data[k], rel_tstamps[k]) for k in range(i,i+n_inputs)])
        else:
            X = [data[k] for k in range(i,i+n_inputs)]

        if random.randint(0, val_split_perc) == 0:
            validation_data.append(X + [y_category])
        else:
            training_data.append(X + [y_category])

    return np.array(list(reversed(training_data))), np.array(list(reversed(validation_data)))

def save(test_data_array, validation_data_array, index, outDir, **kwargs):
    _scriptDir = kwargs['SCRIPT_DIR']

    b_save = False

    save_test_filename = os.path.join(_scriptDir, 'ticks', 'candles', outDir, 'test', 'results_{}.npy'.format(index))
    save_val_filename = os.path.join(_scriptDir, 'ticks', 'candles', outDir, 'validation', 'results_{}.npy'.format(index))

    if not os.path.exists(os.path.dirname(save_test_filename)):
        os.makedirs(os.path.dirname(save_test_filename))

    if not os.path.exists(os.path.dirname(save_val_filename)):
        os.makedirs(os.path.dirname(save_val_filename))

    if test_data_array.any() or validation_data_array.any():
        np.save(save_test_filename, test_data_array)
        np.save(save_val_filename, validation_data_array)

        b_save = True

    if kwargs.get('META'):
        with open(os.path.join(_scriptDir, 'ticks', 'candles', outDir, 'meta.json'), 'w') as f:
            json.dump(kwargs['META'], f)

    return b_save

def save_to_file(test_set, validation_set, outDir, **kwargs):
    test_results = []
    validation_results = []
    save_idx = 0
    total_result_mbytes = 0

    grand_test_rows = 0
    grand_val_rows = 0
    grand_test_mbytes = 0
    grand_val_mbytes = 0

    _scriptDir = kwargs['SCRIPT_DIR']
    VERBOSE = kwargs['VERBOSE']
    output_file_size = kwargs['output_file_size']

    if test_set.any() or validation_set.any():
        test_mbytes = sys.getsizeof(test_set) / 1000000.0
        val_mbytes = sys.getsizeof(validation_set) / 1000000.0

        if VERBOSE:
            print('test obj size: ', test_mbytes, ' MB')
            print('validation obj size: ', val_mbytes, ' MB')

        test_chunk_size = int(len(test_set) // (test_mbytes // output_file_size + (1 if test_mbytes % output_file_size != 0 else 0)))
        val_chunk_size =int(len(validation_set) // (val_mbytes // output_file_size + (1 if val_mbytes % output_file_size != 0 else 0)))

        for i,j in zip(range(0, len(test_set), test_chunk_size), range(0, len(validation_set), val_chunk_size)):
            sub_test_set = test_set[i:i+test_chunk_size]
            sub_val_set = validation_set[j:j+val_chunk_size]

            sub_test_mbytes = sys.getsizeof(sub_test_set)
            sub_val_mbytes = sys.getsizeof(sub_val_set)
            chunk_mbytes = sub_test_mbytes + sub_val_mbytes
            if total_result_mbytes + chunk_mbytes <= output_file_size:
                total_result_mbytes += chunk_mbytes
            else:
                test_results = np.vstack(test_results) if test_results else np.array([])
                validation_results = np.vstack(validation_results) if validation_results else np.array([])

                if save(test_results, validation_results, save_idx, outDir, SCRIPT_DIR=_scriptDir):
                    save_idx += 1
                    total_result_mbytes = 0

                test_results = []
                validation_results = []

            if sub_test_set.any():
                test_results.append(sub_test_set)
                grand_test_rows += sub_test_set.shape[0]
                grand_test_mbytes += sub_test_mbytes

            if validation_set.any():
                validation_results.append(sub_val_set)
                grand_val_rows += sub_val_set.shape[0]
                grand_val_mbytes += sub_val_mbytes

        else:
            if VERBOSE:
                print('no data obj created')

        if VERBOSE:
            print('---------------------')

    test_results = np.vstack(test_results) if test_results else np.array([])
    validation_results = np.vstack(validation_results) if validation_results else np.array([])
    save(test_results, validation_results, save_idx, outDir, **kwargs)

    print('result test rows:', grand_test_rows)
    print('result test bytes:', grand_test_mbytes, 'MB')
    print('result validation rows:', grand_val_rows)
    print('result validation bytes:', grand_val_mbytes, 'MB')

def fileStartDate(name):
    start_date, _ = name.split('_')
    return dt.datetime.strptime(start_date, '%Y-%m-%d-%H-%M-%S')

def fileEndDate(name):
    _, end_date = name.split('.')[0].split('_')
    return dt.datetime.strptime(end_date, '%Y-%m-%d-%H-%M-%S')

if __name__ == '__main__':
    SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

    parser = argparse.ArgumentParser(description='Download trades from report server')
    parser.add_argument('path', help='path to directory of processed ticks')
    parser.add_argument('--box_size', required=True, help='box size of renko candles')
    parser.add_argument('--n_inputs', required=True, help='number of input candles to make prediction')
    parser.add_argument('--n_candles_out', required=True, help='number of candles outputed')
    parser.add_argument('--validation_split', default='0.0', help='percentage split between training and validation data. max split equals 50%')
    parser.add_argument('--relative', help='relative candlestick', default=True)
    parser.add_argument('--output_file_size', type=float, required=True, help='max size of output file before splitting, in MB')
    parser.add_argument('--dry-run', type=str2bool, default=False)
    parser.add_argument('--verbose', type=str2bool, default=False)
    parser.add_argument('--categoryGroup', default=None)
    parser.add_argument('--outDir', default=None)
    args = parser.parse_args()

    bIsRelative = args.relative.upper() == 'TRUE' if type(args.relative) == str else args.relative
    box_size = float(args.box_size)
    PATH = os.path.join(SCRIPT_DIR, args.path.rstrip('/'))
    OUTDIR = args.outDir.rstrip('/') + '/' if args.outDir else None
    n_inputs = int(args.n_inputs)
    n_candles_out = int(args.n_candles_out)
    data_validation_split = float(args.validation_split)

    assert(data_validation_split >= 0.0 and data_validation_split <= 1.0)

    if OUTDIR:
        PROCESSED_PATH = os.path.join(SCRIPT_DIR, '/'.join(args.path.split('/')), OUTDIR)
    else:
        PROCESSED_PATH = os.path.join(SCRIPT_DIR, '/'.join(args.path.split('/')))

    if not os.path.exists(PROCESSED_PATH):
        os.makedirs(PROCESSED_PATH)

    test_destination_path = os.path.join(SCRIPT_DIR, 'ticks', 'candles', OUTDIR, 'test')
    if not os.path.exists(test_destination_path):
        os.makedirs(test_destination_path)

    validation_destination_path = os.path.join(SCRIPT_DIR, 'ticks', 'candles', OUTDIR, 'validation')
    if not os.path.exists(validation_destination_path):
        os.makedirs(validation_destination_path)

    map_of_files = {}
    for (dirpath, dirnames, filenames) in os.walk(PATH):
        for filename in filenames:
            if filename.endswith('.csv'):
                map_of_files[filename] = os.sep.join([dirpath, filename])

    keys_sorted = sorted(map_of_files.keys(), key=fileStartDate)

    prev_date = None
    data = ''

    f_start_date = fileStartDate(keys_sorted[0])

    for key in keys_sorted:
        fname = map_of_files[key]
        with open(fname) as f:
            candles, timestamps = ticksToRenko(f, box_size, Tick)

        if args.verbose:
            print('process file: ', fname)

        rel_candles, rel_tstamps = toRelativeCandles(candles, timestamps)

    test_set, validation_set =  createTestSet(rel_candles, n_inputs, n_candles_out, data_validation_split=data_validation_split,
        rel_tstamps=rel_tstamps, categoryGroup=args.categoryGroup)

    save_to_file(test_set, validation_set, args.outDir,
        SCRIPT_DIR=SCRIPT_DIR, n_inputs=n_inputs, n_candles_out=n_candles_out,
        output_file_size=args.output_file_size, VERBOSE=args.verbose)
