import pickle
import warnings
import threading
import datetime as dt
import json
import hashlib
import os

from trading.utils.exceptions import DataIntegrityError

class DataStream:
    def __init__(self, records=[], path_save=None, path_load=None):
        self.environment = {}
        self.start_time = dt.datetime.now().strftime('%b_%m__%H_%M_%S')
        self.filename = None
        self.path_save = path_save
        self.path_load = path_load

        if self.path_save and not os.path.exists(self.path_save):
            os.makedirs(self.path_save)

        if self.path_save and len(records) == 0:
            warnings.warn('environment.DataStream has was passed zero records to record')

        for r in records:
            self.environment[r] = []

    def load(self):
        with open(self.path_load, 'rb') as f:
            env = pickle.load(f)
            if not hashlib.sha256(json.dumps(env['data'], sort_keys=True).encode('utf-8')).hexdigest() == env['hash']:
                raise DataIntegrityError('{} current data does not match original hash value {}'.format(
                    self.path_load, env['hash']
                ))
        return env

    def hash(self):
        return hashlib.sha256(json.dumps(self.environment, sort_keys=True).encode('utf-8')).hexdigest()

    def _save(self):
        self.filename = os.path.join(self.path_save, 'from_{}_to_{}.pkl'.format(
            self.start_time, dt.datetime.now().strftime('%b_%m__%H_%M_%S'))
        )

        with open(self.filename, 'wb') as f:
            pickle.dump({ 'data': self.environment, 'hash': self.hash() }, f)

    def record(self, **kwargs):
        for k,v in kwargs.items():
            self.environment[k].append(v)

        threading.Thread(None, target=self._save, daemon=True).start()



    def __del__(self):
        if self.filename:
            print('Saved DataStream record to {}'.format(self.filename))
            print('DataStream record hash: {}'.format(self.hash()))
