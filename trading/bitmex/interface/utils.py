import pickle
import json
from collections import deque

class InvalidOrder(Exception):
    pass

class InvalidDelete(InvalidOrder):
    pass

class InvalidLimit(InvalidOrder):
    pass

class InvalidBuyLimit(InvalidLimit):
    pass

class InvalidSellLimit(InvalidLimit):
    pass

class DataError(Exception):
    pass

class PendingOrderTracker:
    def __init__(self, order_price,
                       order_volume,
                       max_hard_loss_usd,
                       max_deviation_before_cancel,
                       max_deviation_before_reset,
                       price_delta_on_reset):

        self.max_hard_loss_usd = max_hard_loss_usd
        self.max_deviation_before_cancel = max_deviation_before_cancel
        self.max_deviation_before_reset = max_deviation_before_reset
        self.price_delta_on_reset = price_delta_on_reset
        self.order_price = order_price
        self.order_volume = order_volume
        self._valid = True

        if order_volume != 0:
            self.side = 'buy' if order_volume > 0 else 'sell'
        else:
            raise InvalidLimit

    def __repr__(self):
        return 'tracker: (prc,vol)=({},{})'.format(self.order_price, self.order_volume)

    def update(self, price):
        if self.side == 'buy':
            deviation = price - self.order_price

        if self.side == 'sell':
            deviation = self.order_price - price

        if self.max_hard_loss_usd and deviation > self.max_hard_loss_usd:
            print('{}, price={}, order_price={}, max_hard_loss_usd={}'.format(
                self, price, self.order_price, self.max_hard_loss_usd
            ))
            self._delete = True
            return 'close'

        if self.max_deviation_before_cancel and deviation > self.max_deviation_before_cancel:
            print('{}, price={}, order_price={}, max_deviation_before_cancel={}'.format(
                self, price, self.order_price, self.max_deviation_before_cancel
            ))
            self._delete = True
            return 'cancel'

        if self.max_deviation_before_reset and deviation > self.max_deviation_before_reset:
            print('{}, price={}, order_price={}, max_deviation_before_reset={}'.format(
                self, price, self.order_price, self.max_deviation_before_reset
            ))
            return 'reset'

class DataStream:
    def __init__(self, filename, data=['orderbook', 'trades']):
        self.i = 0
        self.keys = data

        ext = filename[filename.rfind('.'):]

        assert ext in ['.pkl', '.json'], 'Only pickle and json file types supported'

        if ext == '.pkl':
            ftype = 'rb'
            loader = pickle.load
        else:
            ftype = 'r'
            loader = json.load

        with open('tests/{}'.format(filename), ftype) as f:
            info = loader(f)

            for x in self.keys:
                setattr(self, x, info[x])

        self.len = len(info[self.keys[0]])
        for k in self.keys:
            assert(len(info[k]) == self.len)

    def __next__(self):
        i = self.i
        self.i += 1

        if i < self.len:
            row = []
            for k in self.keys:
                data = getattr(self, k)
                row.append(data[i])
            return tuple(row)
        else:
            raise StopIteration

    def __iter__(self):
        return self

class PendingVolume:
    def __init__(self, price, quantity, preceding_volume):
        self.price = price
        self.quantity = quantity
        self.preceding_volume = preceding_volume

class Order:
    def __init__(self, price, quantity):
        self.price = price
        self.quantity = quantity

    def __repr__(self):
        return 'Order ({}, {})'.format(self.price, self.quantity)
