from trading.utils.events import OrderFilledEvent
import logging

logger = logging.getLogger('root')

class Order:
    def __init__(self, clOrdID, price, quantity, ordType, ordStatus, preceding_quantity, callback):
        self.clOrdID = clOrdID
        self.price = price
        self.ordType = ordType
        self.quantity = quantity
        self.callback = callback
        self.preceding_quantity = preceding_quantity
        self.filled_quantity = 0
        self.ordStatus = ordStatus # New, Canceled, PartiallyFilled, Filled, Rejected
        self.additional = {}

        assert self.quantity != 0, 'quantity must be <> 0'

    @property
    def remaining_quantity(self):
        return self.quantity - self.filled_quantity

    @property
    def side(self):
        return 'buy' if self.quantity > 0 else 'sell'

    @property
    def bitmex_order_id(self):
        return self.additional['bitmex']['orderID']

    def add_info(self, exchange):
        self.additional[exchange['source']] = exchange['data']

    def _fill_matched_orders(self, side, trades):
        remaining_quantity = self.remaining_quantity

        if remaining_quantity > 0:
            side = 'bids'
        elif remaining_quantity < 0:
            side = 'asks'
        else:
            return

        if side == 'bids':
            for _, p, v in trades:
                if v < 0 and p == self.price:
                    if self.preceding_quantity > 0:
                        self.preceding_quantity = self.preceding_quantity - abs(v)
                        v = 0

                    if self.preceding_quantity < 0:
                        v = self.preceding_quantity
                        self.preceding_quantity = 0

                    if v < 0  and self.preceding_quantity == 0:
                        fill_quantity = min(self.remaining_quantity, abs(v))
                        OrderFilledEvent(self.clOrdID, fill_quantity)
                        remaining_quantity -= fill_quantity

            if remaining_quantity <= 0:
                return
        else:
            for _, p, v in trades:
                if v > 0 and p == self.price:
                    if self.preceding_quantity < 0:
                        self.preceding_quantity = self.preceding_quantity + abs(v)
                        v = 0

                    if self.preceding_quantity > 0:
                        v = self.preceding_quantity
                        self.preceding_quantity = 0

                    if v > 0 and self.preceding_quantity == 0:
                        fill_quantity = max(self.remaining_quantity, -v)
                        OrderFilledEvent(self.clOrdID, fill_quantity)
                        remaining_quantity -= fill_quantity

                if remaining_quantity >= 0:
                    return

    def update(self, orderbook, trades):
        if self.quantity > 0:
            # Fill if orderbook shifts
            top_of_book_bid, _ = orderbook['bids'][0]
            if self.price > top_of_book_bid:
                return OrderFilledEvent(self.clOrdID, self.quantity)

            # Reduce preceding volume if orderbook volume drops below preceding volume
            for p,v in orderbook['bids']:
                if p == self.price:
                    if v < self.preceding_quantity:
                        logger.info('orderbook drop: reducing preceding quantity from {} -> {}'.format(
                            self.preceding_quantity, v
                        ))
                        self.preceding_quantity = v
                    break

            # Reduce preceding volume and fill orders as trades hit the orderbook
            self._fill_matched_orders('bids', trades)

        if self.quantity < 0:
            top_of_book_ask, _ = orderbook['asks'][0]
            if self.price < top_of_book_ask:
                return OrderFilledEvent(self.clOrdID, self.quantity)

            for p,v in orderbook['asks']:
                if p == self.price:
                    if v < abs(self.preceding_quantity):
                        logger.info('orderbook drop: reducing preceding quantity from {} -> {}'.format(
                            self.preceding_quantity, -v
                        ))
                        self.preceding_quantity = -v
                    break

            # Reduce preceding volume and fill orders as trades hit the orderbook
            self._fill_matched_orders('asks', trades)

    def __repr__(self):
        return '(\'{}\', {}, {}/{}, \'{}\')'.format(
            self.clOrdID,
            self.price,
            self.filled_quantity,
            self.quantity,
            self.ordStatus
        )
