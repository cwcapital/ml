import requests
import datetime
import dateutil.parser
import os
import sys
from pathlib import Path

MAX_GAP_IN_SECS = 100000
MAX_ITERATIONS = 100

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[1])
RENKO_DIR = os.path.join(PROJECT_DIR, 'renko')

sys.path.append(PROJECT_DIR)

from renko.utils import ticksToRenko, toRelativeCandles, argmax, str2bool

_p_tick = None

def fetch_initial_data(count, box_size):
    class Tick:
        def __init__(self, d):
            self.tstamp = dateutil.parser.parse(d['tstamp'])
            self.bid = float(d['bid'])
            self.ask = float(d['ask'])

    def fetch_initial_data():
        pass

    def strToTime(myString):
        return datetime.datetime.strptime(myString, '%Y-%m-%d-%H-%M-%S')

    def timeToStr(myTime):
        return myTime.strftime('%Y-%m-%d-%H-%M-%S')

    def check_ticks(list_ticks):
        global _p_tick
        for json_tick_obj in list_ticks:
            tick = dateutil.parser.parse(json_tick_obj['tstamp'])

            if _p_tick and (_p_tick - tick).seconds >= MAX_GAP_IN_SECS:
                err_msg = 'tick gap found between {} and {}'.format(_p_tick, tick)
                raise Exception(err_msg)

            _p_tick = tick


    HOST = 'https://dynamicdata.colewoodcapital.com/api'
    EXCHANGE = 'bitmex'
    SYMBOL = 'xbtusd'
    ATTEMPTS = 3

    end_time = datetime.datetime.utcnow()
    begin_time = end_time - datetime.timedelta(hours=1)

    client = requests.session()

    candle_bin = []
    timestamp_bin = []
    CANDLE_BIN_SIZE = count + 1
    i = 0

    while len(candle_bin) < CANDLE_BIN_SIZE or i == MAX_ITERATIONS:
        url = '{0}/ticks?exchange={1}&symbol={2}&' \
                 'from={3}&to={4}'.format(HOST, EXCHANGE, SYMBOL, timeToStr(begin_time), timeToStr(end_time))

        res = None
        for k in range(ATTEMPTS):
            res = client.get(url)
            if res.status_code == 200:
                break
            sleep(3)

        if res.status_code != 200:
            print('fetch tick request failed {0} times, with url={1}, status_code={2}, text={3}'.format(
                ATTEMPTS,
                url,
                res.status_code,
                res.text
            ))
            sys.exit(1)

        data_set = res.json()['data']
        check_ticks(reversed(data_set))

        if data_set:
            candles, timestamps = ticksToRenko(iter(data_set), box_size, Tick)

            candle_bin = candles + candle_bin
            timestamp_bin = timestamps + timestamp_bin

        end_time = begin_time
        begin_time -= datetime.timedelta(hours=1)
        i += 1

    if i == MAX_ITERATIONS:
        err_msg = 'WARNING: max iterations hit @ begin_time={}'.format(begin_time)
        raise Exception(err_msg)

    candle_bin = candle_bin[len(candle_bin)-CANDLE_BIN_SIZE:]
    timestamp_bin = timestamp_bin[len(timestamp_bin)-CANDLE_BIN_SIZE:]

    return candle_bin, timestamp_bin
