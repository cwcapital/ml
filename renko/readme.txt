1. fetch.py - fetches ticks from database and stores ticks in csv file format in directory rawticks/{EXCHANGE}/{SYMBOL}/{BEGIN}_{END}.csv

{BEGIN} and {END} are datetimes in YYYY-mm-dd-HH-MM-SS format

usage:
python fetch.py {HOST} {EXCHANGE} {SYMBOL} {BEGIN} {END}

example:
python fetch.py https://dynamicdata.colewoodcapital.com/api bitmex xbtusd 2018-02-16-09-00-00 2018-02-18-09-00-00 --outDir=testvalidaton/

2. process.py - processes raw ticks according to following logic:

* any gaps between ticks of >= X seconds result in the file being split in two at the time of the gap

Successful run results in directory mirroring raw_ticks_directory with the following file logic:
* startdate1_enddate1.txt = first file
* startdate2_enddate2.txt = second file resulting from gap found in 1_1.txt

usage:
python process.py path/to/raw_ticks_directory {GAP}

example:
python process.py bitmex/xbtusd/testvalidation 500 --verbose True --dry-run False


3. candles.py - converts processed ticks into renko candles

usage: python candles.py path/to/processed_ticks --kwargs

* for kwargs definitions run:
python candles.py --help

output:
a series of results_*.npy files in output_dir = ./ticks/final/

example:
python candles.py ticks/processed/bitmex/xbtusd/rnntest/ --box_size=8 --n_inputs=50 --n_candles_out=1 --verbose=true --output_file_size=5000 --validation_split=0.1 --categoryGroup=identity_category --outDir rnntestcandles/


4. dataquality.py - checks quality of imported data

usage: python dataquality.py {params}

example:
python dataquality.py ticks/final/1_out/ --batch_size 200 --iterations 250 --max_files_opened 0 --max_reuse_count 0 --shuffle_data False --random_batch_selection False


4. ../trading/bitmex/ml_1.py - checks quality of imported data

usage: python ml_1.py --n_input 50 --box_size 8 --algos rnn

example:
python dataquality.py ticks/final/1_out/ --batch_size 200 --iterations 250 --max_files_opened 0 --max_reuse_count 0 --shuffle_data False --random_batch_selection False


6. dnn.py - trains a deep vanilla feed forward network

python dnn.py --n_input_nodes=260 --n_output_nodes=2 --with_dates True --n_hidden 100 100 --learning_rate=0.00001 --keep_prob=0.5 --batch_size=200 --n_epochs=10000000 --n_epochs_save 10000 --restore True --dataInputDir ticks/final/1_out  --restoreDir checkpoints/260_in_1_out_100_100_hidden_16bin_aws/temp.ckpt --save_validation_to_csv True --with_x_batch True

output:
a temp.ckpt and/or final.ckpt file located in /tmp/temp that contains the model's variables stored from the previous run session


7. rnn.py - trains a recurrent network

python rnn.py --n_input 2 --n_output_classes 2 --time_steps 50 --num_units 100 --forget_bias 1 --learning_rate=0.00001 --keep_prob=0.5 --batch_size=200 --n_epochs=10000000 --n_epochs_save 10000 --restore True --dataInputDir ticks/candles/testrnn/  --restoreDir checkpoints/rnn_model_2/algo.ckpt --save_validation_to_csv True --with_x_batch True

output:
a algo.ckpt file located in checkpoints/ that contains the model's variables stored from the previous run session
