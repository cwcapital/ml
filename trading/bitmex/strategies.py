from trading.utils.events import ReturnEvent
from utils.metadata import PROJECT_DIR
import logging
import logging.handlers
import warnings
import math

class RenkoBoxException(Exception):
    pass

class PriceAboveRenkoCeiling(RenkoBoxException):
    pass

class PriceBelowRenkoFloor(RenkoBoxException):
    pass

class SubBox:
    def __init__(self, bottom, top, max_vol_buy, max_vol_sell, delta):
        if bottom < top:
            self.floor = bottom
            self.ceil = top
        elif bottom > top:
            self.floor = top
            self.ceil = bottom
        else:
            raise RenkoBoxException('bottom({}) cannot equal top({})'.format(
                self.bottom, self.top
            ))

        self.delta = delta
        self.max_vol_buy = max_vol_buy
        self.max_vol_sell = max_vol_sell
        self.midpoint = (self.floor + self.ceil) / 2.0

        # number of rungs/levels between the floor and ceiling
        self.rung_count = math.floor((self.ceil - self.floor) / self.delta)


    def __repr__(self):
        return 'SubBox(floor={}, ceil={}, maxBuyVolume={}, maxSellVolume={})'.format(
            self.floor, self.ceil, self.max_vol_buy, self.max_vol_sell
        )

class RenkoBox:
    def __init__(self, sub_boxes):
        '''
        delta describes the distance between orders, as measured from the
        subbox midpoint
        '''
        self.logger = logging.getLogger('__strategy__')
        self.sub_boxes = sub_boxes
        self.count = len(sub_boxes)

        if sub_boxes[0].floor < sub_boxes[self.count-1].ceil:
            self.direction = 'UP'
        else:
            self.direction = 'DOWN'

    def targetVolume(self, side, price=None, index=None):
        if index is None:
            index = self.curIndex(price)

        subBox = self.sub_boxes[index]

        if side == 'bids':
            return subBox.max_vol_buy / math.floor(subBox.rung_count / 2)

        if side == 'asks':
            return subBox.max_vol_sell / math.floor(subBox.rung_count / 2)

        raise RenkoBoxException('invalid side = {}'.format(side))

    def curIndex(self, price):
        '''
        an integer from zero to len(self.sub_boxes) - 1, representing
        which box the current price lies in
        '''
        if self.direction == 'UP':
            for i,box in enumerate(self.sub_boxes):
                if price >= box.floor and price < box.ceil:
                    return i

            last_sub_box = self.sub_boxes[self.count-1]
            if price == last_sub_box.ceil:
                return self.count - 1

        if self.direction == 'DOWN':
            for i,box in enumerate(self.sub_boxes):
                if price > box.floor and price <= box.ceil:
                    return i

            last_sub_box = self.sub_boxes[self.count-1]
            if price == last_sub_box.floor:
                return self.count - 1

        if price < self.sub_boxes[0].floor:
            raise PriceBelowRenkoFloor

        if price > self.sub_boxes[self.count-1].ceil:
            raise PriceAboveRenkoCeiling

        raise RenkoBoxException('failed to determine current index')

    def execute(self, top_of_book_bids, top_of_book_asks, state):
        avg_price = (top_of_book_bids + top_of_book_asks) / 2.0
        index = self.curIndex(avg_price)
        box = self.sub_boxes[index]
        orders = []

        if state['renko_mode'] != 'normal':
            return []

        prc_below = box.midpoint - box.delta
        while prc_below >= box.floor:
            target_vol = self.targetVolume('bids', index=index)
            try:
                vol = state['orders'][prc_below]
                delta = target_vol - vol
                if delta and prc_below <= top_of_book_bids:
                    orders.append((prc_below, delta))
            except KeyError as e:
                if prc_below <= top_of_book_bids:
                    orders.append((prc_below, target_vol))

            prc_below -= box.delta

        orders = list(reversed(orders))

        prc_above = box.midpoint + box.delta
        while prc_above <= box.ceil:
            target_vol = self.targetVolume('asks', index=index)
            try:
                vol = state['orders'][prc_above]
                delta = target_vol - vol
                if delta and prc_above >= top_of_book_asks:
                    orders.append((prc_above, delta))
            except KeyError as e:
                if prc_above >= top_of_book_asks:
                    orders.append((prc_above, target_vol))

            prc_above += box.delta

        if orders:
            self.logger.info('requesting new orders: {}'.format(orders))

        return orders

class StateMachine:
    pass

class Strategy:
    def execute_trend_mode(self, bar, open_exposure):
        if self.mode == 'trend_up' and bar == 0:
            self.logger.info('execute_trend_mode::increase open long exposure')
            return self.volume
        if self.mode == 'trend_down' and bar == 1:
            self.logger.info('execute_trend_mode::increase open short exposure')
            return -self.volume

        return 0

    def execute_normal_mode(self, bar, open_exposure):
        if open_exposure == 0:
            return self.volume * (1 if bar == 0 else -1)
        elif open_exposure > 0:
            if bar == 0:
                self.logger.info('execute_normal_mode::increase open long exposure')
                return self.volume
            else:
                self.logger.info('execute_normal_mode::reverse open exposure from long -> short')
                return -open_exposure - self.volume
        else:
            if bar == 1:
                self.logger.info('execute_normal_mode::increase open short exposure')
                return -self.volume
            else:
                self.logger.info('execute_normal_mode::reverse open exposure from short -> long')
                return -open_exposure + self.volume

        return 0

    def check_higher_high(self, sequence_list):
        x = [1 if i == 0 else -1 for i in sequence_list]
        return sum(x) >= 2

    def check_lower_low(self, sequence_list):
        x = [1 if i == 0 else -1 for i in sequence_list]
        return sum(x) <= -2

    def check_double_retrace(self, history):
        def retrace_count(retrace_sequence):
            '''
            history = [0,1,1,0,1,1,1,0,0,1,1] with
            retrace_sequence = [1,1] would return 3
            '''
            b_reversal = False
            counter = 0
            seq_len = len(retrace_sequence)

            for k in range(len(history)-1):
                if history[k:k+seq_len] == retrace_sequence:
                    if not b_reversal:
                        counter+=1
                    b_reversal = True
                else:
                    b_reversal = False

            return counter

        if self.mode == 'trend_up':
            n_retrace = self.no_of_counter_bars_until_retrace_mode
            retrace_sequence = [1] * n_retrace
            return retrace_count(retrace_sequence) >= 2

        if self.mode == 'trend_down':
            n_retrace = self.no_of_counter_bars_until_retrace_mode
            retrace_sequence = [0] * n_retrace
            return retrace_count(retrace_sequence) >= 2

    def process_new_bar(self, bar, working_history):
        self.logger.debug('processing new bar (mode={}, sub_mode={})...'.format(
            self.mode, self.sub_mode
        ))

        if self.sub_mode in ['retrace_up', 'retrace_down']:
            if self.sub_mode == 'retrace_down':
                sequence_from_last_low = self.working_history[self._retrace_index:]
                if self.check_higher_high(sequence_from_last_low):
                    self._retrace_index = 0
                    self.sub_mode = None
                    self.logger.info('exiting retrace down mode due to higher high reached!!!')
                    return('higher_high')
            else:
                sequence_from_last_high = self.working_history[self._retrace_index:]
                if self.check_lower_low(sequence_from_last_high):
                    self._retrace_index = 0
                    self.sub_mode = None
                    self.logger.info('exiting retrace up mode due to lower low reached!!!')
                    return('lower_low')

            if self.check_double_retrace(working_history):
                self.logger.info('flattening exposure')
                self.logger.info('switching to normal mode ...')
                self.mode = 'normal'
                self.sub_mode = None
                return('zero_exposure')

        n_retrace = self.no_of_counter_bars_until_retrace_mode
        retrace_up = [0] * n_retrace
        retrace_down = [1] * n_retrace
        if not self.sub_mode and self.mode == 'trend_up':
            if working_history[-n_retrace:] == retrace_down:
                self.sub_mode = 'retrace_down'
                self._retrace_index = len(self.working_history) - n_retrace - 1
                self.logger.info('beginning retrace down sub mode ...')
                return 'find_last_high'

        if not self.sub_mode and self.mode == 'trend_down':
            if working_history[-n_retrace:] == retrace_up:
                self.sub_mode = 'retrace_up'
                self._retrace_index = len(self.working_history) - n_retrace - 1
                self.logger.info('beginning retrace up sub mode ...')
                return 'find_last_low'

        n_trend = self.consecutive_bars_until_trend_mode
        trend_up = [0] * n_trend
        trend_down = [1] * n_trend
        if not self.mode[:5] == 'trend' \
          and working_history[-n_trend:] in [trend_up, trend_down]:
            if working_history[-n_trend:] == trend_up:
                self.mode = 'trend_up'
                self.logger.info('switching to trend mode up !!!')
                ReturnEvent({
                    'type': 'state_change',
                    'obj': 'strategy',
                    'variable': 'mode',
                    'value': self.mode
                })
            else:
                self.mode = 'trend_down'
                self.logger.info('beginning trend mode down !!!')
                ReturnEvent({
                    'type': 'state_change',
                    'obj': 'strategy',
                    'variable': 'mode',
                    'value': self.mode
                })

    def execute_clear_exposure(self, open_exposure):
        send_exposure = open_exposure * -1
        self.logger.info('executing clear exposure')
        self.logger.info('switching to normal mode ...')
        self.mode = 'normal'
        self.sub_mode = None
        return send_exposure

    def sleep(self):
        self.mode = 'sleep'
        self.logger.info('switching to sleep mode ...')

    def wake_up(self, mode='normal'):
        self.mode = mode
        self.logger.info('switching to {} mode'.format(mode))

    def execute(self, bar, ticker, open_exposure, **kwargs):
        priority, requested_exposure = self.main_logic(bar, open_exposure)

        event = { 'priority': priority,
                  'source': 'strategy',
                   'mode': self.mode }

        if requested_exposure > 0:
            event['type'] = 'order'
            event['order_type'] = 'pending_buy'
            event['order_price'] = ticker['buy'] - 0.5
            remaining_exposure = max(self.max_first_run_exposure - open_exposure, 0)
            open_qty = min(remaining_exposure, requested_exposure)
            event['open_qty'] = open_qty
        elif requested_exposure < 0:
            event['type'] = 'order'
            event['order_type'] = 'pending_sell'
            event['order_price'] = ticker['sell'] + 0.5
            remaining_exposure = min(-self.max_first_run_exposure - open_exposure, 0)
            open_qty = max(remaining_exposure, requested_exposure)
            event['open_qty'] = open_qty
        else:
            event = {}
            open_qty = 0

        open_exposure += open_qty

        ReturnEvent(event)

        return open_qty, event

    def main_logic(self, bar, open_exposure):
        priority = None

        if self.mode == 'reset':
            prev_bar = self.history[-1]
            if bar != prev_bar:
                self.logger.info('switching to normal mode ...')
                self.mode = 'normal'
                self.logger.info('resetting history ...')
                self.history = []
            else:
                return priority, 0

        self.history.append(bar)

        if self.mode != 'reset':
            self.working_history.append(bar)

            directive = self.process_new_bar(bar, self.working_history)

            if directive == 'zero_exposure':
                self.logger.info('received directive=zero_exposure. clear exposure and reset working history ...')
                self.working_history = []
                return 'high', -open_exposure

            if self.mode == 'normal':
                return_volume = self.execute_normal_mode(bar, open_exposure)
                self.logger.debug('executing in normal mode with {} priority, bar={}, return_volume={}'.format(
                    priority, bar, return_volume
                ))
                return priority, return_volume

            if self.mode in ['trend_up', 'trend_down']:
                return_volume = self.execute_trend_mode(bar, open_exposure)
                self.logger.debug('executing in trend mode with {} priority, bar={}, return_volume={}'.format(
                    priority, bar, return_volume
                ))
                return priority, return_volume

        return priority, 0

    def set_mode(self, **kwargs):
        mode = kwargs.get('mode')
        sub_mode = kwargs.get('sub_mode')

        if mode:
            self.mode = mode
            self.logger.debug('setting mode to {}'.format(mode))

        if sub_mode:
            self.sub_mode = sub_mode
            self.logger.debug('setting sub_mode to {}'.format(sub_mode))

    def __getstate__(self):
        d = dict(self.__dict__)
        if 'logger' in d:
            del d['logger']
        return d

    def __setstate__(self, d):
        self.__dict__.update(d)
        self.logger = logging.getLogger('__strategy__')

    def __init__(self, volume,
                    consecutive_bars_until_trend_mode,
                    no_of_counter_bars_until_retrace_mode,
                    max_first_run_exposure):
        self.mode = 'normal'
        self.consecutive_bars_until_trend_mode = consecutive_bars_until_trend_mode
        self.no_of_counter_bars_until_retrace_mode = no_of_counter_bars_until_retrace_mode
        self.max_first_run_exposure = max_first_run_exposure
        self.sub_mode = None
        self._retrace_index = 0
        self.volume = volume
        self.working_history = []
        self.history = []

        # Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
        # Give the logger a unique name (good practice)
        self.logger = logging.getLogger('__strategy__')

        # Set the log level to LOG_LEVEL
        self.logger.setLevel(logging.DEBUG)
        # Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
        log_filename = "{}/bitmextrading.log".format(PROJECT_DIR)
        handler = logging.handlers.TimedRotatingFileHandler(log_filename, when="midnight", backupCount=3)
        # Format each log message like this
        formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
        # Attach the formatter to the handler
        handler.setFormatter(formatter)
        # Attach the handler to the logger
        self.logger.addHandler(handler)

        self.logger.info('executing custom strategy!!!')
