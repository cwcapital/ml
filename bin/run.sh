PROJECTDIR=/projects/ml
BOXSIZE=8
NINPUT=5

export PROJECTDIR=$PROJECTDIR

$PROJECTDIR/.env/bin/python $PROJECTDIR/run.py --box_size $BOXSIZE --n_input $NINPUT
