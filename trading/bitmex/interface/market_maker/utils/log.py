import logging
from .config import config

if config.LOG_LEVEL.upper() == 'DEBUG':
    LOG_LEVEL = logging.DEBUG
elif config.LOG_LEVEL.upper() == 'INFO':
    LOG_LEVEL = logging.INFO
elif config.LOG_LEVEL.upper() == 'WARN':
    LOG_LEVEL = logging.WARN
elif config.LOG_LEVEL.upper() == 'ERROR':
    LOG_LEVEL = logging.ERROR
else:
    raise Exception('Unknown log level: {}'.format(config.LOG_LEVEL))

def setup_custom_logger(name, log_level=LOG_LEVEL):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    logger.addHandler(handler)
    return logger
