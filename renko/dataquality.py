import hashlib
from utils import DataLoader, str2bool
import sys
import numpy as np
import statistics as stats
import os
import argparse

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description='tests quality of DataLoader random samplings')
parser.add_argument('path', help='path to test data and validation data')
parser.add_argument('--batch_size', type=int, required=True)
parser.add_argument('--iterations', type=int, required=True)
parser.add_argument('--max_files_opened', type=int, required=True)
parser.add_argument('--max_reuse_count', type=int, required=True)
parser.add_argument('--shuffle_data', type=str2bool, required=True)
parser.add_argument('--random_batch_selection', type=str2bool, required=True)
args = parser.parse_args()

hash_dict = {}

print('initialzed ...')

# find total number of test entries
TEST_DATA_DIR = os.path.join(SCRIPT_DIR, args.path, 'test')
assert os.path.exists(TEST_DATA_DIR)

data_set_total = 0
for (dirpath, dirnames, filenames) in os.walk(TEST_DATA_DIR):
    for filename in filenames:
        if filename.endswith('.npy'):
            n = np.load(os.path.join(dirpath, filename))
            data_set_total += n.shape[0]

# find data on dataloader
max_files_opened = args.max_files_opened if args.max_files_opened > 0 else None
max_reuse_count = args.max_reuse_count if args.max_reuse_count > 0 else None

dataloader = DataLoader(args.path, max_files_opened,
               max_reuse_count, args.batch_size, args.shuffle_data,
               args.random_batch_selection, DEBUG=True)
for _ in range(args.iterations):
    x_batch, y_batch = dataloader.next_batch()
    y_batch = y_batch.reshape((y_batch.shape[0], 1))

    for i in np.concatenate((x_batch, y_batch), axis=1):
        entry = hashlib.sha256(str(i).encode('utf-8')).hexdigest()
        try:
            hash_dict[entry] += 1
        except KeyError:
            hash_dict[entry] = 1

count = hash_dict.values()

hash_dict_len = len(hash_dict)

print('sample size: {}'.format(args.batch_size * args.iterations))
print('population size: {}'.format(data_set_total))
print('hashes found: {}'.format(hash_dict_len))
print('mean (count): {}'.format(stats.mean(count)))
print('stdev (count): {}'.format(stats.stdev(count)))
print('sample:population coverage: {}'.format(round(hash_dict_len / data_set_total, 2)))
