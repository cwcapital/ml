import json
import os
from pathlib import Path
from collections import namedtuple

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[4])

with open(os.path.join(PROJECT_DIR, 'config.json')) as f:
    _data = json.loads(f.read())

del _data['__comment']

Config = namedtuple('Config', _data)

config = Config(**_data)
