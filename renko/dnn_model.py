import tensorflow as tf
from tensorflow.contrib.layers import fully_connected, dropout
from tensorflow.contrib.layers import batch_norm
from utils import custom_cross_entropy, check_predictions

def construct_hidden_layer(graph, input_data, n_size, scope, is_training, bn_params, args):
    with graph.as_default():
        hidden = fully_connected(input_data, n_size, scope=scope,
          activation_fn=tf.nn.elu, normalizer_fn=batch_norm, normalizer_params=bn_params)
        hidden_drop = dropout(hidden, args.keep_prob, is_training=is_training)
        tf.summary.histogram(scope, hidden)
        return hidden_drop

def construct_graph(graph, args):
    with graph.as_default():
        n_input_nodes = args.n_input_nodes
        if args.with_dates:
            n_input_nodes *= 2

        X = tf.placeholder(tf.float32, shape=(None, n_input_nodes), name='X')
        y = tf.placeholder(tf.int64, shape=(None), name='y')
        is_training = tf.placeholder(tf.bool, shape=(), name='is_training')

        hidden = getattr(args, 'hidden', None)
        with tf.name_scope('dnn'):
            bn_params = {
                'is_training': is_training,
                'decay': 0.99,
                'updates_collections': None,
                'scale': False
            }

            if hidden:
                input_node = X
                i = 1
                for n_size in hidden:
                    input_node = construct_hidden_layer(graph, input_node,
                                        n_size, 'hidden{}'.format(i),
                                        is_training, bn_params, args)
                    i += 1

                output_node = input_node
            else:
                output_node = X

            logits = fully_connected(output_node, args.n_output_nodes, normalizer_fn=batch_norm, normalizer_params=bn_params, scope='outputs')
            choices = tf.nn.softmax(logits)

            tf.summary.histogram('choices', choices)

        with tf.name_scope('loss'):
            xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
            xentropy_special = custom_cross_entropy(labels=y, choices=choices, n_output_nodes=args.n_output_nodes)
            loss = tf.reduce_mean(xentropy)
            loss_special = tf.reduce_mean(xentropy_special)

        tf.summary.scalar('loss', loss)
        tf.summary.scalar('loss_special', loss_special)

        with tf.name_scope('eval'):
            correct = tf.nn.in_top_k(logits, y, 1)
            accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name='accuracy')

        optimizer = tf.train.AdamOptimizer(learning_rate=args.learning_rate)
        training_op = optimizer.minimize(loss_special, name='training_op')

        merged = tf.summary.merge_all()

        tensors = {
            'training_op': training_op,
            'X': X,
            'y': y,
            'is_training': is_training,
            'choices': choices,
            'accuracy': accuracy,
            'loss': loss,
            'logits': logits
        }

        return tensors
