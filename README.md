-- set PROJECTDIR
export PROJECTDIR=path/to/project/directory

-- append PROJECTDIR to PYTHONPATH
export PYTHONPATH=$PYTHONPATH:$PROJECTDIR

-- install python3.6-dev
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3.6-dev

-- set up virtual environment
export LC_ALL="en_US.UTF-8"
virtualenv -p /usr/bin/python3.6 $PROJECTDIR/.env
$PROJECTDIR/.env/bin/pip install -r $PROJECTDIR/requirements.txt

-- set up configuration (copy and modify sample configuration file)
cp $PROJECTDIR/config.sample.json $PROJECTDIR/config.json

-- run unittests
cd $PROJECTDIR
.env/bin/python -m unittest

-- start the datafeed server
PYTHONPATH=$PROJECTDIR $PROJECTDIR/.env/bin/python $PROJECTDIR/server.py

-- start the markermaker client
PYTHONPATH=$PROJECTDIR $PROJECTDIR/.env/bin/python $PROJECTDIR/run.py --box_size 8 --n_input 5

-- sample bash shells
chmod 755 $PROJECTDIR/bin/server.sh
chmod 755 $PROJECTDIR/bin/run.sh

-- sample server systemd startup script, install in /etc/systemd/system/bitmex-datafeed-server.service
[Unit]
Description=Server for streaming and broadcasting bitmex orderbook and trade data
After=network.target

[Service]
ExecStart=/projects/ml/bin/server.sh
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target

-- sample marketmaker systemd startup script, install in /etc/systemd/system/bitmex-marketmaker.service
[Unit]
Description=Bitmex marketmaker bot
After=network.target

[Service]
ExecStart=/projects/ml/bin/run.sh
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target

-- activate systemd script
systemctl enable bitmex-marketmaker.service
systemctl daemon-reload

TODO: create a unittest script, remove randomness

Todo
remove tracker objects
one command to run all tests
change callback to callbacks in OrderPlaceEvent
delete result.get('events') seen in test_overall.py
