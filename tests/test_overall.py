import pickle
import unittest
import os
import sys
from pathlib import Path
import datetime as dt
from dateutil.tz import tzutc
import random
import logging

from trading.utils.managers import PendingOrderManager
from trading.utils.events import Event
from trading.utils.custom_events import CustomEvent
from tests.utils.mock.entities import fetch_initial_data

logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)

random.seed(50)

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[0])
TEST_DATE = dt.datetime(2018, 5, 22)

sys.path.append(os.path.join(PROJECT_DIR, 'trading', 'bitmex'))

from interface.utils import DataError
from .utils.generate_random_price_data import RandomizedEnvironmentGenerator
from .utils.mock.entities import OrderManager, MockManager
from ml_1 import TradingInterface

class args:
    box_size = 5
    n_input = 10
    restore = False
    DRY_RUN = True
    algos = []

class Config:
    SYMBOL = "XBTUSD"
    DIGITS = 1
    DRY_RUN = True
    LOG_LEVEL = "INFO"
    MAX_POSITION = 1.5
    LOOP_INTERVAL = 0.5

def display_test_name(func):
    def test_wrapper(self, *args, **kwargs):
        begin_msg = 'begin: {}'.format(func.__name__)
        finish_msg = 'finish: {}'.format(func.__name__)

        print('-' * (len(begin_msg) + 2))
        print(begin_msg)
        print('-' * (len(begin_msg) + 2))
        func(self, *args, **kwargs)
        print('-' * (len(finish_msg) + 2))
        print(finish_msg)
        print('-' * (len(finish_msg) + 2))

    return test_wrapper

def fetch_stale_initial_data():
    candles = [7914.0, 7915.0, 7916.0, 7917.0, 7918.0,
                7919.0, 7920.0, 7921.0, 7922.0, 7923.0, 7922.0]
    tstamps = [
                {'id': 2, 'tstamp': dt.datetime(2018, 4, 18, 4, 12, 14, tzinfo=tzutc())},
                {'id': 3, 'tstamp': dt.datetime(2018, 4, 18, 4, 12, 29, tzinfo=tzutc())},
                {'id': 4, 'tstamp': dt.datetime(2018, 4, 18, 4, 12, 54, tzinfo=tzutc())},
                {'id': 5, 'tstamp': dt.datetime(2018, 4, 18, 4, 18, 24, tzinfo=tzutc())},
                {'id': 6, 'tstamp': dt.datetime(2018, 4, 18, 4, 18, 44, tzinfo=tzutc())},
                {'id': 7, 'tstamp': dt.datetime(2018, 4, 18, 4, 30, 39, tzinfo=tzutc())},
                {'id': 8, 'tstamp': dt.datetime(2018, 4, 18, 4, 30, 49, tzinfo=tzutc())},
                {'id': 9, 'tstamp': dt.datetime(2018, 4, 18, 4, 41, 9, tzinfo=tzutc())},
                {'id': 10, 'tstamp': dt.datetime(2018, 4, 18, 4, 44, 19, tzinfo=tzutc())},
                {'id': 11, 'tstamp': dt.datetime(2018, 4, 18, 4, 50, 4, tzinfo=tzutc())},
                {'id': 12, 'tstamp': dt.datetime(2018, 4, 18, 4, 50, 20, tzinfo=tzutc())}
              ]

    return candles, tstamps

class TestExecution(unittest.TestCase):
    '''
    random.seed(99)
    make_overall_data.json -> RandomPriceGenerator([8276, 8285, 8260, 8295, 8290, 8305],
                    n_random_fluctuations=4,
                    variance=3,
                    max_deviation=None,
                    rounding=2)
    '''
    def setUp(self):
        self.manager = MockManager()
        self.interface = TradingInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD',
                                                args,
                                                ordermanager=OrderManager,
                                                initial_data=None,
                                                config=Config)

        Event.instances = {}
        PendingOrderManager.pending = []
        PendingOrderManager.DRY_RUN = True
        PendingOrderManager.interface = self.interface
        CustomEvent._queue = {}

    def tearDown(self):
        pass

    # @display_test_name
    # def test_setting_normal_switch_with_zero_exposure_raises_warning(self):
    #     with self.assertWarns(UserWarning) as cm:
    #         self.assertEqual(self.interface.set_level, 0)
    #         self.strategy.set_level_switch_to_normal_mode(1000)
    # @display_test_name
    # def test_crossing_of_soft_limit_exits_trend_mode_and_switches_to_normal_mode(self):
    #     _, _ = self.strategy.execute(bar=0, ticker=mock_tick)
    #     self.strategy.set_level_switch_to_normal_mode(1000)
    #     self.fail('write the test')
    #
    # @display_test_name
    # def test_crossing_of_soft_limit_with_positive_exposure_resets_exposure_to_zero_and_soft_limit_resets__to_none(self):
    #     self.fail('write the test')
    #
    # @display_test_name
    # def test_crossing_of_soft_limit_with_negative_exposure_resets_exposure_to_zero_and_soft_limit_resets__to_none(self):
    #     self.fail('write the test')
    #
    @display_test_name
    def test_setting_new_soft_limit_deregisters_old_limit(self):
        self.data = RandomizedEnvironmentGenerator(
            [8000, 8010],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2,
            randomize_ticker_last_price=False
        )

        cur_price=8000
        trigger_price=8200

        self.interface.set_soft_limit_switch(cur_price=cur_price, trigger_price=trigger_price)

        orderbook, trades, ticker = next(self.data)

        self.interface.engine(orderbook=orderbook, trades=trades, ticker=ticker, DRY_RUN=True)

        self.assertIsNotNone(CustomEvent.find(name='{}_soft_limit'.format(trigger_price)))

        # Change soft limit
        new_trigger_price = 7900
        self.interface.set_soft_limit_switch(cur_price=cur_price, trigger_price=new_trigger_price)

        self.interface.engine(orderbook=orderbook, trades=trades, ticker=ticker, DRY_RUN=True)

        self.assertIsNone(CustomEvent.find(name='{}_soft_limit'.format(trigger_price)))
        self.assertIsNotNone(CustomEvent.find(name='{}_soft_limit'.format(new_trigger_price)))

    @display_test_name
    def test_filled_limit_gives_rebate(self):
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8287, 8286, 8090],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2,
            randomize_ticker_last_price=False
        )

        # Place an order and confirm that fees are initially zero
        # before the order is filled

        b_confirm_fees_paid_initially_zero = False

        b_pending_buy_order_placed = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                           trades=trades,
                                           ticker=ticker,
                                           DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_type') == 'pending_buy':
                    b_pending_buy_order_placed = True
                    break

            if b_pending_buy_order_placed:
                break

        self.assertTrue(b_pending_buy_order_placed)
        self.assertEqual(self.interface.fees_paid(), 0)

        # Fill the pending order and assert that fees collected are positive

        self.data = RandomizedEnvironmentGenerator(
            [8281, 8280],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2
        )

        b_pending_buy_filled = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                         trades=trades,
                                         ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_state') == 'pending_buy_filled':
                    b_pending_buy_filled = True
                    break

        self.assertTrue(b_pending_buy_filled)
        self.assertGreater(self.interface.fees_paid(), 0)

    @display_test_name
    def test_open_orders_cancel_and_exposure_closes_if_price_crosses_soft_limit(self):
        self.data = RandomizedEnvironmentGenerator(
            [8265, 8281, 8280, 8287, 8289],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2,
            randomize_ticker_last_price=False
        )

        # check no orders are initially open
        orders = PendingOrderManager.fetch_all_orders()
        self.assertEqual(len(orders), 0)

        # open buy trade and non-filled buy order
        b_pending_buy_order_placed = False
        pending_order_price = None
        current_price = None
        for orderbook, trades, ticker in self.data:
            current_price = ticker['last']
            events = self.interface.engine(orderbook=orderbook,
                            trades=trades, ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_type') == 'pending_buy_filled':
                    b_pending_buy_order_placed = True
                    pending_order_price = events[0]['order_price']
                    break

            if b_pending_buy_order_placed:
                break

        orders = PendingOrderManager.fetch_all_orders()
        self.assertEqual(len(orders), 2)
        self.assertEqual(orders[0].ordStatus, 'Filled')
        self.assertEqual(orders[1].ordStatus, 'New')
        self.assertEqual(orders[1].price, 8287)
        self.assertEqual(current_price, 8289)

        # ----
        self.interface.set_soft_limit_switch(cur_price=current_price, trigger_price=8288)

        self.data = RandomizedEnvironmentGenerator(
            [8288.50, 8289, 8288],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2,
            randomize_ticker_last_price=False
        )

        # --- one event loop iteration
        orderbook, trades, ticker = next(self.data)

        self.assertEqual(ticker['last'], 8288.5)

        events = self.interface.engine(orderbook=orderbook,
                                       trades=trades,
                                       ticker=ticker,
                                       DRY_RUN=True)

        for e in events:
            d = e.eventDict
            if d['type'] == 'action' and d.get('action_type') == 'soft_limit_switch_set':
                self.assertEqual(d['value'], 8280)
                break

        # --- one event loop iteration
        # check normal iteration does not trigger the soft limit
        orderbook, trades, ticker = next(self.data)

        self.assertEqual(ticker['last'], 8289)

        events = self.interface.engine(orderbook=orderbook,
                        trades=trades, ticker=ticker, DRY_RUN=True)

        orders = PendingOrderManager.fetch_all_orders()
        _,open_exposure,_ = self.interface.get_vwap_and_total_volume_and_closed_pl()
        self.assertEqual(len(orders), 2)
        self.assertEqual(orders[0].ordStatus, 'Filled')
        self.assertEqual(orders[1].ordStatus, 'New')
        self.assertEqual(open_exposure, 100)

        pending_new_order_cl_ord_id = orders[1].clOrdID

        # --- one event loop iteration
        # --trigger the soft limit !!

        orderbook, trades, ticker = next(self.data)

        self.assertEqual(ticker['last'], 8288)

        self.interface.engine(orderbook=orderbook,
                        trades=trades, ticker=ticker, DRY_RUN=True)

        # updates the CustomEvent.engine() after trigger from previous engine update
        b_soft_limit_triggered = False

        events = self.interface.engine(orderbook=orderbook,
                        trades=trades, ticker=ticker, DRY_RUN=True)

        for e in events:
            d = e.eventDict
            if d['type'] == 'action' and d.get('message') == 'soft_limit_triggered':
                b_soft_limit_triggered = True

        self.assertTrue(b_soft_limit_triggered)

        # updates the pending order manager
        reverse_exposure_order_clOrdID = None
        reverse_exposure_order_quantity = 0

        events = self.interface.engine(orderbook=orderbook,
                        trades=trades, ticker=ticker, DRY_RUN=True)

        for e in events:
            d = e.eventDict
            if d['type'] == 'order' and d.get('order_type') == 'pending_sell':
                reverse_exposure_order_clOrdID = d['clOrdID']
                reverse_exposure_order_quantity = d['quantity']

        self.assertIsNotNone(reverse_exposure_order_clOrdID)
        self.assertEqual(reverse_exposure_order_quantity, -open_exposure)

        order = PendingOrderManager.get(clOrdID=pending_new_order_cl_ord_id)
        _,open_exposure,_ = self.interface.get_vwap_and_total_volume_and_closed_pl()

        self.assertEqual(order.ordStatus, 'Canceled')

        # test reverse exposure order tracks market price if market moves
        # away from the order
        b_reverse_exposure_order_amended = False

        reverse_exposure_order = PendingOrderManager.get(reverse_exposure_order_clOrdID)
        self.assertEqual(reverse_exposure_order.ordStatus, 'New')

        self.data = RandomizedEnvironmentGenerator(
            [8284, 8281],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2,
            randomize_ticker_last_price=False
        )
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                            trades=trades, ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'amend' and d.get('clOrdID') == reverse_exposure_order_clOrdID:
                    self.assertLess(d['new_price'], d['old_price'])
                    b_reverse_exposure_order_amended = True

        self.assertTrue(b_reverse_exposure_order_amended)

    @display_test_name
    def _test_sell_exposure_is_closed_once_market_retraces_to_hard_close_limit(self):
        '''
        NOTE: strategy temporary disables the use of market closes
        '''
        ticks = [
            [8276, 8270, 8272],
            [8271, 8265, 8267],
            [8266, 8260, 8262]
        ]

        for k, ti in enumerate(ticks):
            expected_volume = (k+1) * -100

            self.data = RandomizedEnvironmentGenerator(
                ti,
                TEST_DATE,
                n_random_fluctuations=3,
                variance=0,
                max_deviation=0,
                rounding=2
            )

            for orderbook, trades, ticker in self.data:
                self.interface.manager.set_ticker(ticker)
                result = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)
                PendingOrderManager.update(orderbook=orderbook, trades=trades)

            vwap, total_volume, _ = self.interface.get_vwap_and_total_volume_and_closed_pl()

            self.assertEqual(total_volume, expected_volume)

            self.assertGreater(vwap, 0)
            self.assertGreater(vwap, 8257)

        self.interface.set_hard_close_limit(vwap)

        self.data = RandomizedEnvironmentGenerator(
            [8257, vwap + 0.5],
            TEST_DATE,
            n_random_fluctuations=15,
            variance=2,
            max_deviation=5,
            rounding=2
        )

        b_hard_close_limit_triggered = False

        for orderbook, trades, ticker in self.data:
            self.interface.manager.set_ticker(ticker)
            result = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)
            events = result.get('events')

            for e in events:
                if e['type'] == 'safety' and e['message'] == 'hard_close_limit_triggered':
                    b_hard_close_limit_triggered = True
                    break

        self.assertTrue(b_hard_close_limit_triggered)

        _, total_volume, _ = self.interface.get_vwap_and_total_volume_and_closed_pl()

        self.assertEqual(total_volume, 0)


    @display_test_name
    def _test_buy_exposure_is_closed_once_market_retraces_to_hard_close_limit(self):
        '''
        NOTE: strategy temporary disables the use of market closes
        '''
        ticks = [
            [8276, 8283, 8281],
            [8282, 8288, 8286],
            [8287, 8293, 8291]
        ]

        for k, ti in enumerate(ticks):
            expected_volume = (k+1) * 100

            self.data = RandomizedEnvironmentGenerator(
                ti,
                TEST_DATE,
                n_random_fluctuations=1,
                variance=0,
                max_deviation=0,
                rounding=2
            )

            i=0
            for orderbook, trades, ticker in self.data:
                self.interface.manager.set_ticker(ticker)
                result = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)
                PendingOrderManager.update(orderbook=orderbook, trades=trades)
                i+=1

            vwap, total_volume, _ = self.interface.get_vwap_and_total_volume_and_closed_pl()

            self.assertEqual(total_volume, expected_volume)

            self.assertGreater(vwap, 0)
            self.assertLess(vwap, 8291)

        self.interface.set_hard_close_limit(vwap)

        self.data = RandomizedEnvironmentGenerator(
            [8291, vwap - 0.5],
            TEST_DATE,
            n_random_fluctuations=15,
            variance=2,
            max_deviation=5,
            rounding=2
        )

        b_hard_close_limit_triggered = False

        for orderbook, trades, ticker in self.data:
            self.interface.manager.set_ticker(ticker)
            result = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)
            events = result.get('events')

            for e in events:
                if e['type'] == 'safety' and e['message'] == 'hard_close_limit_triggered':
                    b_hard_close_limit_triggered = True
                    break

        self.assertTrue(b_hard_close_limit_triggered)

        _, total_volume, _ = self.interface.get_vwap_and_total_volume_and_closed_pl()

        self.assertEqual(total_volume, 0)


    @display_test_name
    def test_sell_order_cancels_if_second_order_opens(self):
        # Assert that first sell pending order opens, make sure it does not fill
        data = RandomizedEnvironmentGenerator(
            [8276, 8270, 8265, 8263, 8261],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=0,
            max_deviation=0,
            rounding=2
        )

        b_new_pending = False
        for orderbook, trades, ticker in data:
            events = self.interface.engine(orderbook=orderbook,
                                         trades=trades,
                                         ticker=ticker, DRY_RUN=True)
            for e in events:
                if e['type'] == 'order' and e.eventDict.get('order_type') == 'pending_sell':
                    b_new_pending = True
                    break

            if b_new_pending:
                break

        from trading.utils import runtime
        runtime.Engine.update(orderbook=orderbook, trades=[])
        results = PendingOrderManager.fetch_non_filled_orders()
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0].ordStatus, 'New')

        order1_clOrdID = results[0].clOrdID

        # Assert that second sell pending order opens, make sure it does not fill
        for orderbook, trades, ticker in data:
            events = self.interface.engine(orderbook=orderbook,
                                          trades=trades,
                                          ticker=ticker, DRY_RUN=True)

        results = PendingOrderManager.fetch_non_filled_orders()

        self.assertEqual(results[0].ordStatus, 'New')

        order2_clOrdID = results[0].clOrdID

        self.assertNotEqual(order1_clOrdID, order2_clOrdID)

        # Assert that the first order is canceled
        order1 = PendingOrderManager.get(clOrdID=order1_clOrdID)
        self.assertEqual(order1.ordStatus, 'Canceled')


    @display_test_name
    def _test_sell_order_cancels_if_price_crosses_soft_limit(self):
        '''
        NOTE: test disabled and replaced place tags in runtime.OrderPlaceEvent,
            e.g. - cancel_on_new_pending_order_placed
        '''
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8271, 8266, 8263, 8250],
            TEST_DATE,
            n_random_fluctuations=1,
            variance=0,
            max_deviation=0,
            rounding=2
        )

        b_pending_sell_order_placed = False
        pending_order_price = None

        for orderbook, trades, ticker in self.data:
            self.interface.manager.set_ticker(ticker)
            events = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)

            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_type') == 'pending_sell':
                    b_pending_sell_order_placed = True
                    pending_order_price = d['order_price']
                    break

            orders = PendingOrderManager.fetch_orders_by_status(['New'])

            if len(orders) == 1:
                if orders[0].side == 'sell':
                    b_pending_sell_order_placed = True
                    pending_order_price = orders[0].price
                    break

        self.assertTrue(b_pending_sell_order_placed)
        self.assertGreater(pending_order_price, 0)

        b_pending_sell_order_cancelled = False
        pending_order_volume = None
        for orderbook, trades, ticker in self.data:
            self.interface.manager.set_ticker(ticker)
            events = self.interface.main(orderbook, trades, ticker, DRY_RUN=True)

            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_state') == 'cancel':
                    b_pending_sell_order_cancelled = True
                    pending_order_volume = d['order_quantity']
                    break

        self.assertTrue(b_pending_sell_order_cancelled)
        self.assertLess(pending_order_volume, 0)

    @display_test_name
    def _test_price_deviation_between_initial_data_and_most_recent_tick_raises_exception(self):
        '''
        NOTE: test disabled because DataError is no longer thrown in ml_1.run_init
        raises a DataError when the next tick is greater than the box_size
        '''
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8285],
            TEST_DATE,
            n_random_fluctuations=20,
            variance=3,
            max_deviation=None,
            rounding=2
        )
        self.interface = TradingInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD',
                                                args,
                                                ordermanager=OrderManager,
                                                initial_data=fetch_stale_initial_data,
                                                config=Config)

        initial_candle = self.interface.candles[-1]
        self.assertEqual(initial_candle, 7922.0)

        orderbook, trades, ticker  = next(self.data)
        recent_tick = ticker['last']
        self.assertGreater(recent_tick, 8000)

        with self.assertRaises(DataError):
            self.interface.manager.set_ticker(ticker)
            self.interface.run_init(self.manager)


    @display_test_name
    def test_pending_sell_order_fill(self):
        '''
        default setting is max_deviation_before_cancel = box_size
                           max_deviation_before_reset = args.box_size // 2
            if kwargs['priority'] = high, then cancel / reset values are both 0
        '''
        data = RandomizedEnvironmentGenerator(
            [8276, 8266, 8269, 8271],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=3,
            max_deviation=None,
            rounding=2,
            orderbook_reset_sensitivity=2
        )

        b_pending_sell_order_placed = False
        for orderbook, trades, ticker in data:
            events = self.interface.engine(orderbook=orderbook,
                                         trades=trades,
                                         ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d['order_type'] == 'pending_sell':
                    b_pending_sell_order_placed = True
                    break

            if b_pending_sell_order_placed:
                break

        self.assertTrue(b_pending_sell_order_placed)

        b_pending_sell_filled = False
        for orderbook, trades, ticker in data:
            events = self.interface.engine(orderbook=orderbook,
                                         trades=trades,
                                         ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_state') == 'pending_sell_filled':
                    b_pending_sell_filled = True
                    break

            if b_pending_sell_filled:
                break

        self.assertTrue(b_pending_sell_filled)

    @display_test_name
    def test_pending_buy_order_fill(self):
        '''
        default setting is max_deviation_before_cancel = box_size
                           max_deviation_before_reset = args.box_size // 2
            if kwargs['priority'] = high, then cancel / reset values are both 0
        '''
        self.data = RandomizedEnvironmentGenerator(
            [8270 , 8281, 8278, 8272],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=1,
            max_deviation=None,
            rounding=2,
            orderbook_reset_sensitivity=2
        )

        b_pending_buy_order_placed = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                        trades=trades,
                                        ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_type') == 'pending_buy':
                    b_pending_buy_order_placed = True
                    break

            if b_pending_buy_order_placed:
                break

        self.assertTrue(b_pending_buy_order_placed)

        b_pending_buy_filled = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                        trades=trades,
                                        ticker=ticker, DRY_RUN=True)
            for e in events:
                d = e.eventDict
                if d['type'] == 'order' and d.get('order_state') == 'pending_buy_filled':
                    b_pending_buy_filled = True

        self.assertTrue(b_pending_buy_filled)

    @display_test_name
    def test_enters_trend_down_mode(self):
        '''
        NOTE: test should be fixed: trend mode is set to trigger based on number of
             consecutive candle bars, not net exposure
        '''
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8270, 8271,
             8270, 8264, 8265,
             8264, 8258, 8259,
             8258, 8252, 8253,
             8252, 8248, 8249,
             8248, 8242, 8243
            ],
            TEST_DATE,
            n_random_fluctuations=0,
            variance=1,
            max_deviation=2,
            rounding=2
        )

        b_found_trend_down = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                         trades=trades,
                                         ticker=ticker, DRY_RUN=True)
            for e in events:
                equery = e.eventDict.get
                if equery('type') == 'state_change' and equery('obj') == 'strategy' and equery('variable') == 'mode':
                    b_found_trend_down = True
                    break

            if b_found_trend_down:
                break

        self.assertTrue(b_found_trend_down)

        expected_volume = (self.interface.strategy.consecutive_bars_until_trend_mode - 1) * -100
        _,vol,_ = self.interface.get_vwap_and_total_volume_and_closed_pl()
        self.assertEqual(expected_volume, vol)

    @display_test_name
    def test_enters_trend_up_mode(self):
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8282, 8281,
             8282, 8288, 8287,
             8288, 8294, 8293,
             8294, 8300, 8299,
             8300, 8306, 8305,
             8306, 8312, 8311],
            TEST_DATE,
            n_random_fluctuations=3,
            variance=4,
            max_deviation=4,
            rounding=2,
            orderbook_reset_sensitivity=3
        )

        b_found_trend_up = False
        for orderbook, trades, ticker in self.data:
            events = self.interface.engine(orderbook=orderbook,
                                          trades=trades,
                                          ticker=ticker, DRY_RUN=True)
            for e in events:
                equery = e.eventDict.get
                if equery('type') == 'state_change' and equery('obj') == 'strategy' and equery('variable') == 'mode':
                    b_found_trend_up = True
                    break

            if b_found_trend_up:
                break

        self.assertTrue(b_found_trend_up)
