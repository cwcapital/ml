import unittest
from trading.utils.tests.helper.tags import display_test_name
from trading.utils.custom_events import CustomEvent

class TestCustomEvent(unittest.TestCase):
    def setUp(self):
        CustomEvent._queue = {}

    @display_test_name
    def test_triggered_events_automatically_deregister(self):
        def greater_than_six(**kwargs):
            return kwargs['val'] > 6, {}

        def callback(**kwargs):
            pass

        self.assertEqual(len(CustomEvent._queue), 0)

        event = CustomEvent.register('price_gt_6', greater_than_six, [callback])

        self.assertEqual(len(CustomEvent._queue), 1)

        # Trigger the event and check that event is removed from CustomEvent queue
        event.set_params(val=7)

        CustomEvent.engine()

        self.assertEqual(len(CustomEvent._queue), 0)



    @display_test_name
    def test_registered_event_is_triggered(self):
        def greater_than_six(**kwargs):
            return kwargs['val'] > 6, {'msg': 'successful!!!'}

        res = {
            'b_event_is_triggered': False,
            'msg': None
        }

        def callback_1(**kwargs):
            res['b_event_is_triggered'] = True

        def callback_2(**kwargs):
            res['msg'] = kwargs['msg']

        price_gt_6 = CustomEvent.register('price_gt_6', greater_than_six, [callback_1, callback_2])

        price_gt_6.set_params(val=6)

        result = CustomEvent.engine()

        self.assertEqual(result, [])

        price_gt_6.set_params(val=7)

        result = CustomEvent.engine()

        self.assertEqual(result, ['price_gt_6'])
        self.assertEqual(res['b_event_is_triggered'], True)
        self.assertEqual(res['msg'], 'successful!!!')
