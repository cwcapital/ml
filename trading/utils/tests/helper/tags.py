def display_test_name(func):
    def test_wrapper(self, *args, **kwargs):
        begin_msg = 'begin: {}'.format(func.__name__)
        finish_msg = 'finish: {}'.format(func.__name__)

        print('-' * (len(begin_msg) + 2))
        print(begin_msg)
        print('-' * (len(begin_msg) + 2))
        func(self, *args, **kwargs)
        print('-' * (len(finish_msg) + 2))
        print(finish_msg)
        print('-' * (len(finish_msg) + 2))

    return test_wrapper
