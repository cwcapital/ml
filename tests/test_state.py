import pickle
import unittest
import os
import sys
from pathlib import Path
import datetime as dt
import random
from dateutil.tz import tzutc
from time import sleep

random.seed(99)

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[0])
TEST_DATE = dt.datetime(2018, 5, 22)

sys.path.append(os.path.join(PROJECT_DIR, 'trading', 'bitmex', 'interface'))

class args:
    box_size = 5
    n_input = 10
    restore = False
    time_until_saved_state_considered_stale_in_seconds = 0.01
    algos = []

def fetch_initial_data():
    candles = [8277.0, 8278.0]
    tstamps = [
                {'id': 2, 'tstamp': dt.datetime(2018, 4, 18, 4, 12, 14, tzinfo=tzutc())},
                {'id': 3, 'tstamp': dt.datetime(2018, 4, 18, 4, 12, 29, tzinfo=tzutc())}
              ]

    return candles, tstamps


class OrderManager:
    def __init__(self, run_init, run_main_loop, run_deinit, orderbook_type):
        self.exchange = None
        self.run_init = run_init
        self.run_main_loop = run_main_loop
        self.run_deinit = run_deinit

    def init(self):
        self.start_time = dt.datetime.now()
        self.starting_qty = 0
        self.running_qty = 0
        self.ticker = None

    def set_ticker(self, ticker):
        self.ticker = ticker

    def get_ticker(self):
        return self.ticker


sys.path.append(os.path.join(PROJECT_DIR, 'trading', 'bitmex'))

# from interface.utils import DataError
from .utils.generate_random_price_data import RandomizedEnvironmentGenerator
from ml_1 import TradingInterface

class MockManager:
    def __init__(self):
        pass

class TestExecution(unittest.TestCase):
    def setUp(self):
        self.manager = MockManager()
        self.interface = TradingInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD',
                                                args,
                                                ordermanager=OrderManager,
                                                initial_data=fetch_initial_data)
        self.data = RandomizedEnvironmentGenerator(
            [8276, 8271],
            TEST_DATE,
            n_random_fluctuations=1,
            variance=0,
            max_deviation=0,
            rounding=2
        )

        # with open(os.path.join(PROJECT_DIR, 'trading', 'bitmex', 'interface', 'state.pkl'), 'rb') as f:
        #     self.state = pickle.load(f)

    def tearDown(self):
        pass

    def _test_state_pickle_file_is_created(self):
        mock = {

        }
        self.assertIsNotNone(self.state['timestamp'])

    def _test_restore_before_expiration_does_not_log_error(self):
        '''
        NOTE: restore no longer has an expiration
        '''
        seconds_elapsed = (dt.datetime.now() - self.state['timestamp']).seconds
        valid_limit = seconds_elapsed + 100

    def _test_restore_after_expiration_is_logged_as_error(self):
        '''
        NOTE: removed test since there init no longer throws an exception,
           warns when historical price deviates too much from current price
        '''
        for orderbook, trades, ticker in self.data:
            with self.assertLogs('__main__', level='ERROR') as cm:
                self.interface.manager.set_ticker(ticker)
                self.interface.run_init(state_filename='mock_state_1.pkl')

        self.assertIn('ERROR:__main__:stale state::seconds_passed >= {}'.format(
            args.time_until_saved_state_considered_stale_in_seconds
        ), cm.output)
