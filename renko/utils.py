import numpy as np
# import tensorflow as tf
import os
from random import randint, choice as random_choice
import threading
import sys
import calendar
import datetime as dt

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def custom_cross_entropy(labels, choices, n_output_nodes):
    best_choice = tf.argmax(choices, axis=1)
    correct = tf.equal(labels, best_choice)

    best_choice_val = tf.map_fn(lambda x: tf.reduce_max(x), choices)
    w = -tf.log(best_choice_val)

    unbiased_choice = tf.cast(1 / tf.constant(n_output_nodes), tf.float32)
    l = -tf.log((1.0 - tf.abs(best_choice_val-unbiased_choice)) / 10.0)

    loss = tf.where(correct, w, l)
    return loss

def check_predictions(labels, predictions, threshold):
    choices = tf.argmax(predictions, axis=1)
    correct = tf.equal(labels, choices)

    choice_val = tf.map_fn(lambda x: tf.reduce_max(x), predictions, dtype=tf.float32)
    met_threshold = tf.greater_equal(choice_val, threshold)

    w = tf.constant(1, dtype=tf.int32)
    l = tf.constant(0, dtype=tf.int32)

    return tf.cast(tf.logical_and(correct, met_threshold), dtype=tf.int32)

def trinary_category(category, no_of_categories):
    if category == 0:
        category_group = 0
    elif category == (2 ** no_of_categories) - 1:
        category_group = 1
    else:
        category_group = 2
    return category_group

def directional_category(values):
    seq = [1 if val == 0 else -1 for val in values[1:]]
    seq_sum = sum(seq)
    if seq_sum > 0:
        return 0
    elif seq_sum < 0:
        return 1
    else:
        return 2

def identity_category(values):
    return values[1]

class Tick:
    def __init__(self, csv_line):
        tstamp,bid,ask = csv_line.split(',')
        self.tstamp = dt.datetime.strptime(tstamp, '%Y-%m-%d %H:%M:%S')
        self.bid = float(bid)
        self.ask = float(ask)

class RenkoCandle:
    def __init__(self, tstamp, prevCandleDirection, tick):
        self.tstamp = tstamp
        self.open = tick
        self.close = tick
        self._prev = prevCandleDirection
        self._dir = -1

    def update(self, tick, box_size):
        if self._dir < 0:
            if self._prev == 0:
                if tick - self.open >= box_size:
                    self.close = self.open + box_size
                    self._dir = 0

                if self.open - tick >= (2 * box_size):
                    self.open -= box_size
                    self.close = self.open - box_size
                    self._dir = 1

            elif self._prev == 1:
                if tick - self.open >= (2 * box_size):
                    self.open += box_size
                    self.close = self.open + box_size
                    self._dir = 0

                if self.open - tick >= box_size:
                    self.close = self.open - box_size
                    self._dir = 1

            else: # first candle (no previous candle)

                   if tick - self.open >= box_size:
                       self.close = self.open + box_size
                       self._dir = 0

                   if self.open - tick >= box_size:
                       self.close = self.open - box_size
                       self._dir = 1

        return self._dir >= 0

    def render(self):
        o,h,l,c = (None,None,None,None)

        if self._dir == 0:
          o = self.open
          h = self.close
          l = self.open
          c = self.close


        if self._dir == 1:
          o = self.open
          h = self.open
          l = self.close
          c = self.close

        # if VERBOSE:
        #     return [self.tstamp, o, h, l, c]
        # else:

        return c

def argmax(iterable):
    i=0
    m=iterable[0]
    for j,k in enumerate(iterable[1:]):
        if k > m:
            m = k
            i = j+1
    return i

def get_var(name):
    all_vars = tf.global_variables()
    for i in range(len(all_vars)):
        if all_vars[i].name.startswith(name):
            return all_vars[i]

def flatten(myList):
    return [item for sublist in myList for item in sublist]

def save_to_csv(logits, y, index, **kwargs):
    assert(logits.shape[0] == y.shape[0])
    model_type = kwargs['model_type']

    if kwargs.get('model'):
        with open('analysis/data_{}_model.csv'.format(index), 'w') as f:
            X = kwargs.get('X_batch')
            if hasattr(X, 'any') and X.any():
                for data, res, x in zip(logits, y, X):
                    x = flatten(x)
                    row1 = ','.join(str(i) for i in x) + '\n'
                    f.write(row1)
            else:
                for data, res in zip(logits, y):
                    row = ','.join(str(i) for i in data) + ',{}\n'.format(res)
                    f.write(row)

    if kwargs.get('excel'):
        with open('analysis/data_{}_excel.csv'.format(index), 'w') as f:
            X = kwargs.get('X_batch')
            if hasattr(X, 'any') and X.any():
                for data, res, x in zip(logits, y, X):
                    x = flatten(x)
                    row1a = ','.join(str(i) for i in x[::2]) + '\n'
                    row1b = ','.join(str(i) for i in x[1::2]) + '\n'
                    row2 = ','.join(str(i) for i in data) + ',{}\n'.format(res)
                    f.write(row1a)
                    f.write(row1b)
                    f.write(row2)
            else:
                for data, res in zip(logits, y):
                    row = ','.join(str(i) for i in data) + ',{}\n'.format(res)
                    f.write(row)

def ticksToRenko(tickIterator, box_size, tickObj):
    HOUR_IN_MILLISECONDS = 60 * 60 * 1000
    candles = []

    candle_ts = 0
    first_tick = tickObj(next(tickIterator))
    candle = RenkoCandle(candle_ts, -1, first_tick.bid)
    id_to_tstamp = []

    tick = first_tick
    prev_tick = first_tick
    for next_tick in (tickObj(t) for t in tickIterator):
        bHasCompleted = candle.update(tick.bid, box_size)
        if bHasCompleted:
            # add renko candle to candles[] after has completely formed
            candles.append(candle.render())

            # begin new candle
            candle_ts += 1

            # double increment if there is a gap >= 1 hour
            if (next_tick.tstamp - tick.tstamp).seconds * 1000 >= HOUR_IN_MILLISECONDS:
                candle_ts += 1
                last_candle_direction = candle._dir
                this_candle_open_prc = candle.close
            else:
                last_candle_direction = candle._dir
                this_candle_open_prc = candle.close

            id_to_tstamp.append({
                'id': candle_ts,
                'tstamp': prev_tick.tstamp
            })

            prev_tick = next_tick
            candle = RenkoCandle(candle_ts,
                    last_candle_direction, this_candle_open_prc)

        tick = next_tick
    return [candles, id_to_tstamp]

def seconds_since_start_of_day(tstamp):
    return tstamp.hour * 3600 + tstamp.minute * 60 + tstamp.second

def toRelativeCandles(candles, timestamps):
    if candles:
        rel_candles = []
        rel_tstamps = []
        prev_candle = None
        for candle, ts in zip(candles, timestamps):
            if prev_candle:
                if candle > prev_candle:
                    rel_candles.append(0)
                else:
                    rel_candles.append(1)

            rel_tstamps.append(seconds_since_start_of_day(ts['tstamp']))
            prev_candle = candle

        return rel_candles, rel_tstamps

    return [], []

class AccuracyMeasurer:
    def __init__(self, name):
        self.name = name
        self.count = 0
        self.total = 0
        self.min = sys.float_info.max
        self.max = 0

    def add(self, val):
        if val < self.min:
            self.min = val
        if val > self.max:
            self.max = val

        self.total += val
        self.count += 1

    def print_results(self):
        print(self.name, '- min: {}, max: {}, avg: {}'.format(
            self.min, self.max, self.total / self.count
        ))

class DataLoader:
    def __init__(self, dataInputDir, max_files_opened,
          max_reuse_count_before_data_reload, batch_size,
          shuffle_data=True, random_batch_selection=True, **kwargs):
        self.max_files_opened = max_files_opened
        self.max_reuse_count_before_data_reload = max_reuse_count_before_data_reload
        self.batch_size = batch_size
        self.reuse_count = 0
        self._next_batch = None
        self.reshuffle_count = 0
        self.shuffle_data = shuffle_data
        self.dataInputDir = dataInputDir
        self.batch_index = 0
        self.time_steps = kwargs.get('time_steps')
        self.random_batch_selection = random_batch_selection
        self.data = self._load_test_data(max_files_opened)
        self.next_batch_worker_process = lambda: threading.Thread(
                    name='prepare_next_batch',
                    target=self._prepare_next_batch,
                    args=(batch_size,))

        self.thread = self.next_batch_worker_process()
        self.thread.start()

        self.unstack = kwargs.get('unstack')

        self.validation_data_size = 0
        self.validation_data = self._load_validation_data()

        if kwargs.get('DEBUG'):
            self.DEBUG = kwargs['DEBUG']
        else:
            self.DEBUG = False

    def transform_deprecated(self, x_batch, y_pred, n_partition, n_x_batch_inputs):
        '''expects a x_batch array of rank 2,
        chops it into n_partition/s, and creates
        a phony y_pred array, whose last element is
        the value that is to be predicted'''

        size = x_batch.shape[0] * x_batch.shape[1]
        n = size // n_partition
        x_i = np.reshape(x_batch, [n_partition, n])

        y_i = x_i[:,0:1]
        y_i_plus1 = y_i[1:]
        y_i_star = np.vstack((y_i_plus1, y_pred))
        y_i_star = y_i_star.reshape((-1))

        x_i = x_i.reshape([ n_partition, -1, n_x_batch_inputs ])

        return x_i, y_i_star

    def transform(self, x_batch, y_batch):
        x_batch = x_batch.reshape([-1, self.time_steps, 2])
        return x_batch, y_batch

    def next_batch(self):
        if self.thread:
            self.thread.join()
        next_batch = self._next_batch
        self.thread = self.next_batch_worker_process()
        self.thread.start()

        X_batch, y_batch = self.transform(next_batch[:,:-1], next_batch[:,-1])

        return X_batch, y_batch

    def _get_next_batch(self, batch_size):
        if self.random_batch_selection:
            rows = self.data.shape[0]
            i = randint(0, rows-batch_size)
        else:
            i = self.batch_index
            i_next = (i + batch_size) % self.data_len

        if i_next < i:
            next_batch = np.vstack((self.data[i:], self.data[:i_next]))
            self.batch_index = i_next
        else:
            next_batch = self.data[i:i_next]
            self.batch_index += batch_size

        self._next_batch = next_batch

    def next_validation_batch(self):
        # rand_row = randint(0, self.validation_data_size)
        X_batch, y_batch = self.validation_data

        X_batch, y_batch = self.transform(X_batch, y_batch)

        return X_batch, y_batch

        # X_batch_row = np.reshape(X_batch[rand_row], [-1, 1])
        # y_batch_row = y_batch[rand_row]
        #
        # n_partition = 100
        # X_batch_transformed, y_batch_transformed = self.transform(
        #     X_batch_row, y_batch_row, n_partition, self.unstack
        # )

        # return X_batch_transformed, y_batch_transformed
        # return self.validation_data

    def _load_validation_data(self):
        file_count = self._get_file_count('validation')
        index_range = list(range(file_count))

        results = []
        for k in index_range:
            filename = os.path.join(SCRIPT_DIR, self.dataInputDir, 'validation', 'results_{}.npy'.format(k))
            results.append(np.load(filename))

        results = np.vstack(results)
        self.validation_data_size = results.shape[0]
        if self.shuffle_data:
            print('shuffling validation data ...')
            np.random.shuffle(results)

        X_batch = results[:,:-1]
        y_batch = results[:,-1]

        return X_batch, y_batch

    def _load_test_data(self, no_of_files):
        # with open(os.path.join(SCRIPT_DIR, self.dataInputDir))

        indices = []
        file_count = self._get_file_count('test')
        index_range = list(range(file_count))
        if no_of_files:
            no_of_files = min(no_of_files, file_count)
        else:
            no_of_files = file_count

        for i in range(no_of_files):
            choice = random_choice(index_range)
            indices.append(choice)
            index_range.remove(choice)

        results = []
        for k in indices:
            filename = os.path.join(SCRIPT_DIR, self.dataInputDir, 'test', 'results_{}.npy'.format(k))
            results.append(np.load(filename))

        results = np.vstack(results)
        if self.shuffle_data:
            print('shuffling test data ...')
            np.random.shuffle(results)

        self.data_len = results.shape[0]
        self.max_reshuffle_count = self.data_len // 1.5

        return results

    def _prepare_next_batch(self, batch_size):
        self.reuse_count += 1
        self.reshuffle_count += 1
        # check file reload conditions

        if self.max_reuse_count_before_data_reload and \
           self.reuse_count > self.max_reuse_count_before_data_reload:
            if self.DEBUG:
                print('loading new file data ...')

            self.data = self._load_test_data(self.max_files_opened)
            self.reuse_count = 0
            self.reshuffle_count = 0

            if self.DEBUG:
                print('data finished loading')

        # check data reshuffle conditions
        if self.shuffle_data and self.reshuffle_count > self.max_reshuffle_count:
            if self.DEBUG:
                print('reshuffling data ...')

            np.random.shuffle(self.data)

            self.reshuffle_count = 0

            if self.DEBUG:
                print('finished reshuffling data')

        # this call will set self._next_batch and terminate
        self._get_next_batch(batch_size)

    def _get_file_count(self, file_type):
        filepath = os.path.join(SCRIPT_DIR, self.dataInputDir, file_type)
        files = [name for name in os.listdir(filepath)]
        return len(files)


if __name__ == '__main__':
    dataloader = DataLoader('ticks/final', max_files_opened=5,
      max_reuse_count_before_data_reload=50000, shuffle_data=True,
      random_batch_selection=True, batch_size=args.batch_size)
    X_batch, y_batch = dataloader.next_batch()
    print('X_batch: ', X_batch)
    print('y_batch: ', y_batch)
    print('validation data', dataloader.validation_data)
