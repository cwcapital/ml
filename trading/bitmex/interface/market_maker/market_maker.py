from __future__ import absolute_import
from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal
import logging
import warnings

import utils.config as Config
from . import bitmex
from .utils import errors

import os
from pathlib import Path

from copy import deepcopy

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = str(Path(SCRIPT_DIR).parents[1])
sys.path.append(PROJECT_DIR)

class ExchangeInterface:
    def __init__(self, orderbook_type, dry_run=False, config=None):
        '''
        orderbook_type { orderBookL2, orderBook10, instrument }
        '''
        self.dry_run = dry_run
        self.symbol = config.SYMBOL
        self.orderbook_type = orderbook_type
        # orderIDSuffix = {
        #     'ORDERID_RECONCILIATION_SUFFIX': settings.ORDERID_RECONCILIATION_SUFFIX,
        #     'ORDERID_RECONCILIATION_HEDGE': settings.ORDERID_RECONCILIATION_HEDGE
        # }
        self.logger = logging.getLogger('__marketmaker__')



        self.bitmex = bitmex.BitMEX(orderbook_type=orderbook_type,
                                    base_url=config.BASE_URL, symbol=self.symbol,
                                    apiKey=config.API_KEY,
                                    apiSecret=config.API_SECRET
                                    )
                                    # ,
                                    #  orderIDPrefix=settings.ORDERID_PREFIX,
                                    # orderIDSuffix=orderIDSuffix)

    def get_recent_trades(self):
        return self.bitmex.recent_trades()

    def get_orderbook(self):
        return deepcopy(self.bitmex.ws.get_orderbook(self.orderbook_type))

    def flush_data(self):
        return self.bitmex.ws.flush_data()

    def cancel_order(self, order):
        self.logger.info("Cancelling: %s %d @ %.2f" % (order['side'], order['orderQty'], "@", order['price']))
        while True:
            try:
                self.bitmex.cancel(order['orderID'])
                sleep(0.5)
            except ValueError as e:
                self.logger.info(e)
                sleep(0.5)
            else:
                break

    def fetch_user_margin(self):
        return self.bitmex.fetch_user_margin()

    def send_market(self, orders):
        if self.dry_run:
            return

        # self.logger.info("Resetting current position. Closing all existing orders.")

        # In certain cases, a WS update might not make it through before we call this.
        # For that reason, we grab via HTTP to ensure we grab them all.

        if len(orders):
            for order in orders:
                self.logger.info("Sending market order: %s %d" % (order['side'], order['orderQty']))

            self.bitmex.send_market(orders)

        sleep(0.5)


    def cancel_all_orders(self, side=None):
        # side = { 'Buy', 'Sell', None }
        if self.dry_run:
            return

        self.logger.info("Resetting current position. Cancelling all existing orders.")

        # In certain cases, a WS update might not make it through before we call this.
        # For that reason, we grab via HTTP to ensure we grab them all.
        orders = self.bitmex.http_open_orders()
        if side:
            orders = [o for o in orders if o['side'] == side]

        for order in orders:
            self.logger.info("Cancelling: %s %d @ %.2f" % (order['side'], order['orderQty'], order['price']))

        if len(orders):
            self.bitmex.cancel([order['orderID'] for order in orders])

        sleep(0.5)


    def calc_delta(self):
        """Calculate currency delta for portfolio"""
        portfolio = self.get_portfolio()
        spot_delta = 0
        mark_delta = 0
        for symbol in portfolio:
            item = portfolio[symbol]
            if item['futureType'] == "Quanto":
                spot_delta += item['currentQty'] * item['multiplier'] * item['spot']
                mark_delta += item['currentQty'] * item['multiplier'] * item['markPrice']
            elif item['futureType'] == "Inverse":
                spot_delta += (item['multiplier'] / item['spot']) * item['currentQty']
                mark_delta += (item['multiplier'] / item['markPrice']) * item['currentQty']
            elif item['futureType'] == "Linear":
                spot_delta += item['multiplier'] * item['currentQty']
                mark_delta += item['multiplier'] * item['currentQty']
        basis_delta = mark_delta - spot_delta
        delta = {
            "spot": spot_delta,
            "mark_price": mark_delta,
            "basis": basis_delta
        }
        return delta

    def get_instrument(self, symbol=None):
        if symbol is None:
            symbol = self.symbol
        return self.bitmex.instrument(symbol)

    def get_margin(self):
        if self.dry_run:
            return {'marginBalance': float(config.DRY_BTC), 'availableFunds': float(config.DRY_BTC)}
        return self.bitmex.funds()

    def get_highest_buy(self):
        buys = [o for o in self.get_orders() if o['side'] == 'Buy']
        if not len(buys):
            return {'price': -2**32}
        highest_buy = max(buys or [], key=lambda o: o['price'])
        return highest_buy if highest_buy else {'price': -2**32}

    def get_lowest_sell(self):
        sells = [o for o in self.get_orders() if o['side'] == 'Sell']
        if not len(sells):
            return {'price': 2**32}
        lowest_sell = min(sells or [], key=lambda o: o['price'])
        return lowest_sell if lowest_sell else {'price': 2**32}  # ought to be enough for anyone

    def get_ticker(self, symbol=None):
        if symbol is None:
            symbol = self.symbol
        return self.bitmex.ticker_data(symbol)

    def is_open(self):
        """Check that websockets are still open."""
        return not self.bitmex.ws.exited

    def check_market_open(self):
        instrument = self.get_instrument()
        if instrument["state"] != "Open" and instrument["state"] != "Closed":
            raise errors.MarketClosedError("The instrument %s is not open. State: %s" %
                                           (self.symbol, instrument["state"]))
            sys.exit()

    def check_if_orderbook_empty(self):
        """This function checks whether the order book is empty"""
        instrument = self.get_instrument()
        if instrument['midPrice'] is None:
            raise errors.MarketEmptyError("Orderbook is empty, cannot quote")
            sys.exit()

    def amend_bulk_orders(self, orders):
        if self.dry_run:
            return orders
        return self.bitmex.amend_bulk_orders(orders)

    def create_bulk_orders(self, orders, suffix=""):
        if self.dry_run:
            return orders

        return self.bitmex.create_bulk_orders(orders, suffix)

    def cancel_bulk_orders(self, orders):
        if self.dry_run:
            return orders
        return self.bitmex.cancel([order['orderID'] for order in orders])


class OrderManager:
    def __init__(self, run_init, run_main_loop, run_deinit, orderbook_type, config=None):
        '''
        orderbook_type { orderBookL2, orderBook10, instrument }
        '''
        self.config = config

        self.exchange = ExchangeInterface(orderbook_type, self.config.DRY_RUN, self.config)
        # Once exchange is created, register exit handler that will always cancel orders
        # on any error.
        self.run_init = run_init
        self.run_main_loop = run_main_loop
        self.run_deinit = run_deinit

        atexit.register(self.exit)
        signal.signal(signal.SIGTERM, self.exit)

        self.logger = logging.getLogger('__ordermanager__')
        self.logger.setLevel(logging.INFO)

        self.logger.info("Using symbol %s." % self.exchange.symbol)

    def init(self):
        if self.config.DRY_RUN:
            self.logger.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            self.logger.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        # self.instrument = self.exchange.get_instrument()

        self.starting_qty = 0
        self.running_qty = self.starting_qty

        self.reset()

        self.run_init(ignore_initial_candle=True)

    def reset(self):
        self.exchange.cancel_all_orders()

    def get_ticker(self):
        return self.exchange.get_ticker()

    ###
    # Orders
    ###
    def place_orders(self, buy_orders, sell_orders):
        """Create order items for use in convergence."""
        return self.converge_orders(buy_orders, sell_orders)

    def converge_orders(self, buy_orders, sell_orders, reason=None):
        """Converge the orders we currently have in the book with what we want to be in the book.
           This involves amending any open orders and creating new ones if any have filled completely.
           We start from the closest orders outward."""

        tickLog = self.exchange.get_instrument()['tickLog']
        to_amend = []
        to_create = []
        to_cancel = []
        buys_matched = 0
        sells_matched = 0
        existing_orders = self.exchange.get_orders()

        suffix = ""
        if reason == "RECONCILIATION":
            suffix = '_' + self.exchange.bitmex.orderIDSuffix['ORDERID_RECONCILIATION_SUFFIX']
        elif reason == "HEDGE":
            suffix = '_' + self.exchange.bitmex.orderIDSuffix['ORDERID_RECONCILIATION_HEDGE']
        # Check all existing orders and match them up with what we want to place.
        # If there's an open one, we might be able to amend it to fit what we want.
        for order in existing_orders:
            try:
                if order['side'] == 'Buy':
                    desired_order = buy_orders[buys_matched]
                    buys_matched += 1
                else:
                    desired_order = sell_orders[sells_matched]
                    sells_matched += 1

            except IndexError:
                # Will throw if there isn't a desired order to match. In that case, cancel it.
                to_cancel.append(order)

        while buys_matched < len(buy_orders):
            to_create.append(buy_orders[buys_matched])
            buys_matched += 1

        while sells_matched < len(sell_orders):
            to_create.append(sell_orders[sells_matched])
            sells_matched += 1

        if len(to_amend) > 0:
            for amended_order in reversed(to_amend):
                reference_order = [o for o in existing_orders if o['orderID'] == amended_order['orderID']][0]
                self.logger.info("Amending %4s: %d @ %.*f to %d @ %.*f (%+.*f)" % (
                    amended_order['side'],
                    reference_order['leavesQty'], tickLog, reference_order['price'],
                    amended_order['leavesQty'], tickLog, amended_order['price'],
                    tickLog, (amended_order['price'] - reference_order['price'])
                ))

            self.exchange.amend_bulk_orders(to_amend)

        if len(to_create) > 0:
            self.logger.info("Creating %d orders:" % (len(to_create)))
            for order in reversed(to_create):
                self.logger.info("%4s %d @ %.*f" % (order['side'], order['orderQty'], tickLog, order['price']))
            self.exchange.create_bulk_orders(to_create, suffix)

        # Could happen if we exceed a delta limit
        if len(to_cancel) > 0:
            self.logger.info("Canceling %d orders:" % (len(to_cancel)))
            for order in reversed(to_cancel):
                self.logger.info("%4s %d @ %.*f" % (order['side'], order['leavesQty'], tickLog, order['price']))
            self.exchange.cancel_bulk_orders(to_cancel)

    ###
    # Position Limits
    ###
    def fetch_position_info(self):
        return self.bitmex.fetch_position_info()

    def check_connection(self):
        """Ensure the WS connections are still open."""
        return self.exchange.is_open()

    def exit(self):
        self.logger.info("Shutting down.")
        try:
            self.run_deinit()
            # self.exchange.bitmex.exit()
        except errors.AuthenticationError as e:
            self.logger.info("Was not authenticated; could not cancel orders.")
        except Exception as e:
            self.logger.info("Unable to cancel orders: %s" % e)

    def run_forever(self):
        while True:
            try:
                # sys.stdout.flush()

                # self.check_file_change()
                sleep(config.LOOP_INTERVAL)

                # This will restart on very short downtime, but if it's longer,
                # the MM will crash entirely as it is unable to connect to the WS on boot.
                if not self.check_connection():
                    self.logger.error("Realtime data connection unexpectedly closed, restarting.")
                    sys.exit(1)

                self.run_main_loop()

            except Exception as e:
                # Closes websocket
                del self.exchange
                raise(e)

    def restart(self):
        self.logger.info("Restarting the market maker...")
        os.execv(sys.executable, [sys.executable] + sys.argv)

def cost(instrument, quantity, price):
    mult = instrument["multiplier"]
    P = mult * price if mult >= 0 else mult / price
    return abs(quantity * P)


def margin(instrument, quantity, price):
    return cost(instrument, quantity, price) * instrument["initMargin"]
