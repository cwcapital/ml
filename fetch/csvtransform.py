import tensorflow as tf
import numpy as np
from collections import deque

CANDLEBAR_TIMEFRAME_IN_SECS = 60
N_INPUTS = 5
N_STEPS = 5

class Bar():
    def __init__(self, open_time, open_prc):
        self.open_time = open_time
        self.high = open_prc
        self.low = open_prc
        self.open = open_prc
        self.close = open_prc
        self.volume = 1

    def update(self, prc):
        self.close = prc

        if prc > self.high:
            self.high = prc
        if prc < self.low:
            self.low = prc

        self.volume += 1

    def __str__(self):
        return '(%s, %s, %s, %s, %s, %s)' % (self.open_time, self.open, self.high,
          self.low, self.close, self.volume)

def tick_transform(ticks):
    x = []
    prev_tstamp, prev_bid, _ = ticks[0]
    for tstamp, bid, ask in ticks:
        x.append(tstamp - prev_tstamp)
        x.append(bid - prev_bid)
        prev_tstamp, prev_bid = (tstamp, bid)
    return x

def candle_transform(open_time, open_prc, high, low, close, vol, **kwargs):
    # return ([
    #     # CANDLEBAR_TIMEFRAME_IN_SECS,
    #     open_prc - kwargs['prev_open'],
    #     high - open_prc,
    #     open_prc - low,
    #     close - open_prc,
    #     vol
    # ])
    hh = kwargs['hh']
    ll = kwargs['ll']
    scale = float(hh-ll)
    # print([open_prc, high, low, close])
    # print([
    #     ((open_prc - ll) / scale) * 100.0,
    #     ((low - ll) / scale) * 100.0,
    #     ((high - ll) / scale) * 100.0,
    #     ((close - ll) / scale) * 100.0
    # ])
    # print('ll: ', ll, ', hh: ', hh, '  --------------')
    return ([
        round(((open_prc - ll) / scale) * 100.0, 1),
        round(((high - ll) / scale) * 100.0, 1),
        round(((low - ll) / scale) * 100.0, 1),
        round(((close - ll) / scale) * 100.0, 1),
        vol
    ])

filename_queue = tf.train.string_input_producer([
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-21-00-00-00_2017-12-23-23-59-59.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-24-00-00-00_2017-12-24-01-00-00.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-24-01-00-00_2017-12-24-02-00-00.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-24-02-00-00_2017-12-24-03-00-00.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-24-03-00-00_2017-12-28-23-59-59.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-24-03-00-00_2017-12-28-23-59-59.csv',
    '/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/csv/2017-12-29-00-00-00_2017-12-31-23-59-59.csv'
], num_epochs=1, shuffle=False)

reader = tf.TextLineReader()
_, csv_row = reader.read(filename_queue)

record_defaults = [[tf.constant(0, dtype=tf.int32)], [0.0], [0.0]]
col1, col2, col3 = tf.decode_csv(csv_row, record_defaults=record_defaults)
# tick_stream = tf.stack([col1, col2, col3])

current_bar = None
ticks = []
dataset = []
# dataset_minmax = []
queue = deque(maxlen=500)

with tf.Session() as sess:
    tf.local_variables_initializer().run()
    tf.global_variables_initializer().run()
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    # First tick
    tstamp, bid, ask = sess.run([col1, col2, col3])
    current_bar = Bar(tstamp, bid)
    ticks = [[tstamp, bid, ask]]
    prev_open = bid
    queue.append(bid)

    while True:
        try:
            # Retrieve a single instance:
            t = sess.run([col1, col2, col3])
            tstamp, bid, ask = t
            queue.append(bid)

            # Handle case when starting at end of file
            if tstamp < current_bar.open_time:
                # Reset variables
                ticks = [[tstamp, bid, ask]]
                prev_open = current_bar.open
                current_bar = Bar(tstamp, bid)

            elif tstamp - current_bar.open_time >= CANDLEBAR_TIMEFRAME_IN_SECS:
                # Create training instance
                # first: candlebar
                ll = min(queue)
                hh = max(queue)

                entry = candle_transform(
                    current_bar.open_time,
                    current_bar.open,
                    current_bar.high,
                    current_bar.low,
                    current_bar.close,
                    current_bar.volume,
                    prev_open=prev_open,
                    ll=ll,
                    hh=hh
                )

                # ticks_list = tick_transform(ticks)
                #
                # zero_size = N_INPUTS - len(entry) - len(ticks_list)
                #
                # # second: all ticks that form candlebar
                # entry.extend(ticks_list)
                #
                # # third: padded zeros
                # if zero_size > 1:
                #     entry.extend([0,0] * (zero_size // 2))
                #
                # # Error checking
                # if len(entry) > N_INPUTS:
                #     raise ValueError("Current bar(open_time={0}) has {1} ticks > N_INPUTS({2})".format(
                #         current_bar.open_time,
                #         len(entry),
                #         N_INPUTS
                #     ))



                # Add training instance to dataset
                dataset.append(entry)
                # dataset_minmax.append([ll,hh])

                # Reset variables
                ticks = [[tstamp, bid, ask]]
                prev_open = current_bar.open
                current_bar = Bar(tstamp, bid)
            else:
                current_bar.update(bid)
                ticks.append([tstamp, bid, ask])

        except tf.errors.OutOfRangeError:
            break

# Split dataset into X bars (input) and actual predicted bars Y
X = []
y = []

# First dim of X should be equal to the first dim of y
dim1 = len(dataset) - N_STEPS - 1
dim2 = N_STEPS
dim3 = N_INPUTS

X_ = np.empty((dim1, dim2, dim3))
y_ = np.empty((dim1, 2))

for i in range(dim1):
    X_series = []

    try:
        for j in range(dim2):
            # create X series
            # for k in range(dataset):
            X_[i,j] = dataset[i+j]

        # ll, hh = dataset_minmax[i+dim2]

        # create y series
        # X_series.append(dataset[i+j])
        # y_series.append([l,h])

        hh = X_[i][:,1:2].max(axis=0)
        ll = X_[i][:,2:3].min(axis=0)
        y_[i] = [ll, hh]
        # X.append(bars)
    except IndexError:
        break

X_array = X_
y_array = y_

print('X (shape): ', X_array.shape)
print('y (shape): ', y_array.shape)
print(X_array[10])
print(y_array[10])

np.savez('/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/2017-12-24-00-00-00_2017-12-24-03-00-00', X=X_array, y=y_array)
# np.save('/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/2017-12-24-00-00-00_2017-12-24-03-00-00_X', X_array)
# np.save('/home/jamal/projects/tickdata/machinelearning/datasets/bitmex/xbtusd/2017-12-24-00-00-00_2017-12-24-03-00-00_y', y_array)
