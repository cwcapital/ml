import unittest
from trading.utils.managers import PendingOrderManager
from trading.utils.events import Event, OrderPlaceEvent, ReturnEvent, CancelOrderEvent, CancelAllOrdersEvent, OrderAmendEvent
from trading.bitmex.ml_1 import TradingInterface
from tests.utils.generate_random_price_data import RandomizedEnvironmentGenerator
from tests.utils.mock.entities import OrderManager, MockManager, fetch_initial_data
from tests.utils.ws_thread import BitMEXWebsocket
from trading.utils.tests.helper.tags import display_test_name
from trading.bitmex.interface.market_maker.bitmex import BitMEX
from utils import config as Config
from utils.metadata import PROJECT_DIR
from time import sleep
import random
import datetime as dt
import logging
import warnings
import os
import sys

random.seed(50)

Config.set_path_to_config_file(os.path.join(PROJECT_DIR, 'config.test.json'))
config = Config.load()

logger = logging.getLogger()
logger.level = logging.INFO
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)

class args:
    box_size = 5
    n_input = 10
    restore = False
    algos = []
    DRY_RUN = True

class argsLive:
    box_size = 5
    n_input = 10
    restore = False
    algos = []
    DRY_RUN = False

class Test_PendingOrderManager_With_LiveBitmexOrders(unittest.TestCase):
    def tearDown(self):
        PendingOrderManager.DRY_RUN = False

        # Cancel pending orders
        CancelAllOrdersEvent()

        # Reverse open exposure
        open_exposure = PendingOrderManager.get_exposure()
        if open_exposure != 0:
            OrderPlaceEvent(-open_exposure, prc=None, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)

    def setUp(self):
        Event.reset()
        self.interface = TradingInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD',
                                      argsLive,
                                      ordermanager=None,
                                      initial_data=None,
                                      config=config)
        PendingOrderManager.reset()
        PendingOrderManager.interface = self.interface

    @unittest.skip('subsequent tests will fail if orderbook cannot be fetched')
    @display_test_name
    def test_fetch_order_book(self):
        orderbook = self.interface.manager.exchange.bitmex.fetch_orderbook(symbol='XBTUSD', depth=10)
        self.assertIsNotNone(orderbook)

    @display_test_name
    def test_order_rejected_on_the_server_is_also_rejected_internally(self):
        PendingOrderManager.DRY_RUN = False

        # Place an invalid order
        orderbook = self.interface.manager.exchange.bitmex.fetch_orderbook(symbol='XBTUSD', depth=10)
        open_price = None
        open_price = 0
        for o in orderbook:
            if o['side'] == 'Buy':
                open_price = max(o['price'] + 100, open_price)

        e = OrderPlaceEvent(qty=100, prc=open_price, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        self.assertEqual(events[0]['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(events[0]['exchange']['data']['ordStatus'], 'Canceled')

        self.assertNotEqual(events[0]['ordStatus'], 'New')

        query = PendingOrderManager.get(clOrdID=e.clOrdID)
        self.assertNotEqual(query.ordStatus, 'New')

    @display_test_name
    def test_market_order(self):
        PendingOrderManager.DRY_RUN = False

        # Place a new order
        e = OrderPlaceEvent(qty=100, prc=None, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        order = events[0]

        self.assertEqual(order['exchange']['source'], 'bitmex')
        self.assertEqual(order['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(order['exchange']['data']['ordStatus'], 'Filled')

        sleep(0.3)

        # Close exposure
        e = OrderPlaceEvent(qty=-100, prc=None, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        order = events[0]

        self.assertEqual(order['exchange']['source'], 'bitmex')
        self.assertEqual(order['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(order['exchange']['data']['ordStatus'], 'Filled')

    @display_test_name
    def test_notified_when_order_fills(self):
        try:
            PendingOrderManager.DRY_RUN = False

            endpoint = 'orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD,execution,order,position'

            ws = BitMEXWebsocket(endpoint=endpoint, orderbook_type='orderBook10')
            ws.connect(endpoint=config.BASE_URL, symbol=config.SYMBOL)

            ws.flush_data()

            # Place two sandwich orders
            orderbook = self.interface.manager.exchange.bitmex.fetch_orderbook(symbol='XBTUSD', depth=10)
            buy_open_price = None
            sell_open_price = None
            for o in orderbook:
                if o['side'] == 'Buy':
                    buy_open_price = o['price']
                    buy_open_vol = o['size']
                    break

            for o in orderbook:
                if o['side'] == 'Sell':
                    if sell_open_price is None or o['price'] < sell_open_price:
                        sell_open_price = o['price']
                        sell_open_vol = o['size']

            self.assertFalse(buy_open_vol > 1000000 or sell_open_vol > 1000000, msg='\
too much open volume, (buy,sell)=({},{}). orders will not fill within time allowed'.format(
    buy_open_vol, sell_open_vol
))

            e_buy = OrderPlaceEvent(qty=5000, prc=buy_open_price, DRY_RUN=False)
            e_sell = OrderPlaceEvent(qty=-5000, prc=sell_open_price, DRY_RUN=False)

            PendingOrderManager.update(interface=self.interface)
            events = ReturnEvent.flush()
            buy_order = PendingOrderManager.get(e_buy.clOrdID)
            sell_order = PendingOrderManager.get(e_sell.clOrdID)

            self.assertEqual(events[0]['exchange']['data']['clOrdID'], e_buy.clOrdID)
            self.assertEqual(events[0]['exchange']['data']['ordStatus'], 'New')
            self.assertEqual(buy_order.ordStatus, 'New')

            self.assertEqual(events[1]['exchange']['data']['clOrdID'], e_sell.clOrdID)
            self.assertEqual(events[1]['exchange']['data']['ordStatus'], 'New')
            self.assertEqual(sell_order.ordStatus, 'New')

            print('top of book buy|sell = {}|{}'.format(buy_open_vol, sell_open_vol))
            print('waiting for order to fill ..., TIMEOUT=60 seconds')
            now = dt.datetime.now()
            buy_filled_qty = 0
            sell_filled_qty = 0
            b_timedout = True
            b_partially_filled = False
            buy_market_filled_qty = 0
            sell_market_filled_qty = 0

            k = 1
            while (dt.datetime.now() - now).seconds < 60:
                data = ws.flush_data()
                PendingOrderManager.update(data=data, interface=self.interface)

                for i in data['execution']:
                    if i['clOrdID'] == buy_order.clOrdID:
                        buy_order = PendingOrderManager.get(e_buy.clOrdID)

                        if i['lastQty']:
                            buy_filled_qty += i['lastQty']

                        if i['ordStatus'] == 'PartiallyFilled':
                            b_partially_filled = True

                    if i['clOrdID'] == sell_order.clOrdID:
                        sell_order = PendingOrderManager.get(e_sell.clOrdID)

                        if i['lastQty']:
                            sell_filled_qty -= i['lastQty']

                        if i['ordStatus'] == 'PartiallyFilled':
                            b_partially_filled = True

                self.assertEqual(buy_order.filled_quantity, buy_filled_qty)
                self.assertEqual(sell_order.filled_quantity, sell_filled_qty)

                # Place market orders to fill pending orders
                if buy_filled_qty < 5000:
                    vol = int((sell_open_vol / 3) * (1 + 0.25 * k))
                    OrderPlaceEvent(-vol, prc=None)
                    sell_market_filled_qty += vol

                if sell_filled_qty < 5000:
                    vol = int((buy_open_vol / 3) * (1 + 0.25 * k))
                    OrderPlaceEvent(vol, prc=None)
                    buy_market_filled_qty += vol


                k += 1

                PendingOrderManager.update(interface=self.interface)

                if buy_order.ordStatus != 'New' and sell_order.ordStatus != 'New':
                    print('Orders filled!!!')
                    b_timedout = False
                    break

                sleep(0.2)

            self.assertFalse(b_timedout, msg='Order did not filled within allowed time')

            buy_order = PendingOrderManager.get(buy_order.clOrdID)
            sell_order = PendingOrderManager.get(sell_order.clOrdID)

            self.assertNotEqual(buy_order.additional['bitmex']['ordStatus'], 'New')
            self.assertEqual(buy_order.ordStatus, buy_order.additional['bitmex']['ordStatus'])
            self.assertGreater(buy_order.filled_quantity, 0)

            self.assertNotEqual(sell_order.additional['bitmex']['ordStatus'], 'New')
            self.assertEqual(sell_order.ordStatus, sell_order.additional['bitmex']['ordStatus'])
            self.assertLess(sell_order.filled_quantity, 0)

            if not b_partially_filled:
                warnings.warn('ordStatus never entered PartiallyFilled, and hence, was not tested')

        finally:
            ws.exit()

    @display_test_name
    def test_amend_order(self):
        PendingOrderManager.DRY_RUN = False

        # Place a new order
        orderbook = self.interface.manager.exchange.bitmex.fetch_orderbook(symbol='XBTUSD', depth=10)
        open_price = None
        for o in orderbook:
            if o['side'] == 'Buy':
                open_price = o['price'] - 2

        e = OrderPlaceEvent(qty=100, prc=open_price, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        order = events[0]
        self.assertEqual(order['exchange']['source'], 'bitmex')
        self.assertEqual(order['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(order['exchange']['data']['ordStatus'], 'New')
        self.assertEqual(order['exchange']['data']['price'], open_price)

        query = PendingOrderManager.get(e.clOrdID)
        self.assertEqual(query.price, open_price)

        sleep(0.3)

        # Modify price
        amend_price = open_price + 10
        OrderAmendEvent(e.clOrdID, price=amend_price)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        self.assertEqual(len(events), 1)
        self.assertEqual(events[0]['clOrdID'], e.clOrdID)
        self.assertEqual(events[0]['old_price'], open_price)
        self.assertEqual(events[0]['new_price'], amend_price)

        query = PendingOrderManager.get(clOrdID=e.clOrdID)
        self.assertEqual(query.price, amend_price)
        self.assertEqual(query.additional['bitmex']['price'], amend_price)

    @display_test_name
    def test_place_and_cancel_buy_order(self):
        PendingOrderManager.DRY_RUN = False

        # Place a new order
        orderbook = self.interface.manager.exchange.bitmex.fetch_orderbook(symbol='XBTUSD', depth=10)
        open_price = None
        for o in orderbook:
            if o['side'] == 'Buy':
                open_price = o['price'] - 20

        e = OrderPlaceEvent(qty=100, prc=open_price, DRY_RUN=False)

        PendingOrderManager.update(interface=self.interface)
        events = ReturnEvent.flush()

        self.assertEqual(len(events), 1)

        order = events[0]
        self.assertEqual(order['exchange']['source'], 'bitmex')
        self.assertEqual(order['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(order['exchange']['data']['ordStatus'], 'New')

        sleep(0.3)

        # Cancel the newly placed order
        CancelOrderEvent(clOrdID=e.clOrdID)

        PendingOrderManager.update(interface=self.interface)

        events = ReturnEvent.flush()

        self.assertEqual(len(events), 1)

        order = events[0]
        self.assertEqual(order['exchange']['data']['clOrdID'], e.clOrdID)
        self.assertEqual(order['exchange']['data']['ordStatus'], 'Canceled')

        # Check order shows up as canceled when queried
        query = PendingOrderManager.get(clOrdID=e.clOrdID)
        self.assertEqual(query.additional['bitmex']['ordStatus'], 'Canceled')


class Test_PendingOrderManager_And_FeedEngine(unittest.TestCase):
    def setUp(self):
        PendingOrderManager.pending = []

    @display_test_name
    def test_pending_order_fills(self):
        PendingOrderManager.DRY_RUN = True

        order_open_prc = 8278

        orderbook = {
                'symbol': 'XBTUSD',
                'bids': [[order_open_prc, 1125],
                    [order_open_prc - 1, 1025]],
                'asks': [[order_open_prc + 3, 2525],
                    [order_open_prc + 4, 895]],
                'timestamp': '2018-06-08T17:31:29.274Z'
        }

        data = RandomizedEnvironmentGenerator(
            [8280, 8280, 8275, 8270],
            dt.datetime(2018, 5, 22),
            n_random_fluctuations=5,
            orderbook_reset_sensitivity=3,
            variance=3,
            max_deviation=10,
            rounding=2
        )

        manager = MockManager()

        interface = TradingInterface('orderBook10:XBTUSD,trade:XBTUSD,instrument:XBTUSD',
                                          args,
                                          ordermanager=OrderManager,
                                          initial_data=fetch_initial_data([8275, 8280]),
                                          config=config)

        PendingOrderManager.interface = interface

        vwap, total_volume, _ = interface.get_vwap_and_total_volume_and_closed_pl()

        self.assertEqual(vwap, 0)
        self.assertEqual(total_volume, 0)

        class Order:
            filled = False

            @classmethod
            def endDataLoop(cls, data):
                cls.filled = True

        order = OrderPlaceEvent(qty=100, prc=order_open_prc, orderbook=orderbook,
           callback=[('Filled', Order.endDataLoop)])

        orderbook, trades, ticker = next(data)
        self.assertGreater(ticker['last'], order_open_prc)
        for orderbook, trades, ticker in data:
            interface.engine(orderbook=orderbook, trades=trades, ticker=ticker)
            PendingOrderManager.update(orderbook=orderbook, trades=trades)
            if Order.filled:
                break

        self.assertTrue(Order.filled)

        vwap, total_volume, _ = interface.get_vwap_and_total_volume_and_closed_pl()
        self.assertEqual(vwap, order_open_prc)
        self.assertEqual(total_volume, 100)
        self.assertGreater(interface.fees_paid(), 0)
