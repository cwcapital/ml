from abc import ABCMeta, abstractmethod
from trading.utils.events import RenkoBarEvent, OrderPlaceEvent, CancelOrderEvent,\
 OrderFilledEvent, OrderAmendEvent, ReturnEvent, CancelAllOrdersEvent
from .entities import Order
from .exceptions import InvalidOrderState
from .custom_events import CustomEvent
import logging
import warnings

logger = logging.getLogger('root')

class Manager(metaclass=ABCMeta):
    def __init__(self):
        self.state = 'active'

    @abstractmethod
    def update(self, **kwargs):
        pass

class PendingOrderManager(Manager):
    pending = []
    interface = None

    @classmethod
    def reset(cls):
        cls.pending = []

    @classmethod
    def fetch_order(cls, clOrdID):
        return [o for o in cls.pending if o.clOrdID == clOrdID]

    @classmethod
    def fetch_orders(cls, clOrdIDs):
        return [o for o in cls.pending if o.clOrdID in clOrdIDs]

    @classmethod
    def fetch_all_orders(cls):
        return cls.pending

    @classmethod
    def fetch_orders_by_status(cls, ordStatus):
        return [o for o in cls.pending if o.ordStatus in ordStatus]

    @classmethod
    def fetch_market_orders(cls):
        return [o for o in cls.pending if o.ordType in 'market']

    @classmethod
    def fetch_cancelled_orders(cls):
        return cls.fetch_orders_by_status(ordStatus=['Canceled'])

    @classmethod
    def fetch_non_filled_orders(cls):
        return cls.fetch_orders_by_status(ordStatus=['New', 'PartiallyFilled'])

    @classmethod
    def fetch_filled_orders(cls):
        return cls.fetch_orders_by_status(ordStatus=['Filled'])

    @classmethod
    def get_exposure(cls):
        exposure = 0
        for order in [o for o in cls.pending if o.ordType in 'market' or o.ordStatus in ['Filled', 'PartiallyFilled']]:
            exposure += order.filled_quantity
        return exposure

    @classmethod
    def add_pending(cls, clOrdID, prc, qty, ordType, ordStatus, preceding_quantity, callback):
        order = Order(clOrdID, prc, qty, ordType, ordStatus, preceding_quantity, callback)
        cls.pending.append(order)
        return order

    @classmethod
    def _check_for_filled_orders(cls, orderbook, trades):
        for order in (o for o in cls.pending if o.ordStatus != 'Filled'):
            order.update(orderbook, trades)

    @classmethod
    def update_cancel_order_events(cls, **kwargs):
        if CancelAllOrdersEvent.flush():
            non_filled_orders = cls.fetch_non_filled_orders()
            for o in non_filled_orders:
                CancelOrderEvent(o.clOrdID)

        canceled_orders = CancelOrderEvent.flush()
        for o in canceled_orders:
            order = cls.get(o.clOrdID)
            if order:
                exchange = None

                if not cls.DRY_RUN:
                    info = cls.interface.manager.exchange.bitmex.cancel(order.bitmex_order_id)
                    if info:
                        if len(info) == 1:
                            exchange = {
                                'source': 'bitmex',
                                'data': info[0]
                            }
                        else:
                            raise Exception('expected len(info)=1, not {}. info {}', len(info), info)

                        order.add_info(exchange)

                        # check for any upstream error messages
                        if info[0].get('error'):
                            logger.info(info[0]['error'])

                        # check that order was actually canceled
                        if info[0]['ordStatus'] != 'Canceled':
                            logger.info('cannot cancel {}, ordStatus={}'.format(order, info[0]['ordStatus']))
                            if info[0]['ordStatus'] == 'Filled':
                                OrderFilledEvent(order.clOrdID,
                                                qty=order.remaining_quantity)
                            continue
                    else:
                        warnings.warn('unable to confirm {} was canceled on the server'.format(order))
                        continue

                if order.ordStatus == 'Filled':
                    warnings.warn('{} status cannot be changed from Filled -> Canceled'.format(order))
                    continue
                if order.ordStatus == 'Canceled':
                    warnings.warn('{} is already canceled'.format(order))
                    continue

                order.ordStatus = 'Canceled'
                logger.info('Order {} was successfully canceled'. format(order))

                ret = {
                    'type': 'order',
                    'clOrdID': order.clOrdID,
                    'ordStatus': order.ordStatus
                }

                if exchange:
                    ret['exchange'] = exchange

                ReturnEvent(ret)

                if order.callback:
                    for callback in (cb[1] for cb in order.callback if cb[0] == 'Cancelled'):
                        callback(order)

    @classmethod
    def update_new_order_events(cls, **kwargs):
        new_orders = OrderPlaceEvent.flush()
        for o in new_orders:
            info = None
            exchange = None
            order_status = o.ordStatus

            if not cls.DRY_RUN:
                info = cls.interface.manager.exchange.bitmex.place_order(o.quantity, o.price, clOrdID=o.clOrdID)
                exchange = {
                    'source': 'bitmex',
                    'data': info
                }
                if not info:
                    warnings.warn('place {} failed on server'.format(o))
                    continue # Do not open the order internally

            order = cls.add_pending(o.clOrdID, o.price, o.quantity, o.ordType, order_status, o.preceding_quantity, o.callback)

            if not cls.DRY_RUN:
                # add exchange info to internal order
                order.add_info(exchange)

                # check for any upstream error messages
                if info.get('error'):
                    logger.info(info[0]['error'])

                # cancel order internally if already canceled on server
                if info['ordStatus'] == 'Canceled':
                    CancelOrderEvent(order.clOrdID)

                # check that the status is actually 'New' and not an
                # unexpected order type
                if not info['ordStatus'] in ['New', 'Canceled]']:
                    warnings.warn('found ordStatus={} on server'.format(info['ordStatus']))
                    order_status = info['ordStatus']

            side = 'buy' if o.quantity > 0 else 'sell'

            if o.ordType == 'market':
                try:
                    price = info['price']
                    orderQty = info['orderQty']
                except TypeError as e:
                    if e.args[0] == '\'NoneType\' object is not subscriptable':
                        price = None
                        orderQty = None
                    else:
                        raise(e)

                if o.price:
                    if o.price != price:
                        logger.info('modifying market order price from {} -> {} on the server'.format(
                            o.price, price
                        ))
                    o.price = price

                OrderFilledEvent(o.clOrdID, o.quantity, price=price, orderQty=orderQty)

            ReturnEvent({
                'type': 'order',
                'order_type': '{}_{}'.format(o.ordType, side),
                'clOrdID': o.clOrdID,
                'price': o.price,
                'quantity': o.quantity,
                'ordStatus': order_status,
                'preceding_quantity': o.preceding_quantity,
                'source': 'pending_manager',
                'exchange': exchange
            })

            logger.info('Placed {} order: {}'.format(o.ordType, order))

    @classmethod
    def update(cls, **kwargs):
        cls.update_new_order_events(**kwargs)

        # Order Updates - simulate filling of orders
        if cls.DRY_RUN:
            if kwargs.get('orderbook'):
                trades = kwargs.get('trades', [])
                cls._check_for_filled_orders(kwargs['orderbook'], trades)
        else:
        # Use websocket order data to check if orders were filled on server
            if kwargs.get('data'):
                execution = kwargs['data'].get('execution', [])
                for item in execution:
                    order = cls.get(item['clOrdID'])
                    if order:
                        status = item.get('ordStatus')
                        if status:
                            order.additional['bitmex']['ordStatus'] = status
                        qty = item.get('lastQty')
                        if qty:
                            side = item['side']

                            if side == 'Buy':
                                pass
                            elif side == 'Sell':
                                qty *= -1
                            else:
                                raise Exception('unexpected order side, {}'.format(side))

                            OrderFilledEvent(item['clOrdID'], qty)

        for order in cls.fetch_non_filled_orders():
            if order.callback:
                for callback in (cb[1] for cb in order.callback if cb[0] == 'onUpdate'):
                    callback(order, orderbook=kwargs['orderbook'])

        cls.update_cancel_order_events(**kwargs)

        filled_orders = OrderFilledEvent.flush()
        for o in filled_orders:
            if o.qty == 0:
                warnings.warn('cannot fill order {} with fill qty = zero. skipping ...'.format(o))
                continue

            order = cls.get(o.clOrdID)
            if order:
                if order.side == 'sell':
                    if o.qty > 0:
                        warnings.warn('changing positive fill quantity for sell order to negative quantity')
                        o.qty *= -1

                if order.ordStatus not in ['New', 'PartiallyFilled', 'WaitingToFillMarket']:
                    warnings.warn('cannot fill order with ordStatus({}), order {}'.format(order.ordStatus, order))
                    continue

                total_filled = order.filled_quantity + o.qty
                if (order.quantity > 0 and total_filled > order.quantity) or \
                   (order.quantity < 0 and total_filled < order.quantity):

                    logger.debug('reducing o.qty from {} -> {}'.format(
                        o.qty, order.remaining_quantity
                    ))
                    o.qty = order.remaining_quantity
                    total_filled = order.quantity

                order.filled_quantity = total_filled
                if order.filled_quantity == order.quantity:
                    order.ordStatus = 'Filled'
                    custom_cancel_event_name = 'cancel_{}_on_new_pending_order_placed'.format(order.clOrdID)
                    CustomEvent.deregister(custom_cancel_event_name)
                else:
                    order.ordStatus = 'PartiallyFilled'

                logger.info('Order {} filled {}, filled/total={}/{}'.format(
                    o.clOrdID, o.qty, order.filled_quantity, order.quantity
                ))

                # Update internal filled orders
                if cls.interface:
                    cls.interface.place_order(o.qty,
                                order_price=order.price,
                                order_type=order.ordType,
                                clOrdID=order.clOrdID,
                                **o.kwargs)

                # Run callbacks
                if order.callback:
                    for callback in (cb[1] for cb in order.callback if cb[0] == order.ordStatus):
                        callback({
                            'prc': order.price,
                            'qty': o.qty * (-1 if order.side == 'sell' else 1),
                            'clOrdID': order.clOrdID,
                            'ordType': 'pending'
                        })

        amended_orders = OrderAmendEvent.flush()
        for o in amended_orders:
            order = cls.get(o.clOrdID)
            if order:
                if not cls.DRY_RUN:
                    info = cls.interface.manager.exchange.bitmex.amend_bulk_orders([{'orderID': order.bitmex_order_id,
                        'price': o.price}])
                    if info:
                        if len(info) == 1:
                            exchange = {
                                'source': 'bitmex',
                                'data': info[0]
                            }
                        else:
                            raise Exception('expected len(info)=1, not {}. info {}', len(info), info)

                        # check for any upstream error messages
                        if info[0].get('error'):
                            logger.error(info[0]['error'])

                        order.add_info(exchange)
                    else:
                        warnings.warn('unable to amend on server. expected order {} price to be amended to {}'.format(order, o.price))
                        continue

                if order.ordStatus in ['Canceled', 'Filled']:
                    warnings.warn('cannot amend order in the following state: {}'.format(order))
                    continue

                old_price = order.price
                order.price = o.price

                logger.info('Order {} price amended from {} -> {}'.format(
                    order.clOrdID,
                    old_price,
                    order.price
                ))

                ReturnEvent({
                    'type': 'amend',
                    'clOrdID': order.clOrdID,
                    'old_price': old_price,
                    'new_price': order.price
                })

                if order.callback:
                    for callback in (cb[1] for cb in order.callback if cb[0] == 'Amend'):
                        callback(order)

    @classmethod
    def get(cls, clOrdID):
        for order in cls.pending:
            if clOrdID == order.clOrdID:
                return order

        warnings.warn('could find order, using clOrdID={}'.format(clOrdID))

class RenkoBarManager(Manager):
    def direction_encode(self):
        return 0 if self.direction == 'UP' else 1

    def update(self, **kwargs):
        new_price = kwargs['new_price']
        if not self.price:
            self.price = new_price

        try:
            if new_price >= self.next_up_price:
                self.prev_price = self.price

                delta = new_price - self.price
                self.price = new_price - (delta % self.box_size)
                self.next_up_price = self.price + self.box_size
                self.next_down_price = self.price - (2 * self.box_size)
                self.direction = 'UP'

                return RenkoBarEvent(desc='new_bar', price=self.price, prev_price=self.prev_price, direction='UP')

            if new_price <= self.next_down_price:
                self.prev_price = self.price

                delta = self.price - new_price
                self.price = new_price + (delta % self.box_size)
                self.next_up_price = self.price + (2 * self.box_size)
                self.next_down_price = self.price - self.box_size
                self.direction = 'DOWN'

                return RenkoBarEvent(desc='new_bar', price=self.price, prev_price=self.prev_price, direction='DOWN')

            return None

        except AttributeError as e:
            message = e.args[0]
            if message in [
                '\'RenkoBarManager\' object has no attribute \'next_up_price\'',
                '\'RenkoBarManager\' object has no attribute \'next_down_price\''
            ]:
                self.next_up_price = new_price + self.box_size
                self.next_down_price = new_price - self.box_size
            else:
                raise(e)

    def __init__(self, initial_price, box_size, initial_direction=None):
        '''
        if initial_direction = None, then an equidistant move up or down
        will trigger a new bar (for the first candle)
        '''

        if initial_direction:
            assert initial_direction.upper() in ['UP', 'DOWN'], 'initial_direction must be in either UP, DOWN or None'

        self.price = initial_price
        self.prev_price = None
        self.direction = initial_direction
        self.box_size = box_size

        if initial_direction == 'UP':
            self.next_up_price = self.price + self.box_size
            self.next_down_price = self.price - (2 * self.box_size)

        if initial_direction == 'DOWN':
            self.next_up_price = self.price + (2 * self.box_size)
            self.next_down_price = self.price - self.box_size
