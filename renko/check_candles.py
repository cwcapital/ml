import numpy as np
import os
import sys

np.set_printoptions(threshold=np.nan)

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

path = sys.argv[1]
count = int(sys.argv[2])

open_path = os.path.join(SCRIPT_DIR, path, 'test', 'results_0.npy')
n = np.load(open_path)

print('Test: ')
print(n.shape)
print(n[:count])

open_path = os.path.join(SCRIPT_DIR, path, 'validation', 'results_0.npy')
n = np.load(open_path)

print('Validation: ')
print(n.shape)
print(n[:count])
